<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace console\controllers;

use backend\services\CalendarService;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;
use Yii;

class CalendarController extends Controller
{

    public function actionImport()
    {

        $calendarService = new CalendarService();
        $calendarService->import();

        $this->stdout(Yii::t('backend', 'Calendar sync success') . "\n", Console::FG_GREEN);

        return ExitCode::OK;
    }
}
