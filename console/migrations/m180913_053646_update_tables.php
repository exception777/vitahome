<?php

use yii\db\Migration;

/**
 * Class m180913_053646_update_tables
 */
class m180913_053646_update_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable('ob_customer', 'customer');
        $this->addColumn('customer', 'user_id', $this->integer());
        $this->addColumn('customer', 'email', $this->string(80));
        $this->addColumn('customer', 'information', $this->string(1000));

        $this->renameTable('ob_status', 'status');
        $this->addColumn('status', 'approved', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('status', 'created_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->addColumn('status', 'paid', $this->boolean()->notNull()->defaultValue(0));

        $this->renameTable('ob_reserv', 'booking');
        $this->renameColumn('booking', 'apartment_id', 'object_id');
        $this->renameColumn('booking', 'start_date', 'date_in');
        $this->renameColumn('booking', 'end_date', 'date_out');
        $this->renameColumn('booking', 'comment', 'information');
        $this->addColumn('booking', 'ical_hash', $this->string());
        $this->addColumn('booking', 'user_id', $this->integer());
        $this->addColumn('booking', 'transfer_in', $this->float());
        $this->addColumn('booking', 'transfer_out', $this->float());
        $this->addColumn('booking', 'more_pay', $this->float());
        $this->addColumn('booking', 'booking_total', $this->float());
        $this->addColumn('booking', 'approved', $this->boolean()->notNull()->defaultValue(1));

        $this->renameTable('ob_payment', 'payment');
        $this->renameColumn('payment', 'reserv_id', 'booking_id');
        $this->addColumn('payment', 'created_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'));

        $this->addColumn('user', 'percent', $this->float());

        $this->createTable('ical', [
            'id' => $this->primaryKey(),
            'apartment_id' => $this->integer()->notNull(),
            'title' => $this->string(100)->notNull(),
            'ical_url' => $this->string(500)->notNull(),
            'status_id' => $this->integer(),
            'approved' => $this->boolean()->notNull(),
            'last_update' => $this->dateTime(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);

        $this->createIndex('idx-ical-apartment_id', 'ical', 'apartment_id');
        $this->addForeignKey('fk-ical-apartment_id', 'ical', 'apartment_id', 'apartment',
            'id', 'RESTRICT', 'CASCADE');
        $this->createIndex('idx-ical-status_id', 'ical', 'status_id');
        $this->addForeignKey('fk-ical-status_id', 'ical', 'status_id', 'status',
            'id', 'RESTRICT', 'CASCADE');

        $this->addColumn('apartment', 'ical_hash', $this->string(40));
        $this->createIndex('idx-apartment-ical_hash', 'apartment', 'ical_hash');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('customer', 'user_id');
        $this->dropColumn('customer', 'email');
        $this->dropColumn('customer', 'information');
        $this->renameTable('customer', 'ob_customer');

        $this->dropColumn('status', 'approved');
        $this->dropColumn('status', 'created_at');
        $this->dropColumn('status', 'paid');
        $this->renameTable('status', 'ob_status');

        $this->renameColumn('booking', 'object_id', 'apartment_id');
        $this->renameColumn('booking', 'date_in', 'start_date');
        $this->renameColumn('booking', 'date_out', 'end_date');
        $this->renameColumn('booking', 'information', 'comment');
        $this->dropColumn('booking', 'ical_hash');
        $this->dropColumn('booking', 'user_id');
        $this->dropColumn('booking', 'transfer_in');
        $this->dropColumn('booking', 'transfer_out');
        $this->dropColumn('booking', 'more_pay');
        $this->dropColumn('booking', 'booking_total');
        $this->dropColumn('booking', 'approved');
        $this->dropColumn('booking', 'user_id');
        $this->renameTable('booking', 'ob_reserv');

        $this->renameColumn('payment', 'booking_id', 'reserv_id');
        $this->dropColumn('payment', 'created_at');
        $this->renameTable('payment', 'ob_payment');

        $this->dropColumn('user', 'percent');

        $this->dropIndex('idx-apartment-ical_hash', 'apartment');
        $this->dropColumn('apartment', 'ical_hash');
        $this->dropForeignKey('fk-ical-apartment_id', 'ical');
        $this->dropForeignKey('fk-ical-status_id', 'ical');
        $this->dropIndex('idx-ical-apartment_id', 'ical');
        $this->dropIndex('idx-ical-status_id', 'ical');
        $this->dropTable('ical');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180913_053646_update_tables cannot be reverted.\n";

        return false;
    }
    */
}
