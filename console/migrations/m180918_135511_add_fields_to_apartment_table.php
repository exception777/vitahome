<?php

use yii\db\Migration;

/**
 * Class m180918_135511_add_fields_to_apartment_table
 */
class m180918_135511_add_fields_to_apartment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('apartment', 'expired_at', $this->dateTime());
        $this->addColumn('apartment', 'up_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('apartment', 'expired_at');
        $this->renameColumn('apartment', 'up_at');
    }
}
