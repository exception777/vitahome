<?php

namespace frontend\core;

use common\models\City;
use common\models\Page;
use yii\web\Controller;
use yii;

class BaseController extends Controller
{
    public function __construct($id, $module, $config = [])
    {
        $pages = Yii::$app->cache->get(Yii::t('cache', 'pages'));
        if ($pages === false) {
            $pages = Page::find()->all();
            Yii::$app->cache->set(Yii::t('cache', 'pages'), $pages, Yii::$app->params['cacheTime']['month']);
        }
        $this->view->params['pages'] = $pages;

        $cities = Yii::$app->cache->get(Yii::t('cache', 'cities'));
        if ($cities === false) {
            $cities = City::find()->where(['status' => City::ENABLE])->all();
            Yii::$app->cache->set(Yii::t('cache', 'cities'), $cities, Yii::$app->params['cacheTime']['month']);
        }
        $this->view->params['cities'] = $cities;
        $this->view->params['city'] = null;

        parent::__construct($id, $module, $config);
    }
}