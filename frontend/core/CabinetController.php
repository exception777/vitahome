<?php

namespace frontend\core;

use common\models\Wallet;
use yii;

class CabinetController extends BaseController
{
    public $wallet;

    public function __construct($id, $module, $config = [])
    {
        $this->wallet = Yii::$app->cache->get(Yii::t('cache', 'wallet', ['id' => Yii::$app->user->id]));
        if ($this->wallet === false) {
            $this->wallet = Wallet::find()->where(['user_id' => Yii::$app->user->id])->one();
            if ($this->wallet === null) {
                $this->wallet = new Wallet();
                $this->wallet->user_id = Yii::$app->user->id;
                $this->wallet->balance = 500;
                $this->wallet->save();
            }
            Yii::$app->cache->set(Yii::t('cache', 'wallet', ['id' => Yii::$app->user->id]), $this->wallet, 300);
        }
        $this->view->params['wallet'] = $this->wallet;
        parent::__construct($id, $module, $config);
    }

}