<?php
namespace frontend\controllers;

use common\models\Apartment;
use common\models\City;
use common\models\Page;
use common\models\Slider;
use Yii;
use frontend\core\BaseController;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        $apartments = Apartment::find()->with(['apartmentPhotos'])
            ->where([
                'status' => Apartment::STATUS_APPROVED,
                'user_id' => 3,
        ])->orderBy(['position' => 'ASC'])->limit(20)->all();

        $city = Yii::$app->cache->get('site-index-city');
        if ($city === false) {
            $city = City::findOne(['id' => 1]);
            Yii::$app->cache->set('site-index-city', $city, Yii::$app->params['cacheTime']['day']);
        }

        $slider = Yii::$app->cache->get('site-index-slider');
        if ($slider === false) {
            $slider = Slider::find()->onCondition(['status' => 1])->orderBy(['id' => 'asc'])->all();
            Yii::$app->cache->set('site-index-slider', $slider, Yii::$app->params['cacheTime']['day']);
        }

        return $this->render('index', [
            'apartments' => $apartments,
            'city' => $city,
            'slider' => $slider
        ]);
    }

    public function actionPage($alias)
    {
        $page = Yii::$app->cache->get('site-page-' . $alias);
        if ($page === false) {
            $page = Page::findOne(['alias' => $alias]);
            Yii::$app->cache->set('site-page-' . $alias, $page, 3600 * 24);
        }

        if (is_null($page)) {
            throw new NotFoundHttpException(Yii::t('frontend', 'Not Found'));
        }
        return $this->render('page', [
            'page' => $page
        ]);
    }

}
