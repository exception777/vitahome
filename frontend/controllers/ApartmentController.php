<?php

namespace frontend\controllers;

use common\models\Apartment;
use frontend\core\BaseController;
use yii\web\NotFoundHttpException;

class ApartmentController extends BaseController
{
    public function actionIndex($id)
    {
        $apartment = Apartment::find()
            ->with(['apartmentPhotos', 'city'])
            ->where('id = :id', [
                ':id' => $id
            ])
            ->one();

        if (is_null($apartment)) {
            throw new NotFoundHttpException(\Yii::t('frontend', 'Not Found'));
        }

        if ($apartment->city->id == 1 && $apartment->expired_at < date('Y-m-d H:i:s')) {
            throw new NotFoundHttpException(\Yii::t('frontend', 'Not Found'));
        }

        return $this->render('index', [
            'apartment' => $apartment
        ]);
    }

}
