<?php

namespace frontend\controllers\cabinet;

use common\models\Apartment;
use common\models\ApartmentPhoto;
use frontend\core\CabinetController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii;
use yii\web\Response;

class ApartmentController extends CabinetController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disable' => ['post'],
                    'approved' => ['post'],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new Apartment();

        $post = \Yii::$app->request->post();
        if($model->load($post)) {
            $phones = [];
            foreach($post['phones'] as $phone){
                if(!empty($phone)){
                    $phones[] = $phone;
                }
            }
            $model->phone = implode(',', $phones);
            $model->user_id = Yii::$app->user->id;
            $model->status = $model->checkStatus();
            if ($model->save()) {
                Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success save'));
                return $this->redirect(['edit', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('frontend', 'Error save'));
            }
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionEdit($id)
    {
        /**
         * @var $model \common\models\Apartment
         */
        $model = Apartment::find()->with('apartmentPhotos')->where(['id' => $id, 'user_id' => \Yii::$app->user->identity->id])->one();
        if(is_null($model))
            throw new NotFoundHttpException;

        $post = \Yii::$app->request->post();
        if($model->load($post)){
            $phones = [];
            foreach($post['phones'] as $phone){
                if(!empty(trim($phone))){
                    $phones[] = $phone;
                }
            }
            $model->phone = implode(',', $phones);
            $model->updated_at = date('Y-m-d H:i:s');
            $model->user_id = Yii::$app->user->id;
            $model->status = $model->checkStatus();
            if ($model->save()) {
                Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success save'));
                if (isset($post['btn_update_and_list'])) {
                    return $this->redirect(['index']);
                }
                return $this->redirect(['edit', 'id' => $model->id]);
            } else{
                Yii::$app->session->setFlash('error', \Yii::t('backend', 'Error save'));
            }
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }


    public function actionDisable($id)
    {
        $model = Apartment::find()->where(['id' => $id, 'user_id' => Yii::$app->user->id])->one();
        if($model === null){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model->status = Apartment::STATUS_ARCHIVE;

        if($model->save()) {
            Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success save'));
        } else {
            Yii::$app->session->setFlash('error', \Yii::t('backend', 'Error save'));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionApproved($id)
    {
        $model = Apartment::find()->where(['id' => $id, 'user_id' => Yii::$app->user->id])->one();
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if ($model->valid()) {
            $model->status = Apartment::STATUS_APPROVED;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success save'));
            } else {
                Yii::$app->session->setFlash('error', \Yii::t('backend', 'Error save'));
            }
        } else {
            Yii::$app->session->setFlash('error', \Yii::t('backend', 'Error save'));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionPosition(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = [];
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            /**
             * @var $model \common\models\Apartment
             */
            $model = Apartment::find()->with('apartmentPhotos')->where(['id' => $data['id'], 'user_id' => Yii::$app->user->id])->one();
            if ($model === null) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            foreach ($model->apartmentPhotos as $photo) {
                foreach($data['position'] as $key=>$value){
                    if($value == $photo->id){
                        $photo->position = $key;
                        $photo->save();
                        continue;
                    }
                }
            }

            $result = [
                'code' => 100,
            ];
        }
        return $result;
    }


    public function actionPhotodelete($id)
    {
        $model = ApartmentPhoto::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $apartmentId = $model->apartment_id;
        @unlink(Yii::$app->params['uploadOrig'] . $apartmentId . '/' . $model->name);
        @unlink(Yii::$app->params['uploadLarge'] . $apartmentId . '/' . $model->name);
        @unlink(Yii::$app->params['uploadSmall'] . $apartmentId . '/' . $model->name);
        $model->delete();
        Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success save'));
        return $this->redirect(['/cabinet/apartment/edit', 'id' => $apartmentId]);
    }

    public function actionPhotoRotate($id, $rotate)
    {
        $result = ['code' => 'danger', 'message' => 'Страница не найдена'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isAjax) {
            return $result;
        }
        $data = Yii::$app->request->post();

        $model = ApartmentPhoto::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model->rotate($rotate);

        $result['code'] = 'success';
        $result['message'] = Yii::t('backend', 'Success save');
        return $result;
    }
}
