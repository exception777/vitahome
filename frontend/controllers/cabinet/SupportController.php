<?php

namespace frontend\controllers\cabinet;

use common\models\Support;
use common\models\SupportMessage;
use frontend\core\CabinetController;
use frontend\models\SupportForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii;
use yii\data\Pagination;

class SupportController extends CabinetController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    public function actionList()
    {
        $query = Support::find()->with([
            'supportMessages' => function($query) {
                $query->andWhere('status = :status AND user_id != :user',
                    [':status' => SupportMessage::STATUS_NEW, ':user' => Yii::$app->user->id]);
            }
        ])
            ->where(['user_id' => \Yii::$app->user->identity->id])
            ->orderBy(['status' => SORT_DESC, 'id' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
        $supports = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'supports' => $supports,
            'pages' => $pages,
        ]);
    }

    public function actionAdd()
    {
        $model = new SupportForm();

        $post = \Yii::$app->request->post();
        if($model->load($post)) {
            if ($model->validate()) {
                $support = new Support();
                $support->title = $model->title;
                $support->user_id = Yii::$app->user->id;
                $support->status = Support::STATUS_OPEN;
                if($support->save()) {
                    $message = new SupportMessage();
                    $message->message = $model->message;
                    $message->user_id = Yii::$app->user->id;
                    $message->support_id = $support->id;
                    $message->status = SupportMessage::STATUS_NEW;
                    $message->save();
                    Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success save'));
                    return $this->redirect(['view', 'id' => $support->id]);
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('frontend', 'Error save'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('frontend', 'Error save'));
            }
        }
        return $this->render('add', [
            'model' => $model
        ]);
    }

    public function actionView($id)
    {
        $support = Support::find()->with(['supportMessages'])
            ->where(['id' => $id, 'user_id' => Yii::$app->user->id])->one();
        if($support === null)
            throw new NotFoundHttpException('Not Found');

        foreach ($support->supportMessages as $supportMessage) {
            if($supportMessage->user_id != $support->user_id && $supportMessage->status == SupportMessage::STATUS_NEW){
                Yii::$app->db->createCommand('UPDATE support_message SET status = :status WHERE support_id = :id AND user_id <> :userId',
                    [':status' => SupportMessage::STATUS_DEFAULT, 'id' => $support->id, ':userId' => $support->user_id])->execute();
                break;
            }
        }
        $message = new SupportMessage();
        $post = \Yii::$app->request->post();
        if($message->load($post)) {
            $message->support_id = $support->id;
            $message->user_id = Yii::$app->user->id;
            $message->status = SupportMessage::STATUS_NEW;
            if ($message->save()) {
                Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success save'));
                return $this->redirect(['view', 'id' => $support->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('frontend', 'Error save'));
            }
        }
        return $this->render('view', [
            'support' => $support,
            'message' => $message
        ]);
    }

    public function actionClose($id)
    {
        $support = Support::find()
            ->where(['id' => $id, 'user_id' => Yii::$app->user->id])->one();
        if($support === null)
            throw new NotFoundHttpException('Not Found');

        $support->status = Support::STATUS_CLOSE;
        if ($support->save()) {
            Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success save'));
            return $this->redirect(['list']);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('frontend', 'Error save'));
        }
        return $this->redirect(['view', 'id' => $support->id]);
    }


}
