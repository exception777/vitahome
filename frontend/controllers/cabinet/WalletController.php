<?php
namespace frontend\controllers\cabinet;

use Yii;
use common\models\WalletTransaction;
use frontend\core\CabinetController;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class WalletController extends CabinetController{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function actionRefill()
    {

        return $this->render('refill', [
        ]);
    }

    public function actionTransaction()
    {
        $query = WalletTransaction::find()->where(['wallet_id' => $this->wallet->id])->orderBy(['id' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 20]);

        $transactions = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('transaction', [
            'transactions' => $transactions,
            'pages' => $pages
        ]);
    }


}