<?php
namespace frontend\controllers\cabinet;

use Yii;
use common\models\Apartment;
use common\models\Service;
use common\models\Wallet;
use common\models\WalletTransaction;
use dektrium\user\models\User;
use frontend\core\CabinetController;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;

class AccountController extends CabinetController{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function actionAds()
    {
        $query = Apartment::find()->with(['city', 'apartmentPhotos'])
            ->where(['user_id' => \Yii::$app->user->identity->id, 'status' => Apartment::STATUS_APPROVED])
            ->orderBy(['up_at' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
        $apartments = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $countArchive = Apartment::find()
            ->where(['user_id' => \Yii::$app->user->identity->id,
                'status' => [Apartment::STATUS_DISABLE, Apartment::STATUS_MODERATE, Apartment::STATUS_ARCHIVE]])
            ->count();
        $services = Service::find()->where(['status' => 1])->all();
        return $this->render('index', [
            'apartments' => $apartments,
            'pages' => $pages,
            'countArchive' => $countArchive,
            'services' => $services
        ]);
    }

    public function actionArchive()
    {
        $query = Apartment::find()->with(['city', 'apartmentPhotos'])
            ->where(['user_id' => \Yii::$app->user->identity->id,
                'status' => [Apartment::STATUS_DISABLE, Apartment::STATUS_MODERATE, Apartment::STATUS_ARCHIVE]])
            ->orderBy(['id' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
        $apartments = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $countApproved = Apartment::find()
            ->where(['user_id' => \Yii::$app->user->identity->id, 'status' => Apartment::STATUS_APPROVED])
            ->count();

        return $this->render('archive', [
            'apartments' => $apartments,
            'pages' => $pages,
            'countApproved' => $countApproved
        ]);
    }

    public function actionService($id, $serviceId)
    {
        $result = ['type' => 'danger', 'message' => 'Страница не найдена'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isAjax) {
            return $result;
        }

        $apartment = Apartment::find()
            ->where(['id' => $id, 'user_id' => Yii::$app->user->identity->id, 'status' => Apartment::STATUS_APPROVED])
            ->one();
        if (!$apartment) {
            $result['type'] = 'danger';
            $result['message'] = 'Объявление не найдено.';
            return $result;
        }
        $service = Service::find()->where(['id' => $serviceId, 'status' => 1])->one();
        if (!$service) {
            $result['type'] = 'danger';
            $result['message'] = 'Услуга не найдена.';
            return $result;
        }
        $wallet = Wallet::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
        if (!$wallet) {
            $result['type'] = 'danger';
            $result['message'] = 'Личный счет не найдена.';
            return $result;
        }

        $price = Yii::$app->user->identity->isRole(['admin']) ? 0 : $service->price;

        if ($wallet->balance < $price) {
            $result['type'] = 'warning';
            $result['message'] = 'Не достаточно средств на счету.';
            return $result;
        }

        if ($service->id == 1 || $service->id == 2) {
            $wallet->balance = $wallet->balance - $price;
            $wallet->save();
            Yii::$app->cache->delete(Yii::t('cache', 'wallet', ['id' => Yii::$app->user->id]));

            $transaction = new WalletTransaction();
            $transaction->wallet_id = $wallet->id;
            $transaction->amount = $price;
            $transaction->comment = $service->name;
            $transaction->status = WalletTransaction::PAY_SERVICE;
            $transaction->save();

            if ($service->id == 1) {
                $apartment->up_at = date('Y-m-d H:i:s');
            }

            if ($service->id == 2) {
                $newExpiredAt = new \DateTime();
                $newExpiredAt->modify('+1 month');
                $apartment->expired_at = $newExpiredAt->format('Y-m-d H:i:s');
                $apartment->status = Apartment::STATUS_APPROVED;
            }
            $apartment->save();

            $result['type'] = 'success';
            $result['message'] = 'Оплата прошла успешно.';
        }

        return $result;

    }
}