<?php
namespace frontend\controllers;

use common\models\Area;
use Yii;
use frontend\core\BaseController;
use yii\filters\VerbFilter;
use \yii\web\BadRequestHttpException;

/**
 * Ajax controller
 */
class AjaxController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'areas' => ['post'],
                ],
            ],
        ];
    }

    public function actionAreas($id)
    {
        if (Yii::$app->request->isAjax) {

            $areas = Area::find()
                ->where(['city_id' => $id])
                ->orderBy('name DESC')
                ->all();

            $options = "<option>-</option>";

            if($areas){
                $options = '';
                foreach($areas as $area){
                    $options .= "<option value='".$area->id."'>".$area->name."</option>";
                }
            }

            $response = Yii::$app->response;
            $response->format = \yii\web\Response::FORMAT_JSON;
            $response->data = ['options' => $options];
            $response->statusCode = 200;
            return $response;
        }
        else throw new BadRequestHttpException;
    }
}
