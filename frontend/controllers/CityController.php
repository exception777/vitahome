<?php
namespace frontend\controllers;

use common\models\Apartment;
use common\models\City;
use Yii;
use frontend\core\BaseController;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class CityController extends BaseController
{
    public function actionIndex($cityAlias)
    {
        $city = Yii::$app->cache->get('city-' . $cityAlias);
        if ($city === false) {
            $city = City::findOne(['alias' => $cityAlias, 'status' => City::ENABLE]);
            Yii::$app->cache->set('city-' . $cityAlias, $city, Yii::$app->params['cacheTime']['day']);
        }

        if (is_null($city)) {
            throw new NotFoundHttpException(\Yii::t('frontend', 'Not Found'));
        }

        $query = Apartment::find()
            ->with(['apartmentPhotos'])
            ->where([
                'city_id' => $city->id,
                'status' => Apartment::STATUS_APPROVED
            ]);

        if ($city->id == 1) {
            $query->andWhere('expired_at > :nowDate', [':nowDate' => date('Y-m-d H:i:s')]);
        }

        if (is_numeric(Yii::$app->request->get('area'))) {
            $query->andWhere(['area_id' => Yii::$app->request->get('area')]);
        }
        if (is_numeric(Yii::$app->request->get('rooms'))) {
            if (Yii::$app->request->get('rooms') < 4) {
                $query->andWhere(['room' => Yii::$app->request->get('rooms')]);
            } else {
                $query->andWhere(['>=', 'room', Yii::$app->request->get('rooms')]);
            }
        }
        if (is_numeric(Yii::$app->request->get('price_from')) && is_numeric(Yii::$app->request->get('price_to'))) {
            $query->andWhere(['>=', 'price', Yii::$app->request->get('price_from')])
                ->andWhere(['<=', 'price', Yii::$app->request->get('price_to')]);
        } else {
            if (is_numeric(Yii::$app->request->get('price_from'))) {
                $query->andWhere(['>=', 'price', Yii::$app->request->get('price_from')]);
            }
            if (is_numeric(Yii::$app->request->get('price_to'))) {
                $query->andWhere(['<=', 'price', Yii::$app->request->get('price_to')]);
            }
        }

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize' => 10,
            'forcePageParam' => false
        ]);
        $apartments = $query->orderBy(['up_at' => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'city' => $city,
            'apartments' => $apartments,
            'pages' => $pages,
        ]);
    }

    public function actionMap($cityAlias)
    {
        $city = Yii::$app->cache->get('city-' . $cityAlias);
        if ($city === false) {
            $city = City::findOne(['alias' => $cityAlias, 'status' => City::ENABLE]);
            Yii::$app->cache->set('city-' . $cityAlias, $city, Yii::$app->params['cacheTime']['day']);
        }

        if (is_null($city)) {
            throw new NotFoundHttpException(\Yii::t('frontend', 'Not Found'));
        }

        $query = Apartment::find()
            ->with(['apartmentPhotos'])
            ->where([
                'city_id' => $city->id,
                'status' => Apartment::STATUS_APPROVED
            ]);

        if ($city->id == 1) {
            $query->andWhere('expired_at > :nowDate', [':nowDate' => date('Y-m-d H:i:s')]);
        }

        if (is_numeric(Yii::$app->request->get('area'))) {
            $query->andWhere(['area_id' => Yii::$app->request->get('area')]);
        }
        if (is_numeric(Yii::$app->request->get('rooms'))) {
            if (Yii::$app->request->get('rooms') < 4) {
                $query->andWhere(['room' => Yii::$app->request->get('rooms')]);
            } else {
                $query->andWhere(['>=', 'room', Yii::$app->request->get('rooms')]);
            }
        }
        if (is_numeric(Yii::$app->request->get('price_from')) && is_numeric(Yii::$app->request->get('price_to'))) {
            $query->andWhere(['>=', 'price', Yii::$app->request->get('price_from')])
                ->andWhere(['<=', 'price', Yii::$app->request->get('price_to')]);
        } else {
            if (is_numeric(Yii::$app->request->get('price_from'))) {
                $query->andWhere(['>=', 'price', Yii::$app->request->get('price_from')]);
            }
            if (is_numeric(Yii::$app->request->get('price_to'))) {
                $query->andWhere(['<=', 'price', Yii::$app->request->get('price_to')]);
            }
        }

        $apartments = $query->all();

        return $this->render('map', [
            'city' => $city,
            'apartments' => $apartments,
        ]);
    }

    public function actionCategory($cityAlias, $categoryAlias)
    {
        if ($categoryAlias !== 'kvartiry-posutochno') {
            throw new NotFoundHttpException(\Yii::t('frontend', 'Not Found'));
        }

        $city = Yii::$app->cache->get('city-category-' . $cityAlias . '-' . $categoryAlias);
        if ($city === false) {
            $city = City::findOne(['alias' => $cityAlias, 'status' => City::ENABLE]);
            Yii::$app->cache->set('city-category-' . $cityAlias . '-' . $categoryAlias, $city, 3600 * 24);
        }

        if (is_null($city)) {
            throw new NotFoundHttpException(\Yii::t('frontend', 'Not Found'));
        }

        $query = Apartment::find()->with(['apartmentPhotos'])->where(['city_id' => $city->id, 'status' => Apartment::STATUS_APPROVED]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 5]);
        $apartments = $query->orderBy(['updated_at' => SORT_DESC])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('category', [
            'apartments' => $apartments,
            'pages' => $pages,
        ]);
    }
}
