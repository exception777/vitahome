<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/font-awesome/css/font-awesome.min.css',
        'plugins/swiper/swiper-bundle.min.css',
        'css/site.css?v=1.2.2',
    ];
    public $js = [
        'plugins/swiper/swiper-bundle.min.js',
        'js/vitahome.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
