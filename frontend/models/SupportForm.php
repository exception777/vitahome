<?php
namespace frontend\models;

use yii\base\Model;

class SupportForm extends Model
{
    public $title;
    public $message;

    public function rules()
    {
        return [
            [['title', 'message'], 'required'],
            [['title', 'message'], 'trim'],
            [['message'], 'safe'],
            [['title'], 'string', 'length' => [8, 256]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => \Yii::t('frontend', 'Title'),
            'message' => \Yii::t('frontend', 'Message'),
        ];
    }
}