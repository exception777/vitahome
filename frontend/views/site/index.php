<?php
use yii\helpers\Html;

/* @var $this yii\web\View
 * @var $apartments \common\models\Apartment
 * @var $city \common\models\City
 */
$this->title = 'Квартиры посуточно в Астане';

$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs('$(".carousel").carousel({interval: 6000})');

$this->params['description'] = $city->description;
$this->params['keywords'] = $city->keywords;
?>

<section class="home-header" id="homepage-header">
    <div id="hero-img-wrapper" class="hero-img-wrapper">
        <div class="hero-image">
            <img alt="vitahome.kz главная страница"
                 src="/images/slider/cada0d931c8dc788fbe14f536728b72cf30b13a1.jpeg">
        </div>
    </div>
    <div class="container home-header-content">
        <div class="row">
            <header class="header-group header-group-white text-center" id="hero-header-text">
                <h1>
                    <span>Квартиры</span>
                    <span>посуточно</span>
                </h1>
                <p class="header-section-secondary text-center hidden-xs hidden-xxs">Поиск квартир в аренду
                    посуточно</p>
            </header>
            <div class="home-search-wrapper">
                <div id="filter-location" class="filter-main-location">
                    <div class="filter-item-locale">
                        <div class="text-locale">Выберите город...</div>
                        <div class="location-caret"><i class="fa fa-chevron-down"></i></div>
                        <ul class="locale-list">
                            <?php
                            foreach ($this->params['cities'] as $value) {
                                echo '<li>'.Html::a($value['name'], ['city/index', 'cityAlias' => $value['alias']]).'</li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <header class="header-group">
        <h2 class="header-section text-center">
            <span>Астана</span>
        </h2>
    </header>

    <ul class="nav nav-pills nav-justified" style="margin: 10px 0">
        <li>
            <?= Html::a('Списком', ['city/index', 'cityAlias' => 'astana'])?>
        </li>
        <li>
            <?= Html::a('На карте', ['city/map', 'cityAlias' => 'astana']) ?>
        </li>
    </ul>

    <div class="row">
        <?php foreach ($apartments as $apartment): ?>
            <?=$this->render('@frontend/views/apartment/_item_small', ['apartment' => $apartment])?>
        <?php endforeach; ?>
    </div>
    <?php
    if ($apartments) {
        $features = [];
        $center = '';
        foreach ($apartments as $apartment) {
            if (!$apartment->maps) continue;
            if (!$center)
                $center = $apartment->maps;
            $photo = '';
            if ($apartment->apartmentPhotos) {
                $photo = Html::a(Html::img('/images/apartments/small/' . $apartment->id . '/' .
                    $apartment->apartmentPhotos[0]->name, ['class' => "img-responsive img-rounded", 'style' => 'float:left;padding-right:10px;width: 100px']),
                    ['apartment/index', 'id' => $apartment->id], ['title' => $apartment->name]);
            }
            $features[] = '{
                "type": "Feature",
                "id": ' . $apartment->id . ',
                "geometry": {
                    "type": "Point",
                    "coordinates": [' . $apartment->maps . ']
                },
                "properties": {
                    "balloonContent": \'' . $photo . '<h4 class="apartment-header maps-header">' . Html::a($apartment->name,
                    ['apartment/index', 'id' => $apartment->id], ['title' => $apartment->name]) . '</h4>\'+
                        \'<p>Цена: ' . $apartment->price . ' тенге</p>\',
                    "clusterCaption": "' . $apartment->name . '",
                    "hintContent": "' . $apartment->name . '"
                }
            }';
        }
        $jsonJs = '{
            "type": "FeatureCollection",
            "features": [
                ' . implode(',', $features) . '
            ]
        }';
        $this->registerJs('
            ymaps.ready(init);
            var myMap;

            function init(){
                var objectManager = new ymaps.ObjectManager({clusterize: true});
                objectManager.add(' . $jsonJs . ');
                myMap = new ymaps.Map("map", {
                    center: [' . $center . '],
                    zoom: 14
                });
                myMap.geoObjects.add(objectManager);
            }
        ');
        echo '<div id="map" class="border-gray" style=" height: 400px"></div>';
    }
    ?>
    <div class="row" style="padding: 30px 0;">
        <div class="col-sm-12">
            <p><strong>Аренда квартир посуточно</strong></p>
            <div class="row">
                <?php foreach ($this->params['cities'] as $value) :?>
                    <div class="col-sm-4"><?= Html::a($value->title, ['city/index', 'cityAlias' => $value['alias']], ['title' => $value->title]) ?></div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
