<?php

use yii\helpers\Html;

/* @var $this yii\web\View
 * @var $page \common\models\Page
 */
$this->title = $page['title'];
$this->params['description'] = $page['description'];
$this->params['keywords'] = $page['keywords'];
?>

<?= $this->render('../layouts/filters') ?>

<div class="container">
    <h1><?=$page['title']?></h1>

    <?=$page['text']?>

</div>