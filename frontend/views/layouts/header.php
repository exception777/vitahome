<?php
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\bootstrap\Html;
use yii\helpers\Url;

?>
<header class="header">
    <?php
    $itemsMenu = [
        [
            'label' => 'Квартиры посуточно',
            'url' => Yii::$app->getHomeUrl(),
            'active' => Yii::$app->controller->route === 'site/index'
        ]
    ];
    if(isset($this->params['pages'])){
        foreach($this->params['pages'] as $value){
            $itemsMenu[] = [
                'label' => $value['title'],
                'url' => '/page/' . $value['alias'],
                'active' => Url::to() === '/page/' . $value['alias']
            ];
        }
    }
    if (Yii::$app->user->isGuest){
        $itemsMenuRight =[
            ['label' => '<i class="fa fa-sign-in" aria-hidden="true"></i> ' . Yii::t('frontend', 'Login'), 'url' => '/user/security/login']
        ];
    }else{
        $itemsMenuRight = [
            [
                'label' => Yii::t('frontend', 'Cabinet'),
                'items' => [
                    [
                        'label' => '<i class="fa fa-plus" aria-hidden="true"></i> ' . Yii::t('frontend', 'Create Ads'),
                        'url' => '/cabinet/apartment/create'
                    ],
                    [
                        'label' => '<i class="fa fa-dashboard" aria-hidden="true"></i> ' . Yii::t('frontend', 'Ads'),
                        'url' => '/cabinet/account/ads'
                    ],
                    [
                        'label' => '<i class="fa fa-sign-out" aria-hidden="true"></i> ' . Yii::t('frontend', 'Logout'),
                        'url' => '/user/security/logout',
                        'linkOptions' => ['data-method' => 'post']
                    ],
                ]
            ]
        ];
    }

    NavBar::begin(['brandLabel' => 'VitaHome', 'id' => 'navbar-main-menu', 'options' => ['class' => 'navbar-main-menu']]);
    echo Nav::widget([
        'items' => $itemsMenu,
        'options' => ['class' => 'navbar-nav'],
    ]);
    echo Nav::widget([
        'items' => $itemsMenuRight,
        'encodeLabels' => false,
        'options' => ['class' => 'navbar-nav navbar-right'],
    ]);
    NavBar::end();
    ?>

</header>

