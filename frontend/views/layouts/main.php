<?php
use yii\helpers\Html;
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="KJDatXsDVY91BtPYQxCf5hLOCSpgIamL_keW-gb3LxE" />
    <meta name="yandex-verification" content="851a589babff5dec" />
    <meta name="yandex-verification" content="46907f5657700faf" />
    <meta name="description" content="<?php if(isset($this->params['description'])) echo $this->params['description'];?>">
    <meta name="keywords" content="<?php if(isset($this->params['keywords'])) echo $this->params['keywords'];?>">
    <meta name="verify-admitad" content="35d71a3372" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125403460-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-125403460-3');
    </script>
</head>
<body>
    <?php $this->beginBody() ?>

    <?=$this->render('header')?>

    <?php
    if (array_search(Yii::$app->controller->id, ['cabinet/account', 'cabinet/apartment', 'cabinet/wallet',
            'cabinet/support', 'settings']) !== false) {
        echo $this->render(
            'content_cabinet',
            ['content' => $content]
        );
    } else {
        echo $this->render('content', ['content' => $content]);
    }
    ?>

    <?=$this->render('footer')?>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
