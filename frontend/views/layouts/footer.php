
<footer class="footer">
    <div class="footer-wrapper-copyright">
        <div class="container">
            <p id="footer-copyright">
                ©2015-<?=date('Y')?> <a href="http://www.vitahome.kz">Сайт каталог vitahome.kz</a> Все права защищены.<br>
            </p>
        </div>
    </div>
</footer>