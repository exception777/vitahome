<?php
use yii\bootstrap\Html;
use frontend\helpers\FrontendHelper;

$areas = [];
if (!isset($city)) {
    $city = new \common\models\City();
} else {
    $helper = new FrontendHelper();
    $areas = $helper->getCityAreas($city->id);
}
?>
<div class="filter-bar">
    <div class="container">
        <form id="search-form" action="<?= \yii\helpers\Url::to(['city/index', 'cityAlias' => $city->alias])?>" method="get">
            <div id="filter-location" class="filter-location float-left">
                <div class="filter-item-locale">
                    <div class="text-locale"><?= $city->name?: 'Выберите город'?></div>
                    <div class="location-caret"><i class="fa fa-chevron-down"></i></div>
                    <ul class="locale-list">
                        <?php
                        foreach ($this->params['cities'] as $value) {
                            echo '<li>'.Html::a($value['name'], ['city/index', 'cityAlias' => $value['alias']]).'</li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="filter-options-list float-left">
                <div class="float-left filter-item">
                    <div class="filter-name">Район</div>
                    <div class="filter-input">
                        <?= Html::dropDownList('area', Yii::$app->request->get('area'), $areas,
                            ['class' => 'form-control input-sm', 'prompt' => 'Выберите район'])?>
                    </div>
                </div>
                <div class="float-left filter-item">
                    <div class="filter-name">Количество комнат</div>
                    <div class="filter-input">
                        <?= Html::dropDownList('rooms', Yii::$app->request->get('rooms'),
                            [1 => '1 комната', 2 => '2 комнаты', 3 => '3 комнаты', 4 => '4 комнаты и более'],
                            ['class' => 'form-control input-sm', 'prompt' => 'Выберите из списка'])?>
                    </div>
                </div>
                <div class="float-left filter-item">
                    <div class="filter-name">Цена, KZT</div>
                    <div class="filter-input">
                        <?= Html::textInput('price_from', Yii::$app->request->get('price_from'),
                            ['type'=> "number", 'class' => "form-control input-sm filter-price-from", 'placeholder' => "от", 'maxlength' => "6"])?>
                        <?= Html::textInput('price_to', Yii::$app->request->get('price_to'),
                            ['type'=> "number", 'class' => "form-control input-sm filter-price-to", 'placeholder' => "до", 'maxlength' => "6"])?>
                    </div>
                </div>
            </div>
            <div class="float-right">
                <button type="submit" class="btn btn-default filter-btn-search"><i class="fa fa-search"></i> Поиск</button>
            </div>
        </form>
    </div>
</div>

<?php
$this->registerJs(<<<JS
    $("#search-form").submit(function() {
        console.log($(this));
        $(this).find(':input').filter(function() { return !this.value; }).attr('disabled', 'disabled');
        return true;
    });
JS
);
?>