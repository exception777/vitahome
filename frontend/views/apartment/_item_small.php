<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;

/** @var $apartment \common\models\Apartment */
?>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="apartment">
        <div class="apartment-id">№ <?= str_pad($apartment->id, 4, '0', STR_PAD_LEFT); ?></div>
        <div class="apartment-photo">
            <?php if ($apartment->apartmentPhotos): ?>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php foreach ($apartment->apartmentPhotos as $apartmentPhoto): ?>
                            <?php $image = Yii::$app->thumbnail->url(Yii::getAlias('@webroot/images/apartments/large/') . $apartment->id . '/' . $apartmentPhoto->name, [
                                'thumbnail' => [
                                    'width' => 480,
                                    'height' => 320,
                                ]
                            ]); ?>

                            <div class="swiper-slide">
                                <?= Html::a('<img data-src="'.$image.'" class="swiper-lazy">',
                                    ['apartment/index', 'id' => $apartment->id],
                                    ['title' => $apartment->name]
                                ) ?>
                            </div>
                        <?php endforeach ?>
                    </div>

                    <div class="swiper-pagination"></div>

                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            <?php endif  ?>

            <h3 class="apartment-header">
                <?= Html::a($apartment->name, ['apartment/index', 'id' => $apartment->id], ['title' => $apartment->name]) ?>
            </h3>
        </div>

        <?php if (!empty($apartment->discount_price)): ?>
            <div class="apartment-discount-label"></div>
        <?php endif ?>

        <?php if (!empty($apartment->is_free_now)): ?>
            <div class="apartment-free-now-label">
                Свободна сейчас
            </div>
        <?php endif ?>

        <div class="apartment-info">
            <?php if (!empty($apartment->discount_price)): ?>
                <div class="apartment-info-price">
                    <span class="apartment-info-discount-price">
                        <?= Yii::$app->formatter->asCurrency($apartment->discount_price) ?>
                    </span>
                    <span class="apartment-info-price-old">
                        <?= Yii::$app->formatter->asCurrency($apartment->price) ?>
                    </span>
                </div>
            <?php else: ?>
                <div class="apartment-info-price">
                    <?= Yii::$app->formatter->asCurrency($apartment->price) ?>
                </div>
            <?php endif?>
            <div class="apartment-info-meta">
                <?= $apartment->address ?>
            </div>
            <div class="apartment-info-meta hidden-xs">
                <?php echo Html::encode(StringHelper::truncateWords($apartment->text, 17, '...')); ?>
            </div>
            <div class="apartment-info-meta visible-xs visible-sm">
                <?php
                $phones = explode(',', $apartment->phone);
                foreach ($phones as $phone) {
                    echo '<a href="tel:' . $phone . '" class="btn btn-default btn-block"><i class="fa fa-phone"></i> ' . $phone . '</a>';
                }
                ?>
            </div>
        </div>
    </div>
</div>