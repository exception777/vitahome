<?php
/* @var $this yii\web\View */
/* @var $apartment common\models\Apartment */
$this->title = Yii::t('frontend', 'Apartment title', [
        'room' => $apartment->room,
        'city' => $apartment->city->name,
        'area' => $apartment->area->name,
        'price' => Yii::$app->formatter->asCurrency($apartment->price),
    ]);

$this->params['description'] = \yii\helpers\StringHelper::truncateWords($apartment->text, 15);
$this->params['keywords'] = Yii::t('frontend', 'Apartment keywords', [
    'room' => $apartment->room,
    'city' => $apartment->city->name,
    'area' => $apartment->area->name,
    'price' => Yii::$app->formatter->asCurrency($apartment->price),
]);

$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU', ['depends' => [\yii\web\JqueryAsset::className()]]);
if ($apartment->maps) {
    $this->registerJs('
        ymaps.ready(init);
        var myMap;

        function init(){
            myMap = new ymaps.Map("map", {
                center: ['.$apartment->maps.'],
                zoom: 14
            });
            myPlacemark = new ymaps.Placemark(['.$apartment->maps.'], {
                hintContent: "'.$apartment->name.'",
                balloonContent: "'.$apartment->address.'"
            });

            myMap.geoObjects.add(myPlacemark);
        }
    ');
}

$this->registerCssFile('/plugins/owlcarousel/assets/owl.carousel.min.css');
$this->registerCssFile('/plugins/owlcarousel/assets/owl.theme.default.min.css');
$this->registerJsFile('/plugins/owlcarousel/owl.carousel.min.js', ['position' => \yii\web\View::POS_HEAD, 'depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="container">
    <?php if($apartment->apartmentPhotos):?>
        <div class="apartment-photo-section">
            <div class="owl-carousel owl-theme">
                <?php foreach($apartment->apartmentPhotos as $photo):?>
                <div class="item">
                    <img data-src="/images/apartments/large/<?=$apartment->id?>/<?=$photo->name?>"
                         src="/images/apartments/large/<?=$apartment->id?>/<?=$photo->name?>"/>
                </div>
                <?php endforeach?>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $(".owl-carousel").owlCarousel({
                        margin:30,
                        center: true,
                        loop:true,
                        items: <?= Yii::$app->devicedetect->isMobile() ? 1:1?>,
                        autoHeight: true,
                        autoWidth: false,
                        nav: true,
                        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
                        dots: false
                    });
                });
            </script>

            <?php if (!empty($apartment->discount_price)): ?>
                <div class="apartment-discount-label"></div>
            <?php endif ?>

            <?php if (!empty($apartment->is_free_now)): ?>
                <div class="apartment-free-now-label">
                    Свободна сейчас
                </div>
            <?php endif ?>
        </div>
    <?php endif;?>

    <div class="row margin-bottom">
        <div class="col-lg-9 col-md-8">
            <div class="apartment-section">
                <div class="row">
                    <div class="col-md-6">
                        <small>№ <?=str_pad($apartment->id, 4, '0', STR_PAD_LEFT)?></small>
                        <div class="text-muted"><?=$apartment->city->name?>, <?=$apartment->area->name?></div>
                    </div>
                    <div class="col-md-6 text-right">
                        <?php if (!empty($apartment->discount_price)): ?>
                            <span class="apartment-price-old">
                                <?= Yii::$app->formatter->asCurrency($apartment->price) ?>
                            </span>
                            <span class="apartment-discount-price">
                                <?= Yii::$app->formatter->asCurrency($apartment->discount_price) ?>
                            </span>
                        <?php else: ?>
                            <span class="apartment-price">
                                <?= Yii::$app->formatter->asCurrency($apartment->price) ?>
                            </span>
                        <?php endif?>
                    </div>
                </div>
            </div>

            <h1 class="title-section apartment-section"><?=$apartment->name?></h1>
            <p><?=$apartment->text?></p>

            <div class="row">
                <div class="col-sm-6">
                    <div><strong>Комнат:</strong> <?=$apartment->room?></div>
                        <?php if($apartment->square):?>
                            <div><strong>Площадь:</strong> <?=$apartment->square?> м<sup>2</sup></div>
                        <?php endif;?>
                        <?php if($apartment->sleep):?>
                            <div><strong>Спальных мест:</strong> <?=$apartment->sleep?></div>
                        <?php endif;?>
                </div>
                <div class="col-sm-6">
                    <div><strong>Этаж:</strong> <?=$apartment->floor?> </div>
                    <div><strong>Этажность:</strong> <?=$apartment->floor_total?></div>
                </div>
            </div>
        </div>
        <aside class="col-lg-3 col-md-4">
            <?php if ($apartment->status == \common\models\Apartment::STATUS_APPROVED): ?>
                <div>
                    <strong>Контакты</strong>
                    <?php
                        $phones = explode(',', $apartment->phone);
                        foreach ($phones as $phone) {
                            echo '<p><a href="tel:'.$phone.'" class="btn btn-default btn-block">
                            <i class="fa fa-phone"></i> ' . $phone .'</a></p>';
                        }
                    ?>
                </div>
                <?php if ($apartment->email): ?>
                    <p>
                        <a href="mailto: <?=$apartment->email?>" class="btn btn-default btn-block">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i> <?=$apartment->email?>
                        </a>
                    </p>
                <?php endif ?>
            <?php endif ?>
        </aside>
    </div>

    <?php if($apartment->maps):?>
        <div id="map" class="border-gray" style="height: 400px"></div>
    <?php endif?>
</div>