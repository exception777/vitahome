<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
<div class="container">
<div class="row padding-top">
    <div class="col-xs-6">
        <?=Html::a('<i class="fa fa-plus"></i> ' . Yii::t('frontend', 'Create Ads'),
            ['/cabinet/apartment/create'], ['class' => 'add-btn-block'])?>
    </div>
    <div class="col-xs-6 text-right">
        <?php Pjax::begin(['id' => 'wallet-balance']);?>
            <?php if(isset($this->params['wallet'])):?>
            <div class="wallet-block">
                <div class="wallet-block-balance"><?=Yii::t('frontend', 'Wallet balance', [$this->params['wallet']->balance])?></div>
                <div class="wallet-refill"><?=Html::a(Yii::t('frontend', 'Refill'), ['/cabinet/wallet/refill'], ['data-pjax' => 0])?></div>
            </div>
            <?php endif;?>
        <?php Pjax::end()?>
    </div>
</div>

<ul class="nav nav-tabs" style="margin-bottom: 10px">
    <li role="presentation" class="<?= Yii::$app->controller->id === 'cabinet/account'?'active':'' ?>">
        <?=Html::a(Yii::t('frontend', 'Ads'), ['/cabinet/account/ads'])?>
    </li>
    <li role="presentation" class="<?= Yii::$app->controller->route === 'cabinet/wallet/transaction'?'active':'' ?>">
        <?=Html::a(Yii::t('frontend', 'Personal account'), ['/cabinet/wallet/transaction'])?>
    </li>
    <li role="presentation" class="<?= Yii::$app->controller->id === 'cabinet/support'?'active':'' ?>">
        <?=Html::a(Yii::t('frontend', 'Support'), ['/cabinet/support/list'])?>
    </li>
    <li role="presentation" class="<?= Yii::$app->controller->id === 'settings' ?'active':'' ?>">
        <?=Html::a(Yii::t('frontend', 'Personal data'), ['/user/settings/profile'])?>
    </li>
</ul>
</div>