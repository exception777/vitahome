<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \common\models\SupportMessage;

/* @var $this yii\web\View
 * @var $support \common\models\Support
 * @var $message \common\models\SupportMessage
 */

$this->title = $support->title;

?>

<div class="container">

<div class="h3"><?=$support->title?></div>
<?php if($support->supportMessages):?>
    <?php
    $countMessage = count($support->supportMessages);
    $i = 0;
    foreach ($support->supportMessages as $supportMessage):?>
        <div class="support-message <?=$supportMessage->user_id == Yii::$app->user->id?'message-right':
            ($supportMessage->status == SupportMessage::STATUS_NEW?'message-new':'')?>">
            <div class="message"><?=$supportMessage->message?></div>
            <div class="message-date"><?=Yii::$app->formatter->asDatetime($supportMessage->created_at, 'dd MMM YYYY, HH:mm')?></div>
        </div>
        <?php if (++$i != $countMessage): ?><div class="support-delimiter"></div><?php endif;?>
    <?php endforeach;?>
<?php else:?>
    <p class="text-center text-muted"><?=Yii::t('frontend', 'Not record')?></p>
<?php endif;?>
<?php if($support->status == \common\models\Support::STATUS_OPEN):?>
    <?php $form = ActiveForm::begin([
        'id' => 'apartment-create',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => 'col-sm-offset-3',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

        <?=$form->errorSummary($message)?>
        <?=$form->field($message, 'message')->textarea(['rows' => 6]) ?>


    <div class="box-footer">
        <div class="row">
            <div class="col-sm-3 col-xs-12">
            </div>
            <div class="col-sm-4 col-xs-6">
                <?= Html::submitButton('<i class="fa fa-paper-plane"></i> ' . \Yii::t('frontend', 'Send'),
                    ['class' => ' btn btn-success']) ?>
            </div>
            <div class="col-sm-5 col-xs-6 text-right">
                <?= Html::a('<i class="fa fa-times"></i> ' . \Yii::t('frontend', 'Closed'), ['close', 'id' => $support->id],
                    ['class' => 'btn btn-danger']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
<?php endif;?>

</div>