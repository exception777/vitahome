<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View
 * @var $model \frontend\models\SupportForm
 */

$this->title = Yii::t('frontend', 'Support Add');
?>


<div class="container">
<?php $form = ActiveForm::begin([
    'id' => 'apartment-create',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => 'col-sm-offset-3',
            'wrapper' => 'col-sm-9',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

    <?=$form->errorSummary($model)?>
    <?=$form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?=$form->field($model, 'message')->textarea(['rows' => 6]) ?>


<div class="box-footer">
    <?= Html::submitButton('<i class="fa fa-paper-plane"></i> ' . \Yii::t('frontend', 'Send'),
        ['class' => 'col-sm-offset-3 btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>
</div>
