<?php

use yii\helpers\Html;
use yii\helpers\Url;
use \common\models\Support;
/* @var $this yii\web\View
 * @var array $supports \common\models\Support
 */
$this->title = Yii::t('backend', 'Support');
?>

<div class="container">
<div class="row">
    <div class="col-md-12">
        <p><?=Html::a('<i class="fa fa-plus"></i> ' . Yii::t('frontend', 'Support Add'), ['/cabinet/support/add'], ['class' => 'btn btn-success'])?></p>
        <?php if($supports):?>
            <div class="list-group support-list">
            <?php foreach($supports as $support):?>
                <a href="<?=Url::to(['/cabinet/support/view', 'id' => $support->id])?>" class="list-group-item">
                    <?php if($support->status == Support::STATUS_OPEN):?>
                        <span class="label label-success"><?=Yii::t('frontend', 'Open')?></span>
                    <?php else:?>
                        <span class="label label-danger"><?=Yii::t('frontend', 'Close')?></span>
                    <?php endif;?>
                    <h4 class="list-group-item-heading">
                        <?php if(count($support->supportMessages) > 0) {
                            echo '<span class="label label-success" title="' . Yii::t('frontend', 'New message') .
                                '" data-toggle="tooltip" data-placement="bottom" ><i class="fa fa-commenting-o" aria-hidden="true"></i></span>';
                        } ?> <?=$support->title?>
                    </h4>
                    <p class="list-group-item-text">
                        <?=Yii::t('frontend', 'Added',
                            ['date' => Yii::$app->formatter->asDatetime($support->created_at, 'dd MMMM YYYY, HH:mm')])?>
                    </p>
                </a>
            <?php endforeach;?>
            </div>
        <?php else:?>
            <p class="text-center text-muted"><?=Yii::t('frontend', 'Not record')?></p>
        <?php endif;?>
    </div>
    <div class="col-md-12 text-center">
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    </div>
</div>

</div>