<?php


/* @var $this yii\web\View
 * @var array $model \common\models\Apartment
 */
$this->title = $model->name;
?>

<div class="container">
<h1><?=$this->title?></h1>
<hr>

<?=$this->render('_form', ['model' => $model])?>
</div>
