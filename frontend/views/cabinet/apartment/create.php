<?php
/* @var $this yii\web\View
 * @var $apartment common\models\Apartment
 */

$this->title = Yii::t('frontend', 'Create Ads');

?>

<div class="container">
<h1><?=$this->title?></h1>
<hr>

<?=$this->render('_form', ['model' => $model])?>
</div>