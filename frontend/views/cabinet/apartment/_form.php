<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \common\models\City;
/**
 * @var $model common\models\Apartment
 * @var $this yii\web\View
 */
$coords = explode(',', $model->maps);
$x = 0;
if(isset($coords[0])){
    if(is_numeric($coords[0])){
        $x = $coords[0];
    }
}
$y = 0;
if(isset($coords[1])) {
    if(is_numeric($coords[1])){
        $y = $coords[1];
    }
}
$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
$this->registerJs('
ymaps.ready(function(){
	map = new ymaps.Map ("map",{center:[' . ($x?:'51.128422') . ',' . ($y?:'71.430564') . '],zoom:12},{});
	map.events.add("click", function (e) {
            var coords = e.get("coords");
            $("#apartment-maps").val(coords[0].toPrecision(8)+","+coords[1].toPrecision(8));
            map.balloon.open(coords, {
                contentHeader: "Установлена метка",
                contentBody:
                    "<p>Координаты на карте: " + [
                        coords[0].toPrecision(8),
                        coords[1].toPrecision(8)
                    ].join(", ") + "</p>"
            });

        });

    var x = ' . $x . ', y = ' . $y . ';
    if(x != 0 && y != 0){
        map.balloon.open([x, y],{
            contentBody: "<p>Координаты на карте: " + [x, y].join(",") + "</p>"
        });
    }
});
');
if(!$model->isNewRecord){
    $this->registerJs('
    $(function() {
        $( "#sortable" ).sortable({
            connectWith: "#sortable",
            opacity: 0.8,
            stop: function(event, ui) {
                var k = 0;
                var pos_photo = [];
                $("ul#sortable li.ui-state-default").each(function(){
                    pos_photo[k] = $(this).data("id");
                    k++;
                });
                $.ajax({
                    type: "POST",
                    url: "'.\yii\helpers\Url::toRoute(['/cabinet/apartment/position']).'",
                    data: {"id": '.$model->id.', "position": pos_photo},
                    cache: false
                }).done(function (data) {
                    $(\'#photo-success\').html(\'\');

                    if(data.code === 100){
                        $(\'#photo-success\').html(\'<div class="alert alert-success">Позиция фотографии изменена.</div>\');
                    } else{
                        $(\'#photo-alert\').html(\'<div class="alert alert-danger">Не удалось изменить позицию фотографии.</div>\');
                    }
                }).fail(function() {
                    alert( "Ошибка!!! Не удалось изменить позицию фотографий." );
                });
            }
        });
        $( "#sortable" ).disableSelection();
      });
    ');
}

$phones = explode(',', $model->phone);
$phoneTempLabel = '<div id="phone-field-{key}" class="form-group">
                        {label}
                        <div>{input}</div>
                    </div>';
$phoneTemp = '<div id="phone-field-{key}" class="form-group">
                    <div class="input-group">
                        {input}
                        <div class="input-group-addon input-group-addon-primary cursor-pointer">
                            <span class="phone-remove" data-id="{key}"><i class="fa fa-times"></i></span>
                        </div>
                    </div>
            </div>';

$this->registerJs('
var phoneIndex = '.count($phones).';
var phoneId = phoneIndex;
var phoneTemp = \''.str_replace("\n", "", $phoneTemp).'\';
$("#block-phones").on("click", ".phone-remove", function(){
    $(this).parents("#phone-field-" + $(this).data("id")).remove();
    phoneIndex--;
});
$("#phone-add").on("click", function(){
    if(phoneIndex <= 5){
        var phone = phoneTemp.replace(/{key}/g, phoneId)
            .replace(/{input}/g, \'<input type="text" id="phone\'+phoneId+\'" class="form-control phone-input" name="phones[]" value="" maxlength="20">\');
        $("#block-phones").append(phone);
        phoneId++;
        phoneIndex++;
    }
});
$(".phone-input").phoneInput();
');

$cities = \common\models\City::find()->where(['status' => City::ENABLE])->all();
$citiesDropDownList = [];
$citiesDropDownListItemAttribute = [];
foreach ($cities as $city) {
    $citiesDropDownList[$city->id] = $city->name;
    $citiesDropDownListItemAttribute[$city->id] = ['data-location' => $city->location];
}
?>
<?php $form = ActiveForm::begin([
    'id' => 'apartment-create',
    'options' => ['enctype'=>'multipart/form-data'],

    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => 'col-sm-offset-3',
            'wrapper' => 'col-sm-9',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

    <?=$form->errorSummary($model)?>

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Описание</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'price')->textInput() ?>
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Информация</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'floor')->textInput() ?>
                    <?= $form->field($model, 'floor_total')->textInput() ?>
                    <?= $form->field($model, 'square')->textInput() ?>
                    <?= $form->field($model, 'room')->textInput() ?>
                    <?= $form->field($model, 'sleep')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Контакты</h3>
                </div>
                <div class="panel-body">
                    <div id="block-phones">
                        <?php
                        if($phones){
                            foreach($phones as $key => $phone){
                                $input = Html::textInput('phones[]', $phone,
                                    ['id' => 'phone'.$key, 'class' => 'form-control phone-input',
                                        'maxlength' => 20, 'required' => $key == 0? true: false]);
                                if($key == 0) {
                                    echo str_replace(['{key}', '{label}', '{input}'],
                                        [
                                            $key,
                                            Html::label('Телефон', 'phone' . $key, ['class' => 'control-label']),
                                            $input
                                        ],
                                        $phoneTempLabel);
                                }else{
                                    echo str_replace(['{key}', '{input}'], [$key, $input], $phoneTemp);
                                }
                            }
                        }
                        ?>
                    </div>
                    <div class="form-group ">
                            <button type="button" id="phone-add" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Добавить номер</button>

                    </div>
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Адрес</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'city_id')->dropDownList($citiesDropDownList,
                        ['prompt' => '-- Выберите город --',
                            'onchange'=>'',
                            'options'=>$citiesDropDownListItemAttribute
                        ])
                        ->hint('Вашего города нет в списке? Напишите в '.Html::a('тех поддержку',['/cabinet/support/add']).' его добавят в ближайшее время.') ?>

                    <?= $form->field($model, 'area_id')->dropDownList([]) ?>

                    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Местоположение</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'maps')->hiddenInput(['maxlength' => true])->label(false) ?>
                    <div id="map"  style="width: 100%; height: 400px; "></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Фотографии</h3>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'photos[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label(false) ?>
                    <div style="margin-bottom:10px">
                        <?php
                        if($model->apartmentPhotos){
                            echo '<ul id="sortable">';
                            foreach($model->apartmentPhotos as $value) {
                                echo '<li class="ui-state-default photo-block" data-position="'.$value->position.'" data-id="'.$value->id.'">' .
                                    Html::img('/images/apartments/small/' . $model->id . '/' . $value->name, ['width' => '200', 'class' => 'photo']) .
                                    Html::a('<i class="fa fa-rotate-left"></i>', ['photo-rotate', 'id' => $value->id, 'rotate' => 90],
                                        ['class' => 'photo-rotate rotate-left', 'title' => 'Повернуть влево']) .
                                    Html::a('<i class="fa fa-rotate-right"></i>', ['photo-rotate', 'id' => $value->id, 'rotate' => -90],
                                        ['class' => 'photo-rotate rotate-right', 'title' => 'Повернуть вправо']) .
                                    Html::a('<i class="fa fa-close"></i> удалить', ['photodelete', 'id'=>$value->id], [
                                        'class' => 'photo-delete',
                                        'title' => "Удалить",
                                        'aria-label' => "Удалить",
                                        'data-confirm' => "Вы уверены, что хотите удалить этот элемент?"
                                    ]) .
                                    '</li>';
                            }
                            echo '</ul>';
                        }else{
                            echo '<div  class="alert alert-warning"><p>Нет загруженных фотографий</p></div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php if($model->isNewRecord): ?>
        <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('frontend', 'Publish'),
            ['class' => 'btn btn-success', 'name' => 'btn_create_and_edit']) ?>
    <?php else: ?>
        <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('frontend', 'Edit ads'),
            ['class' => 'btn btn-primary', 'name' => 'btn_update_and_edit']) ?>
    <?php endif; ?>

<?php ActiveForm::end(); ?>

<?php
$url = Yii::$app->urlManager->createUrl('ajax/areas?id=');
$defaultLoad = $model->city_id ? 'loadAreas('.$model->city_id.');' : '';
$js = <<<JS
{$defaultLoad}
$('#apartment-city_id').on('change', function(){
    var location = $(this).find("option[value=\"" + $(this).val() + "\"]").data("location");
    if (location) {
        map.setCenter(location.split(","));
    }
    loadAreas($(this).val());
});
function loadAreas(cityId){
    $.post("{$url}" + cityId, function( data ) {
        $("#apartment-area_id").html(data.options);
        $('#apartment-area_id').val({$model->area_id});
    });
}

$('.photo-rotate').on('click', function() {
    var url = $(this).attr('href');
    var img = $(this).parent().find('img.photo');
    $.ajax({
        url: url,
        type: 'POST',
        data: { }
    })
    .done(function (data) {
        img.attr('src', img.attr('src') + '?' + new Date().getTime());
    })
    .fail(function () {
        alert('Ошибка в запросе');
    })
    .always(function () {});
    return false;    
});

JS;

$this->registerJs($js);
?>
