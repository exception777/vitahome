<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
/* @var $this yii\web\View
 * @var array $apartments \common\models\Apartment
 */
$this->title = Yii::t('frontend', 'Cabinet');
?>

<div class="container">
    <table class="table">
        <tr>
            <th>Номер</th>
            <th>Операция</th>
            <th>Комментарий</th>
            <th>Оплата</th>
            <th>Дата</th>
        </tr>
        <?php foreach ($transactions as $transaction): ?>
            <tr>
                <td><?= $transaction->id ?></td>
                <td><?= $transaction->statusLabel()?></td>
                <td><?= $transaction->comment?></td>
                <td><?= Yii::$app->formatter->asCurrency($transaction->amount)?></td>
                <td><?= Yii::$app->formatter->asDatetime($transaction->created_at)?></td>
            </tr>
        <?php endforeach;?>
    </table>

    <div class="col-md-12 text-center">
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    </div>
</div>