<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
/* @var $this yii\web\View
 * @var array $apartments \common\models\Apartment
 */
$this->title = Yii::t('frontend', 'Cabinet');
?>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Пополнить баланс</h3>
            <p>Для пополнения баланса Вам необходимо сделать перевод средств на банковскую карту:</p>
            <p><strong>5169 4931 8538 0220</strong></p>
            <p>В комментарии для перевода указать номер личного кабинета, если это возможно.</p>
            <p>Ваш номер личного кабинета: <strong><?= Yii::$app->user->id ?></strong></p>
            <p>Либо отправить квитанцию и номер личного кабинета по
                <a href="https://wa.me/77014003447" target="_blank">Whatsapp на номер +7 701 400 34 47</a></p>
        </div>
    </div>
</div>

