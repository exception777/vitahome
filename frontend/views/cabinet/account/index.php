<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\Modal;

/* @var $this yii\web\View
 * @var array $apartments \common\models\Apartment
 */
$this->title = Yii::t('frontend', 'Cabinet');
?>

<div class="container">
    <div id="alert-container"></div>
    <?php
    echo Nav::widget([
        'items' => [
            [
                'label' => 'Подключенные (' . $pages->totalCount . ')',
                'url' => ['ads']
            ],
            [
                'label' => 'Архив (' . $countArchive . ')',
                'url' => ['archive']
            ]
        ],
        'options' => ['class' => 'nav-pills']
    ]);

    \yii\widgets\Pjax::begin([
        'id' => 'apartments-list'
    ]);
    ?>
    <?php if ($apartments): ?>
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Объявление</th>
                <th class="text-right">Дейстивие</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($apartments as $apartment):?>
                <tr>
                    <td>
                        <?= str_pad($apartment->id, 4, '0', STR_PAD_LEFT); ?>
                    </td>
                    <td>
                        <div class="object-list-item-photo">
                            <?php if ($apartment->apartmentPhotos): ?>
                                <?= Html::img('/images/apartments/small/' . $apartment->id . '/' .
                                    $apartment->apartmentPhotos[0]->name, ['class' => " img-rounded", 'height' => '50px']) ?>
                            <?php endif; ?>
                        </div>
                        <div class="object-list-item-content">
                            <?php if ($apartment->city->id == 1): ?>
                                <?php if ($apartment->expired_at > date('Y-m-d H:i:s')): ?>
                                    <div class="text-success">Отключится <?= $apartment->getExpiredAt()->format('d.m.Y, H:i') ?></div>
                                <?php else: ?>
                                    <div class="text-danger">Отключено <?= $apartment->getExpiredAt()->format('d.m.Y, H:i') ?></div>
                                <?php endif ?>
                            <?php endif ?>
                            <div><strong><?= $apartment->name ?></strong></div>
                            <div>Цена: <?= $apartment->price ?> тенге</div>
                            <div class="text-muted">Последнее поднятие <?= Yii::$app->formatter->asRelativeTime($apartment->up_at)?></div>
                        </div>
                    </td>
                    <td style="text-align: right">
                        <?php if ($apartment->expired_at < date('Y-m-d H:i:s') && $apartment->city->id == 1): ?>
                            <button type="button" class="btn btn-success btn-sm apartment-active" title="Подключить объявление"
                                    data-url="<?= \yii\helpers\Url::to(['/cabinet/account/service', 'id' => $apartment->id, 'serviceId' => 2])?>"
                                    data-service-id="2"><i class="fa fa-check"></i> </button>
                        <?php else: ?>
                            <button type="button" class="btn btn-success btn-sm apartment-up" title="Поднять объявление"
                                    data-url="<?= \yii\helpers\Url::to(['/cabinet/account/service', 'id' => $apartment->id, 'serviceId' => 1])?>"
                                    data-service-id="1"><i class="fa fa-arrow-up"></i> </button>
                        <?php endif ?>
                        <?= Html::a('<i class="fa fa-external-link"></i>',
                            ['apartment/index', 'id' => $apartment->id],
                            ['title' => Yii::t('frontend', 'View'), 'class' => 'btn btn-default btn-sm']) ?>
                        <?= Html::a('<i class="fa fa-edit"></i>',
                            ['/cabinet/apartment/edit', 'id' => $apartment->id],
                            ['title' => Yii::t('frontend', 'Edit'), 'class' => 'btn btn-default btn-sm']); ?>
                        <?= Html::a('<i class="fa fa-times"></i>', ['/cabinet/apartment/disable', 'id' => $apartment->id],
                            ['title' => Yii::t('frontend', 'Disable'), 'class' => 'btn btn-danger btn-sm', 'data-method' => 'post']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="text-center text-muted"><?= Yii::t('frontend', 'Not record') ?></p>
    <?php endif; ?>
    <div class="text-center">
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    </div>
    <?php \yii\widgets\Pjax::end();?>
</div>

<?php
Modal::begin([
    'header'=>'<h4 class="modal-title"></h4>',
    'id'=>'modal',
    'footer' => '<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary btn-sm service-pay" data-url="">Продолжить</button>'
]);

echo "<div class='modalContent'></div>";
Modal::end();

Modal::begin([
    'header'=>'<h4 class="modal-title"></h4>',
    'id'=>'apartment-active-modal',
    'class' => 'modal',
    'footer' => '<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary btn-sm service-pay" data-url="">Продолжить</button>'
]);

echo "<div class='modalContent'></div>";
Modal::end();

$serviceData = [];
foreach ($services as $service) {
    $serviceData[$service->id] = [
        'name' => $service->name,
        'description' => $service->description,
        'price' => Yii::$app->formatter->asCurrency($service->price)
    ];
}
$serviceData = json_encode($serviceData);
$this->registerJs(<<<JS
var serviceData = {$serviceData};
$('#apartments-list').on('click', '.apartment-up', function() {
    var service = serviceData[$(this).data('serviceId')];
    $('#modal .modalContent').html(service.description);
    $('.service-pay').data('url', $(this).data('url'));
    $('#modal').find('.modal-header > h4').html(service.name + ': ' + service.price);
    $('#modal').modal('show');
});
$('#apartments-list').on('click', '.apartment-active', function() {
    var service = serviceData[$(this).data('serviceId')];
    $('#apartment-active-modal .modalContent').html(service.description);
    $('.service-pay').data('url', $(this).data('url'));
    $('#apartment-active-modal').find('.modal-header > h4').html(service.name + ': ' + service.price);
    $('#apartment-active-modal').modal('show');
});
$('.service-pay').on('click', function(){
    $('.modal').modal('hide');
    var url = $(this).data('url');
    if (url) {
        $.ajax({
            url: url,
            success: function(data){
                console.log(data);
                $('#alert-container').html(alertShow(data.message, data.type));
                $.pjax.reload({container: '#apartments-list', async: false});
                $.pjax.reload({container: '#wallet-balance', async: false});
            },
            error: function(){
               alert('Ошибка, попробуйте позже'); 
            }
        });
    }
});
JS
);

?>