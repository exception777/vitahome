<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use common\models\Apartment;
/* @var $this yii\web\View
 * @var array $apartments \common\models\Apartment
 */
$this->title = Yii::t('frontend', 'Cabinet');
?>

<div class="container">
<?php
echo Nav::widget([
    'items' => [
        [
            'label' => 'Подключенные (' . $countApproved . ')',
            'url' => ['ads']
        ],
        [
            'label' => 'Архив (' . $pages->totalCount . ')',
            'url' => ['archive']
        ]
    ],
    'options' => ['class' => 'nav-pills']
])

?>
    <?php if ($apartments): ?>
    <table class="table">
        <thead>
        <tr>
            <th style="width: 60px">ID</th>
            <th>Объявление</th>
            <th style="width: 150px; text-align: right">Дейстивие</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($apartments as $apartment):?>
            <tr>
                <td>
                    <?= str_pad($apartment->id, 4, '0', STR_PAD_LEFT); ?>
                </td>
                <td>
                    <div class="object-list-item-photo">
                        <?php if ($apartment->apartmentPhotos): ?>
                            <?= Html::img('/images/apartments/small/' . $apartment->id . '/' .
                                $apartment->apartmentPhotos[0]->name, ['class' => " img-rounded", 'height' => '50px']) ?>
                        <?php endif; ?>
                    </div>
                    <div class="object-list-item-content">
                        <div><?=$apartment->getStatusLabel()?></div>
                        <div><strong><?=$apartment->name?></strong></div>
                        <div>Цена: <?php echo $apartment->price?> тенге</div>
                    </div>
                </td>
                <td style="text-align: right">
                    <?=Html::a('<i class="fa fa-edit"></i>',
                        ['/cabinet/apartment/edit', 'id' => $apartment->id],
                        ['title' => Yii::t('frontend', 'Edit'), 'class' => 'btn btn-default btn-sm']);?>
                    <?php if($apartment->status != Apartment::STATUS_MODERATE):?>
                        <?=Html::a('<i class="fa fa-refresh"></i>',
                            ['/cabinet/apartment/approved', 'id' => $apartment->id],
                            ['title' => 'Восстановить из архива', 'class' => 'btn btn-default btn-sm', 'data-method' => 'post'])?>
                    <?php endif;?>
                    <?=Html::a('<i class="fa fa-times"></i>',
                        ['#', 'id' => $apartment->id],
                        ['title' => Yii::t('frontend', 'Remove'), 'class' => 'btn btn-danger btn-sm']);?>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <?php else: ?>
        <p class="text-center text-muted"><?= Yii::t('frontend', 'Not record') ?></p>
    <?php endif; ?>
    <div class="text-center">
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    </div>
</div>
