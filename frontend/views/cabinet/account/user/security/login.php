<?php
use dektrium\user\widgets\Connect;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */

$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container login-container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

            <header class="header-group text-center">
                <h1><?= Html::encode($this->title) ?></h1>
            </header>
                <?php $form = ActiveForm::begin([
                    'id'                     => 'login-form',
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                    'validateOnBlur'         => false,
                    'validateOnType'         => false,
                    'validateOnChange'       => false,
                ]) ?>
            <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

                <?= $form->field($model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]) ?>

                <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()->label(Yii::t('user', 'Password') . ($module->enablePasswordRecovery ? ' (' . Html::a(Yii::t('user', 'Forgot password?'), ['/user/recovery/request'], ['tabindex' => '5']) . ')' : '')) ?>

                <div class="form-group">
                    <div class="checkbox">
                    </div>
                </div>
                <div class="checkbox">
                    <label>
                        <?=Html::activeCheckbox($model, 'rememberMe', ['label' => null,'tabindex' => '4'])?>
                        <?=Html::activeLabel($model, 'rememberMe')?>
                    </label>
                </div>

            <p class="text-center">
                <?= Html::submitButton(Yii::t('user', 'Sign in'), ['class' => 'btn btn-orange', 'tabindex' => '3']) ?>
            </p>
                <?php ActiveForm::end(); ?>
                <br>
                <?php if ($module->enableRegistration): ?>
                    <p class="text-center">
                        <?= Html::a(Yii::t('frontend', 'Sign up'), ['/user/registration/register'], ['class' => 'btn btn-success']) ?>
                    </p>
                <?php endif ?>
            <?php if ($module->enableConfirmation): ?>
                <p class="text-center">
                    <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
                </p>
            <?php endif ?>
            <?= Connect::widget([
                'baseAuthUrl' => ['/user/security/auth'],
            ]) ?>
        </div>
    </div>
</div>
