<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\RecoveryForm $model
 */

$this->title = Yii::t('user', 'Recover your password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container login-container">
    <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

            <header class="header-group text-center">
                <h1><?= Html::encode($this->title) ?></h1>
            </header>
            <?php $form = ActiveForm::begin([
                'id' => 'password-recovery-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
            ]); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <p class="text-center">
                <?= Html::submitButton(Yii::t('user', 'Continue'), ['class' => 'btn btn-primary']) ?><br>
            </p>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
