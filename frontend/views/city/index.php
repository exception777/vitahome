<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
/* @var $this yii\web\View
 * @var $apartments \common\models\Apartment
 * @var $city \common\models\City
 */

$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU', ['depends' => [\yii\web\JqueryAsset::className()]]);
$titles = [];
$keywords = $city->keywords;
if (Yii::$app->request->get('rooms')) {
    $titles[] = Yii::t('frontend', 'City title room', ['room' => Yii::$app->request->get('rooms')]);
    $keywords .= ', ' . Yii::t('frontend', 'City title room', ['room' => Yii::$app->request->get('rooms')]);
}
if (Yii::$app->request->get('price_from') && Yii::$app->request->get('price_to')) {
    $titles[] = Yii::t('frontend', 'City title price from price to',
        ['price_from' => Yii::$app->request->get('price_from'), 'price_to' => Yii::$app->request->get('price_to')]);
} else {
    if (Yii::$app->request->get('price_from')) {
        $titles[] = Yii::t('frontend', 'City title price', ['price' => Yii::$app->request->get('price_from')]);
    }
    if (Yii::$app->request->get('price_to')) {
        $titles[] = Yii::t('frontend', 'City title price to', ['price' => Yii::$app->request->get('price_to')]);
    }
}
$title = implode(' ', $titles);
if ($title) {
    $this->title = $title . ' ' . Yii::t('frontend', 'City title city', ['city' => $city->name]);
} else {
    $this->title = $city->title;
}

$this->params['description'] = $city->description;
$this->params['keywords'] = $keywords;
?>

<?= $this->render('../layouts/filters', ['city' => $city]) ?>

<div class="container">
    <ul class="nav nav-pills nav-justified" style="margin-top: 10px">
        <li class="active">
            <?= Html::a('Списком', ['city/index', 'cityAlias' => $city->alias])?>
        </li>
        <li>
            <?= Html::a('На карте', ['city/map', 'cityAlias' => $city->alias]) ?>
        </li>
    </ul>

    <header class="header-group">
        <h1 class="header-section text-center"><?= $city->title ?></h1>
    </header>

    <div class="row">
        <div class="row">
            <div class="col-sm-6">
                <p><?= Html::a(Yii::t('frontend', 'One room apartment'),
                        ['city/index', 'cityAlias' => $city->alias, 'rooms' => 1])?></p>
            </div>
            <div class="col-sm-6">
                <p><?= Html::a(Yii::t('frontend', 'Two rooms apartment'),
                        ['city/index', 'cityAlias' => $city->alias, 'rooms' => 2])?></p>
            </div>
            <div class="col-sm-6">
                <p><?= Html::a(Yii::t('frontend', 'Three rooms apartment'),
                        ['city/index', 'cityAlias' => $city->alias, 'rooms' => 3])?></p>
            </div>
            <div class="col-sm-6">
                <p><?= Html::a(Yii::t('frontend', 'Four rooms apartment'),
                        ['city/index', 'cityAlias' => $city->alias, 'rooms' => 4])?></p>
            </div>
        </div>
        
        <?php if ($apartments):?>
            <div class="row">
                <?php foreach ($apartments as $apartment): ?>
                    <?=$this->render('@frontend/views/apartment/_item_small', ['apartment' => $apartment])?>
                <?php endforeach; ?>
            </div>
        <?php else: ?>
            <div class="ads-not-found">
                <div>Объявления не найдены</div>
                <div class="small">Пожалуйста, воспользуйтесь поиском, чтобы найти нужные объявления.</div>
            </div>
        <?php endif;?>

        <div class="row">
            <div class="col-sm-12 text-center">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                    'options' => ['class' => 'pagination page-vh']
                ]);
                ?>
            </div>
        </div>
    </div>

    <?php
    if ($apartments) {
        $features =[];
        $center = '';
        foreach($apartments as $apartment){
            if(!$apartment->maps) continue;
            if(!$center)
                $center = $apartment->maps;
            $photo = '';
            if($apartment->apartmentPhotos) {
                $photo = Html::a(Html::img('/images/apartments/small/' . $apartment->id . '/' .
                    $apartment->apartmentPhotos[0]->name, ['class' => "img-responsive img-rounded", 'style' => 'float:left;padding-right:10px;width: 100px']),
                    ['apartment/index', 'id' => $apartment->id], ['title' => $apartment->name]);
            }
            $features[] = '{
                "type": "Feature",
                "id": '.$apartment->id.',
                "geometry": {
                    "type": "Point",
                    "coordinates": ['.$apartment->maps.']
                },
                "properties": {
                    "balloonContent": \''.$photo.'<h3 class="apartment-header maps-header">'.Html::a($apartment->name,
                    ['apartment/index', 'id' => $apartment->id], ['title' => $apartment->name]).'</h3>\'+
                        \'<p class="apartment-price">Цена: <span class="label label-success">'.$apartment->price.' тенге</span></p>\',
                    "clusterCaption": "'.$apartment->name.'",
                    "hintContent": "'.$apartment->name.'"
                }
            }';
        }
        $jsonJs = '{
            "type": "FeatureCollection",
            "features": [
                '.implode(',', $features).'
            ]
        }';
        $this->registerJs('
            ymaps.ready(init);
            var myMap;

            function init(){
                var objectManager = new ymaps.ObjectManager({clusterize: true});
                objectManager.add('.$jsonJs.');
                myMap = new ymaps.Map("map", {
                    center: ['.$center.'],
                    zoom: 14
                });
                myMap.geoObjects.add(objectManager);
            }
        ');
        echo '<div id="map" class="border-gray" style=" height: 400px"></div>';
    }
    ?>
</div>
