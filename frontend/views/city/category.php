<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
/* @var $this yii\web\View
 * @var $apartments \common\models\Apartment
 * @var $city \common\models\City
 */

$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = 'Квартиры посуточно';

$this->params['description'] = '';
$this->params['keywords'] = '';
?>

<?=$this->render('../layouts/header')?>

<div class="body-content">
    <div class="row">
        <?php foreach($apartments as $apartment):?>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                <div class="apartment">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 apartment-image text-center">
                            <div class="apartment-id">№ <?=str_pad($apartment->id, 4, '0', STR_PAD_LEFT);?></div>

                            <?php if($apartment->apartmentPhotos) {?>
                            <?=Html::a(Html::img('/images/apartments/small/' . $apartment->id . '/' .
                                $apartment->apartmentPhotos[0]->name, ['class' => "img-responsive img-rounded"]),
                                ['apartment/index', 'id' => $apartment->id], ['title' => $apartment->name])?>
                            <?php }?>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3 class="apartment-header"><?=Html::a($apartment->name, ['apartment/index', 'id' => $apartment->id], ['title' => $apartment->name])?></h3>
                            <p class="apartment-price">Цена: <span class="label label-success"><?php echo $apartment->price?> тенге</span></p>
                            <p class="aparment-address"><?=$apartment->address?></p>
                            <p class="apartment-text hidden-xs">
                                <?php echo Html::encode(StringHelper::truncateWords($apartment->text, 17, '...'));?>
                            </p>
                            <p class="visible-xs visible-sm">
                                <?php
                                $phones = explode(',', $apartment->phone);
                                foreach ($phones as $phone) {
                                    echo '<a href="tel:'.$phone.'" class="btn btn-success btn-block"><i class="glyphicon glyphicon-earphone"></i> ' . $phone .'</a><br>';
                                }
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
        </div>
    </div>
    <?php
    if($apartments){
        $features =[];
        $center = '';
        foreach($apartments as $apartment){
            if(!$apartment->maps) continue;
            if(!$center)
                $center = $apartment->maps;
            $photo = '';
            if($apartment->apartmentPhotos) {
                $photo = Html::a(Html::img('/images/apartments/small/' . $apartment->id . '/' .
                    $apartment->apartmentPhotos[0]->name, ['class' => "img-responsive img-rounded", 'style' => 'float:left;padding-right:10px;width: 100px']),
                    ['apartment/index', 'id' => $apartment->id], ['title' => $apartment->name]);
            }
            $features[] = '{
                "type": "Feature",
                "id": '.$apartment->id.',
                "geometry": {
                    "type": "Point",
                    "coordinates": ['.$apartment->maps.']
                },
                "properties": {
                    "balloonContent": \''.$photo.'<h3 class="apartment-header maps-header">'.Html::a($apartment->name,
                    ['apartment/index', 'id' => $apartment->id], ['title' => $apartment->name]).'</h3>\'+
                        \'<p class="apartment-price">Цена: <span class="label label-success">'.$apartment->price.' тенге</span></p>\',
                    "clusterCaption": "'.$apartment->name.'",
                    "hintContent": "'.$apartment->name.'"
                }
            }';
        }
        $jsonJs = '{
            "type": "FeatureCollection",
            "features": [
                '.implode(',', $features).'
            ]
        }';
        $this->registerJs('
            ymaps.ready(init);
            var myMap;

            function init(){
                var objectManager = new ymaps.ObjectManager({clusterize: true});
                objectManager.add('.$jsonJs.');
                myMap = new ymaps.Map("map", {
                    center: ['.$center.'],
                    zoom: 14
                });
                myMap.geoObjects.add(objectManager);
            }
        ');
        echo '<div id="map" style=" height: 400px"></div>';
    }
    ?>
</div>
