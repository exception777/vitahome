<?php

namespace frontend\helpers;

use common\models\Area;
use yii\helpers\ArrayHelper;

class FrontendHelper
{

    public function getCityAreas($id)
    {
        $areas = Area::find()->where(['city_id' => $id, 'status' => 1])->asArray()->all();

        return ArrayHelper::map($areas, 'id', 'name');
    }
}