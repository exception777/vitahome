<?php
return [
    'name' => 'VitaHome',
    'language' => 'ru-RU',
    'timeZone' => 'Asia/Almaty',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'frontendCache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => Yii::getAlias('@frontend') . '/runtime/cache'
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],

        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'cache*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'models*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'formatter' => [
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
//            'currencyCode' => 'KZT',
            'numberFormatterOptions' => [
                NumberFormatter::MIN_FRACTION_DIGITS => 0,
                NumberFormatter::MAX_FRACTION_DIGITS => 0,
            ],
            'numberFormatterSymbols' => [
                NumberFormatter::CURRENCY_SYMBOL => 'тенге',
            ]
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableFlashMessages' => true,
            'enableRegistration' => true,
            // генерация пароля из 8 символов
            'enableGeneratingPassword' => false,
            // подтверждение регистрации
            'enableConfirmation' => false,
            // вход без подтверждения регистрации
            'enableUnconfirmedLogin' => true,
            // востановление пароля
            'enablePasswordRecovery' => true,
            // возможность удалить учетную запись
            'enableAccountDelete' => false,
            /**
             * Метод изминения пароля:
             * STRATEGY_DEFAULT Сообщение о подтверждении отправляется на почту пользователя
             * STRATEGY_INSECURE без подтверждения
             * STRATEGY_SECURE Подтверждение отправляется на новые и на старые адреса почты пользователя,
             * пользователь должен нажать обе ссылки подтверждения
             */
            'emailChangeStrategy' => \dektrium\user\Module::STRATEGY_DEFAULT,
            // время жизни токена на подтверждение
            'confirmWithin' => 86400,
            // время входа без пароля
            'rememberFor' => 1209600,
            // время жизни токена на восстановления пароля
            'recoverWithin' => 21600,
            // количество итераций хэширования алгоритмом Blowfish
            'cost' => 12,
            // список юзеров допустимых к администрированию пользователей
            'admins' => [],
            // группа допустимая к администрированию пользователей
            'adminPermission' => 'admin',
            // правила url
            //'urlRules' => []
            'modelMap' => [
                'User' => 'common\models\User',
            ],
            'mailer' => [
                'sender'                => ['no-reply@vitahome.kz' => 'vitahome.kz'],
                'welcomeSubject'        => 'Активация аккаунта',
                'confirmationSubject'   => 'Подтвердите регистрацию на сайте vitahome.kz',
                'reconfirmationSubject' => 'Подтвердите свой адрес эл. почты',
                'recoverySubject'       => 'Смена пароля на сайте vitahome.kz',
            ],
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
    ],
];

