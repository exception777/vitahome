<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'documentTypes' => [0 => 'Удостоверение личности', 1 => 'Загроничный паспорт', 2 => 'Паспорт', 3 => 'Водительские права'],
    'yesNo' => [0 => 'Нет', 1 => 'Да'],
    'statusList' => [1 => 'Включено', 0 => 'Отключено'],
    'uploadOrig' => Yii::getAlias('@backend') . '/web/uploads/apartments/',
    'uploadLarge' => Yii::getAlias('@frontend') . '/web/images/apartments/large/',
    'uploadSmall' => Yii::getAlias('@frontend') . '/web/images/apartments/small/',
    'uploadSlider' => Yii::getAlias('@frontend') . '/web/images/slider/',

    'cacheTime' => [
        'hour' => 3600,
        '+6 hours' => 21600,
        '+12 hours' => 43200,
        'day' => 86400,
        'week' => 604800,
        'month' => 2592000
    ]
];
