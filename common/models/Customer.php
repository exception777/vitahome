<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $name
 * @property string $document
 * @property string $document_number
 * @property string $document_date
 * @property string $iin
 * @property string $phone
 * @property string $created_at
 * @property int $user_id
 * @property string $email
 * @property string $information
 *
 * @property Booking[] $bookings
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['document_date', 'created_at'], 'safe'],
            [['user_id'], 'integer'],
            [['name', 'document', 'phone'], 'string', 'max' => 128],
            [['document_number', 'iin'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 80],
            [['information'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'name' => Yii::t('models', 'Name'),
            'document' => Yii::t('models', 'Document'),
            'document_number' => Yii::t('models', 'Document Number'),
            'document_date' => Yii::t('models', 'Document Date'),
            'iin' => Yii::t('models', 'Iin'),
            'phone' => Yii::t('models', 'Phone'),
            'created_at' => Yii::t('models', 'Created At'),
            'user_id' => Yii::t('models', 'User ID'),
            'email' => Yii::t('models', 'Email'),
            'information' => Yii::t('models', 'Information'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
