<?php

namespace common\models;

use dosamigos\transliterator\TransliteratorHelper;
use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $text
 * @property string $created_at
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'title', 'text'], 'required'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['alias', 'title'], 'string', 'max' => 255],
            [['keywords', 'description'], 'string', 'max' => 512],
            [['alias'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'alias' => 'Альяс',
            'title' => 'Заголовок',
            'keywords' => 'Ключевые слова',
            'description' => 'Описание',
            'text' => 'Текст',
            'created_at' => 'Создано',
        ];
    }
}
