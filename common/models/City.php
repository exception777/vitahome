<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $name
 * @property string $keywords
 * @property string $description
 * @property string $title
 * @property integer $status
 * @property string $location
 * @property string $alias
 *
 * @property Apartment[] $apartments
 * @property Area[] $areas
 */
class City extends \yii\db\ActiveRecord
{
    const ENABLE = 1;
    const DISABLE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 64],
            [['keywords', 'description'], 'string', 'max' => 512],
            [['title'], 'string', 'max' => 256],
            [['location'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'keywords' => Yii::t('backend', 'Keywords'),
            'description' => Yii::t('backend', 'Description'),
            'title' => Yii::t('backend', 'Title'),
            'status' => Yii::t('backend', 'Status'),
            'location' => Yii::t('backend', 'Location'),
            'alias' => Yii::t('backend', 'Alias'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartments()
    {
        return $this->hasMany(Apartment::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreas()
    {
        return $this->hasMany(Area::className(), ['city_id' => 'id']);
    }

    public static function allCities(){
        return ArrayHelper::map(City::find()
            ->asArray()
            ->orderBy('name')
            ->all(), 'id', 'name');
    }
}
