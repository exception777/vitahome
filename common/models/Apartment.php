<?php

namespace common\models;

use Gregwar\Image\Image;
use Yii;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;


/**
 * This is the model class for table "apartment".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property int $price
 * @property int $discount_price
 * @property int $is_free_now
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property int $floor
 * @property int $floor_total
 * @property int $room
 * @property string $sleep
 * @property double $square
 * @property string $maps
 * @property int $city_id
 * @property int $area_id
 * @property int $status
 * @property int $position
 * @property int $user_id
 * @property string $updated_at
 * @property string $created_at
 * @property int $ob_enable
 * @property string $ob_name
 * @property string $ical_hash
 * @property string $expired_at
 * @property string $up_at
 *
 * @property City $city
 * @property Area $area
 * @property User $user
 * @property ApartmentPhoto[] $apartmentPhotos
 * @property Booking[] $bookings
 * @property Ical[] $icals
 */

class Apartment extends \yii\db\ActiveRecord
{
    const STATUS_DISABLE = 0;
    const STATUS_APPROVED = 1;
    const STATUS_ARCHIVE = 2;
    const STATUS_REMOVE = 3;
    const STATUS_MODERATE = 4;

    /**
     * @var UploadedFile[]
     */
    public $photos;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apartment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text', 'price','phone', 'email', 'room', 'sleep', 'address', 'maps', 'city_id', 'area_id', 'user_id'], 'required'],
            [['text'], 'string'],
            [['price', 'discount_price', 'floor', 'floor_total', 'room', 'city_id', 'area_id', 'status', 'user_id', 'ob_enable', 'position', 'is_free_now'], 'integer'],
            [['updated_at', 'created_at', 'expired_at', 'up_at'], 'safe'],
            [['square'], 'number'],
            [['phone'], 'string', 'max' => 512],
            [['name', 'email'], 'string', 'max' => 256],
            [['address', 'ob_name'], 'string', 'max' => 128],
            [['sleep'], 'string', 'max' => 24],
            [['maps'], 'string', 'max' => 64],
            [['ical_hash'], 'string', 'max' => 40],
            [['photos'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 10],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['area_id'], 'exist', 'skipOnError' => true, 'targetClass' => Area::className(), 'targetAttribute' => ['area_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'name' => Yii::t('models', 'Name'),
            'text' => Yii::t('models', 'Text'),
            'price' => Yii::t('models', 'Price'),
            'discount_price' => Yii::t('models', 'Discount Price'),
            'is_free_now' => Yii::t('models', 'Is free now'),
            'phone' => Yii::t('models', 'Phone'),
            'email' => Yii::t('models', 'Email'),
            'address' => Yii::t('models', 'Address'),
            'floor' => Yii::t('models', 'Floor'),
            'floor_total' => Yii::t('models', 'Floor Total'),
            'room' => Yii::t('models', 'Room'),
            'sleep' => Yii::t('models', 'Sleep'),
            'square' => Yii::t('models', 'Square'),
            'maps' => Yii::t('models', 'Maps'),
            'city_id' => Yii::t('models', 'City ID'),
            'area_id' => Yii::t('models', 'Area ID'),
            'status' => Yii::t('models', 'Status'),
            'position' => Yii::t('models', 'Position'),
            'user_id' => Yii::t('models', 'User ID'),
            'expired_at' => Yii::t('models', 'Expired At'),
            'up_at' => Yii::t('models', 'Up At'),
            'updated_at' => Yii::t('models', 'Updated At'),
            'created_at' => Yii::t('models', 'Created At'),
            'ob_enable' => Yii::t('models', 'Ob Enable'),
            'ob_name' => Yii::t('models', 'Ob Name'),
            'ical_hash' => Yii::t('models', 'Ical Hash'),
        ];
    }

    public function getExpiredAt() {
        return new \DateTime($this->expired_at);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentPhotos()
    {
        return $this->hasMany(ApartmentPhoto::className(), ['apartment_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['object_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIcals()
    {
        return $this->hasMany(Ical::className(), ['apartment_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->photos = UploadedFile::getInstances($this, 'photos');
            if (!$this->position) {
                $this->position = 0;
            }

            if (!$this->ical_hash) {
                $this->ical_hash = $this->id . '-' . Yii::$app->getSecurity()->generateRandomString();
            }
            if (!$this->expired_at) {
                $this->expired_at = date('Y-m-d H:i:s');
            }
            if (!$this->up_at) {
                $this->up_at = date('Y-m-d H:i:s');
            }

            if (!$this->created_at) {
                $this->created_at = date('Y-m-d H:i:s');
            }

            $this->updated_at = date('Y-m-d H:i:s');
            return true;
        }
        return false;
    }
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        $this->uploadImages();
    }

    public function getStatusLabel()
    {
        switch($this->status){
            case 0: $label = '<span class="label label-info">Отключено</span>';
                break;
            case 1: $label = '<span class="label label-success">Активно</span>';
                break;
            case 2: $label = '<span class="label label-warning">Архив</span>';
                break;
            case 3: $label = '<span class="label label-danger">Удалено</span>';
                break;
            case 4: $label = '<span class="label label-warning">На модерации</span>';
                break;
            default:
                $label = '';
        }
        return $label;
    }

    public static function getStatusList()
    {
        return [0 => 'Отключено', 1 => 'Активно', 2 => 'Архив', 3 => 'Удалено', 4 => 'На модерации'];
    }

    public static function getObEnabled()
    {
        return ArrayHelper::map(Apartment::find()
            ->where(['ob_enable' => 1])
            ->asArray()
            ->orderBy('ob_name')
            ->all(), 'id', 'ob_name');
    }

    public function getDirOrig($file = null)
    {
        $dir = Yii::getAlias('@backend') . '/web/uploads/apartments/' . $this->id . '/';
        return $file ? $dir . $file : $dir;
    }
    public function getDirLarge($file = null)
    {
        $dir = Yii::getAlias('@frontend') . '/web/images/apartments/large/' . $this->id . '/';
        return $file ? $dir . $file : $dir;
    }
    public function getDirSmall($file = null)
    {
        $dir = Yii::getAlias('@frontend') . '/web/images/apartments/small/' . $this->id . '/';
        return $file ? $dir . $file : $dir;
    }

    private function uploadImages()
    {
        if ($this->photos) {
            $maxPos = 0;
            foreach ($this->apartmentPhotos as $val) {
                if($val->position > $maxPos){
                    $maxPos = $val->position + 1;
                }
            }
            if (!file_exists($this->getDirOrig())) {
                mkdir($this->getDirOrig(), 0777, true);
            }
            if (!file_exists($this->getDirLarge())) {
                mkdir($this->getDirLarge(), 0777, true);
            }
            if (!file_exists($this->getDirSmall())) {
                mkdir($this->getDirSmall(), 0777, true);
            }
            foreach($this->photos as $file){
                $apartmentPhoto = new ApartmentPhoto();
                $apartmentPhoto->apartment_id = $this->id;
                $apartmentPhoto->name = hash('sha1', $file->baseName . time()) . '.' . $file->extension;
                $apartmentPhoto->position = $maxPos;
                if($file->saveAs($this->getDirOrig($apartmentPhoto->name))){
                    Image::open($this->getDirOrig($apartmentPhoto->name))->zoomCrop(800, 600)->save($this->getDirLarge($apartmentPhoto->name));
                    Image::open($this->getDirOrig($apartmentPhoto->name))->zoomCrop(280, 200)->save($this->getDirSmall($apartmentPhoto->name));
                    $apartmentPhoto->save();
                }
            }
        }
    }

    public function checkStatus()
    {
        if ($this->status < 2) {
            if ($this->phone && count($this->apartmentPhotos) > 0) {
                $this->status = 1;
            } else {
                $this->status = 0;
            }
        }
        return $this->status;
    }

    public function valid()
    {
        return $this->phone && count($this->apartmentPhotos) > 0;
    }

}
