<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property int $id
 * @property string $pay_date
 * @property int $price
 * @property string $comment
 * @property int $booking_id
 * @property string $created_at
 *
 * @property Booking $booking
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pay_date', 'price', 'booking_id'], 'required'],
            [['pay_date', 'created_at'], 'safe'],
            [['price', 'booking_id'], 'integer'],
            [['comment'], 'string', 'max' => 500],
            [['booking_id'], 'exist', 'skipOnError' => true, 'targetClass' => Booking::className(), 'targetAttribute' => ['booking_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'pay_date' => Yii::t('models', 'Pay Date'),
            'price' => Yii::t('models', 'Price'),
            'comment' => Yii::t('models', 'Comment'),
            'booking_id' => Yii::t('models', 'Booking ID'),
            'created_at' => Yii::t('models', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
    }
}
