<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $user_id
 * @property integer $status
 * @property string $name
 * @property string $image
 * @property string $description
 * @property string $url
 * @property string $expire_at
 * @property string $updated_at
 * @property string $created_at
 *
 * @property User $user
 * @property City $city
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'user_id', 'name', 'expire_at'], 'required'],
            [['city_id', 'user_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['expire_at', 'updated_at', 'created_at'], 'safe'],
            [['name', 'image', 'url'], 'string', 'max' => 128],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'city_id' => Yii::t('backend', 'City ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'status' => Yii::t('backend', 'Status'),
            'name' => Yii::t('backend', 'Name'),
            'image' => Yii::t('backend', 'Image'),
            'description' => Yii::t('backend', 'Description'),
            'url' => Yii::t('backend', 'Url'),
            'expire_at' => Yii::t('backend', 'Expire At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
