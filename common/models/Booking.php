<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property int $id
 * @property int $object_id
 * @property string $date_in
 * @property string $date_out
 * @property int $price
 * @property int $total
 * @property int $discount
 * @property int $customer_id
 * @property string $created_at
 * @property double $days
 * @property int $status_id
 * @property string $information
 * @property string $ical_hash
 * @property int $user_id
 * @property double $transfer_in
 * @property double $transfer_out
 * @property double $more_pay
 * @property double $booking_total
 * @property int $approved
 *
 * @property Customer $customer
 * @property Apartment $object
 * @property Status $status
 * @property Payment[] $payments
 */
class Booking extends \yii\db\ActiveRecord
{
    const ENABLE = 1;
    const DISABLE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['object_id', 'status_id', 'user_id', 'date_in', 'date_out', 'price', 'total', 'booking_total', 'customer_id'], 'required'],
            [['object_id', 'status_id', 'user_id', 'customer_id', 'approved'], 'integer'],
            [['days', 'total', 'booking_total', 'transfer_in', 'transfer_out', 'more_pay', 'price'], 'number'],
            [['date_in', 'date_out', 'created_at'], 'safe'],
            [['information'], 'string', 'max' => 500],
            [['ical_hash'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Apartment::className(), 'targetAttribute' => ['object_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'object_id' => Yii::t('models', 'Object ID'),
            'date_in' => Yii::t('models', 'Date In'),
            'date_out' => Yii::t('models', 'Date Out'),
            'price' => Yii::t('models', 'Price'),
            'total' => Yii::t('models', 'Total'),
            'discount' => Yii::t('models', 'Discount'),
            'customer_id' => Yii::t('models', 'Customer ID'),
            'created_at' => Yii::t('models', 'Created At'),
            'days' => Yii::t('models', 'Days'),
            'status_id' => Yii::t('models', 'Status ID'),
            'information' => Yii::t('models', 'Information'),
            'ical_hash' => Yii::t('models', 'Ical Hash'),
            'user_id' => Yii::t('models', 'User ID'),
            'transfer_in' => Yii::t('models', 'Transfer In'),
            'transfer_out' => Yii::t('models', 'Transfer Out'),
            'more_pay' => Yii::t('models', 'More Pay'),
            'booking_total' => Yii::t('models', 'Booking Total'),
            'approved' => Yii::t('models', 'Approved'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Apartment::className(), ['id' => 'object_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['booking_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
