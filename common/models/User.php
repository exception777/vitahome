<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

class User extends \dektrium\user\models\User
{
    public $_roles = [];
    public $_roleItems;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][]   = 'percent';
        $scenarios['update'][]   = 'percent';
        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();

        $rules['percentDouble'] = ['percent', 'double'];

        return $rules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        $labels['percent'] = Yii::t('models', 'User percent');
        return $labels;
    }


    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->isNewRecord) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole('user');
            $auth->assign($role, $this->id);
        }
        return true;
    }

    public function afterDelete()
    {
        parent::afterDelete();

        return true;
    }

    public function getRoleItems()
    {
        if (!$this->_roleItems) {
            $this->_roleItems = Yii::$app->authManager->getRolesByUser($this->id);
        }
        return $this->_roleItems;
    }

    public function getRoles() {
        if (!$this->_roles) {
            foreach ($this->roleItems as $role) {
                $this->_roles[] = $role->name;
            }
        }
        return $this->_roles;
    }

    /**
     * @param $name string|array
     * @return bool
     */
    public function isRole($name)
    {
        if (is_array($name)) {
            foreach ($name as $v) {
                if (array_search($v, $this->roles) !== false) {
                    return true;
                }
            }
        } else {
            return array_search($name, $this->roles) !== false;
        }
        return false;
    }


    public static function dropList()
    {
        $users = self::find()->all();

        return ArrayHelper::map($users, 'id', 'username');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartments()
    {
        return $this->hasMany(Apartment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners()
    {
        return $this->hasMany(Banner::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportMessages()
    {
        return $this->hasMany(SupportMessage::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWallet()
    {
        return $this->hasOne(Wallet::className(), ['user_id' => 'id']);
    }
}