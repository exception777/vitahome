<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ical".
 *
 * @property int $id
 * @property int $apartment_id
 * @property string $title
 * @property string $ical_url
 * @property int $status_id
 * @property int $approved
 * @property string $last_update
 * @property string $created_at
 *
 * @property Apartment $apartment
 * @property Status $status
 */
class Ical extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ical';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'ical_url', 'approved'], 'required'],
            [['apartment_id', 'status_id', 'approved'], 'integer'],
            [['last_update', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['ical_url'], 'string', 'max' => 500],
            [['apartment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Apartment::className(), 'targetAttribute' => ['apartment_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'apartment_id' => Yii::t('models', 'Apartment ID'),
            'title' => Yii::t('models', 'Title'),
            'ical_url' => Yii::t('models', 'Ical Url'),
            'status_id' => Yii::t('models', 'Status ID'),
            'approved' => Yii::t('models', 'Approved'),
            'last_update' => Yii::t('models', 'Last Update'),
            'created_at' => Yii::t('models', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartment()
    {
        return $this->hasOne(Apartment::className(), ['id' => 'apartment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
}
