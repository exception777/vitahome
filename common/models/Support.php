<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "support".
 *
 * @property integer $id
 * @property string $title
 * @property integer $status
 * @property integer $user_id
 * @property string $created_at
 *
 * @property SupportMessage[] $supportMessages
 * @property User $user
 */
class Support extends \yii\db\ActiveRecord
{
    const STATUS_CLOSE = 0;
    const STATUS_OPEN = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'support';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status', 'user_id'], 'required'],
            [['status', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 256],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'title' => Yii::t('backend', 'Title'),
            'status' => Yii::t('backend', 'Status'),
            'user_id' => Yii::t('backend', 'User ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    public function getStatusLabel()
    {
        switch($this->status){
            case self::STATUS_CLOSE: $label = '<label class="label label-danger">' . Yii::t('backend', 'Status Close') . '</label>';
                break;
            case self::STATUS_OPEN: $label = '<label class="label label-info">' . Yii::t('backend', 'Status Default') . '</label>';
                break;
            default: $label = '';
        }
        return $label;
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_CLOSE => Yii::t('backend', 'Status Close'),
            self::STATUS_OPEN => Yii::t('backend', 'Status Default'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportMessages()
    {
        return $this->hasMany(SupportMessage::className(), ['support_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
