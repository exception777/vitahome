<?php

namespace common\models;

use Gregwar\Image\Image;
use Yii;

/**
 * This is the model class for table "apartment_photo".
 *
 * @property integer $id
 * @property integer $apartment_id
 * @property string $name
 * @property integer $position
 *
 * @property Apartment $apartment
 */
class ApartmentPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apartment_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apartment_id', 'name'], 'required'],
            [['apartment_id', 'position'], 'integer'],
            [['name'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'apartment_id' => 'Apartment ID',
            'name' => 'Name',
            'position' => 'Position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartment()
    {
        return $this->hasOne(Apartment::className(), ['id' => 'apartment_id']);
    }

    public function rotate($rotate)
    {
        if (!$this->getApartment()) {
            return false;
        }
        Image::open($this->apartment->getDirOrig($this->name))->rotate($rotate)->save($this->apartment->getDirOrig($this->name));
        Image::open($this->apartment->getDirOrig($this->name))->zoomCrop(800, 600)->save($this->apartment->getDirLarge($this->name));
        Image::open($this->apartment->getDirOrig($this->name))->zoomCrop(280, 200)->save($this->apartment->getDirSmall($this->name));
        return true;
    }
}
