<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "status".
 *
 * @property int $id
 * @property string $name
 * @property string $color
 * @property int $approved
 * @property string $created_at
 * @property int $paid
 *
 * @property Booking[] $bookings
 * @property Ical[] $icals
 */
class Status extends \yii\db\ActiveRecord
{
    const ENABLE = 1;
    const DISABLE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'required'],
            [['approved', 'paid'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'name' => Yii::t('models', 'Name'),
            'color' => Yii::t('models', 'Color'),
            'approved' => Yii::t('models', 'Approved'),
            'created_at' => Yii::t('models', 'Created At'),
            'paid' => Yii::t('models', 'Paid'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['status_id' => 'id']);
    }

    public static function dropList()
    {
        $items = self::find()->all();

        return ArrayHelper::map($items, 'id', 'name');
    }
}
