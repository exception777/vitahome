<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wallet_transaction".
 *
 * @property integer $id
 * @property integer $wallet_id
 * @property integer $amount
 * @property double $commission
 * @property string $comment
 * @property integer $status
 * @property string $created_at
 *
 * @property Wallet $wallet
 */
class WalletTransaction extends \yii\db\ActiveRecord
{
    const PAY_SERVICE = 1;
    const REFILL = 10;

    public static $statusLabel = [self::PAY_SERVICE => 'Оплата рекламы', self::REFILL => 'Пополнение счета'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wallet_id', 'amount', 'status'], 'required'],
            [['wallet_id', 'amount', 'status'], 'integer'],
            [['commission'], 'number'],
            [['created_at'], 'safe'],
            [['comment'], 'string', 'max' => 256],
            [['wallet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wallet::className(), 'targetAttribute' => ['wallet_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'wallet_id' => Yii::t('backend', 'Wallet ID'),
            'amount' => Yii::t('backend', 'Amount'),
            'commission' => Yii::t('backend', 'Commission'),
            'comment' => Yii::t('backend', 'Comment'),
            'status' => Yii::t('backend', 'Status'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWallet()
    {
        return $this->hasOne(Wallet::className(), ['id' => 'wallet_id']);
    }

    public function statusLabel()
    {
        return self::$statusLabel[$this->status];
    }
}
