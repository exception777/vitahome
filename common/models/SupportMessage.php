<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "support_message".
 *
 * @property integer $id
 * @property integer $support_id
 * @property string $message
 * @property integer $status
 * @property integer $user_id
 * @property string $created_at
 *
 * @property Support $support
 * @property User $user
 */
class SupportMessage extends \yii\db\ActiveRecord
{
    const STATUS_DEFAULT = 0;
    const STATUS_NEW = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'support_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['support_id', 'message', 'user_id'], 'required'],
            [['support_id', 'status', 'user_id'], 'integer'],
            [['message'], 'string'],
            [['created_at'], 'safe'],
            [['support_id'], 'exist', 'skipOnError' => true, 'targetClass' => Support::className(), 'targetAttribute' => ['support_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'support_id' => Yii::t('backend', 'Support ID'),
            'message' => Yii::t('backend', 'Message'),
            'status' => Yii::t('backend', 'Status'),
            'user_id' => Yii::t('backend', 'User ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupport()
    {
        return $this->hasOne(Support::className(), ['id' => 'support_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
