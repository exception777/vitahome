<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $salary array */
/* @var $salaryDate \DateTime */

$this->title = Yii::t('backend', 'Salary');
$this->params['breadcrumbs'][] = $this->title;

$total = 0;

$fmt = new \IntlDateFormatter(
    'ru_RU',
    \IntlDateFormatter::FULL,
    \IntlDateFormatter::FULL,
    Yii::$app->getTimeZone(),
    \IntlDateFormatter::GREGORIAN,
    'LLLL Y'
);
$startDate = new \DateTime('2018-03-01');
$nowDate = new \DateTime();
$dateArr = [];
while ($startDate <= $nowDate) {
    $dateArr[$startDate->format('Y-m-d')] = $fmt->format($startDate);
    $startDate->modify('+1 month');
}
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <?php $form = ActiveForm::begin();?>

        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
                <div class="input-group">
                    <?= Html::dropDownList('salary_date', $salaryDate->format('Y-m-01'), $dateArr, ['class' => 'form-control']);?>
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-eye"></i> Показать</button>
                    </span>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="box-body table-responsive no-padding">

        <div class="box-group" id="accordion">
            <?php foreach ($salary as $key => $item):
                $totalSalary = ($item['total'] * $item['percent'] / 100);
                $bookingTotalSalary = ($item['booking_total'] * $item['percent'] / 100);
                $total += $totalSalary;
                ?>
                <div class="panel box box-solid">
                    <div class="box-header with-border">
                        <h4 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?= $key?>">
                                <?= $item['name']?>: <?= Yii::$app->formatter->asCurrency($totalSalary)?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse-<?= $key?>" class="panel-collapse collapse">
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>За проживание</dt>
                                <dd><?= Yii::$app->formatter->asCurrency($item['total']) .' % ' . $item['percent'] .
                                    ' = ' . Yii::$app->formatter->asCurrency($totalSalary)?></dd>
                                <dt>Всего к оплате</dt>
                                <dd><?= Yii::$app->formatter->asCurrency($item['booking_total']) .' % ' . $item['percent'] .
                                    ' = ' . Yii::$app->formatter->asCurrency($bookingTotalSalary)?></dd>
                            </dl>

                            <?php
                            if ($item['bookings']) {
                                Pjax::begin();
                                echo \yii\grid\GridView::widget([
                                    'dataProvider' => new \yii\data\ArrayDataProvider([
                                        'allModels' => $item['bookings'],
                                        'pagination' => ['pageSize' => 10]
                                    ]),
                                    'layout' => "{items}\n{summary}\n{pager}",
                                    'columns' => [
                                        'id',
                                        [
                                            'attribute' => 'object_id',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                return $model->object ? $model->object->title : Yii::t('yii', '(not set)');
                                            }
                                        ],
                                        [
                                            'attribute' => 'status_id',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                return $model->status ? '<span class="label" style="background-color: ' .
                                                    $model->status->color . '">' . $model->status->name . '</span>' :
                                                    Yii::t('yii', '(not set)');
                                            }
                                        ],
                                        [
                                            'attribute' => 'customer_id',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                $guest = '<div>' . $model->getAttributeLabel('customer_id') . ': ' . ($model->customer ?
                                                        $model->customer->name : Yii::t('yii', '(not set)')) . '</div>';
                                                return $guest . '<div class="text-muted">' . Yii::$app->formatter->asDatetime($model->date_in) .
                                                    ' - ' . Yii::$app->formatter->asDatetime($model->date_out) . '</div>';
                                            }
                                        ],
                                        [
                                            'attribute' => 'price',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                $price = '<div>' . $model->days . ' * ' . Yii::$app->formatter->asCurrency($model->price) .
                                                    ' = ' . Yii::$app->formatter->asCurrency($model->total) . '</div>';
                                                $bookingTotal = '<div>' . $model->getAttributeLabel('booking_total') . ': ' .
                                                    Yii::$app->formatter->asCurrency($model->booking_total) . '</div>';
                                                return $price . $bookingTotal;
                                            }
                                        ],
                                        [
                                            'attribute' => 'approved',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                return $model->approved ? '<i class="fa fa-check text-success"></i> ' :
                                                    '<i class="fa fa-times text-danger"></i> ';
                                            }
                                        ],
                                        [
                                            'attribute' => 'created_at',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                return '<div>' . Yii::$app->formatter->asDatetime($model->created_at) . '</div>';
                                            }
                                        ]
                                    ],
                                ]);
                                Pjax::end();
                            }
                            ?>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
    <div class="box-footer">
        <h4>Итого: <strong><?= Yii::$app->formatter->asCurrency($total)?></strong></h4>
    </div>
</div>
