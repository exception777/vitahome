<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $salary array */
/* @var $salaryDate \DateTime */

$this->title = Yii::t('backend', 'Incomes');
$this->params['breadcrumbs'][] = $this->title;
\backend\assets\MomentAsset::register($this);
?>

<div class="customer-index box box-primary">
    <div class="box-header with-border">
        <h4><?=$this->title?></h4>
        <?php $form = ActiveForm::begin(['id'=> 'form-payment', 'method' => 'get', 'action' => \yii\helpers\Url::to(['report/incomes'])]);?>
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div class="input-group drp-container">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <?php
                        echo \kartik\daterange\DateRangePicker::widget([
                            'name' => 'date_range',
                            'value' => $dateStart->format('Y-m-d') . ' - ' . $dateEnd->format('Y-m-d'),
                            'convertFormat' => true,
                            'useWithAddon' => true,
                            'startAttribute' => 'date_start',
                            'endAttribute' => 'date_end',
                            'pluginOptions' => [
                                'locale' => ['format'=>'Y-m-d']
                            ]
                        ]);
                        ?>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-flat" type="submit"  title="Показать">
                                <i class="fa fa-eye"></i></button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="box-body">
        <?php if ($chartData) {
            echo \miloschuman\highcharts\Highcharts::widget([
                'setupOptions' => [
                    'lang' => [
                        'loading' => 'Загрузка...',
                        'months' => ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                        'weekdays' => ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                        'shortMonths' => ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                        'exportButtonTitle' => "Экспорт",
                        'printButtonTitle' => "Печать",
                        'rangeSelectorFrom' => "С",
                        'rangeSelectorTo' => "По",
                        'rangeSelectorZoom' => "Период",
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'printChart' => 'Напечатать график'
                    ]
                ],
                'options' => [
                    'chart' => [
                        'type' => 'spline'
                    ],
                    'title' => [
                        'text' => 'Доходы'
                    ],
                    'xAxis' => [
                        'type' => 'datetime',
                        'dateTimeLabelFormats' => [
                            'month' => '%e. %b',
                            'year' => '%b'
                        ],
                    ],
                    'yAxis' => [
                        'title' => [
                            'text' => 'Оплата'
                        ],
                        'min' => 0
                    ],
                    'plotOptions' => [
                        'spline' => [
                            'marker' => [
                                'enabled' => true
                            ]
                        ]
                    ],
                    'time' => [
                        'timezone' => Yii::$app->getTimeZone()
                    ],
                    'series' => [
                        [
                            'name' => 'Оплата',
                            'data' => $chartData
                        ],
                    ]
                ],
                'scripts' => [
                    'modules/exporting',
                ],
            ]);
            echo '<p><strong>' . Yii::t('backend', 'Total period') . '</strong>: ' . Yii::$app->formatter->asCurrency($total) . '</p>';
        } else {
            echo '<h3>' . Yii::t('backend', 'No data') . '</h3>';
        }
        ?>
    </div>
</div>
