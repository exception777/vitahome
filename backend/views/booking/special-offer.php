<?php

use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ApartmentSearch */
/* @var $apartments common\models\Apartment */
/* @var $status common\models\Status */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Специальные предложения';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data'],
    'id' => 'apartment-form'
]); ?>
<div class="box box-solid">
    <div class="box-body">
        <div id="x-window" class="x_panel">
            <div id="x-alert"></div>
            <div class="x-grid-body">
                <div id="x-grid-items">
                    <div class="x-grid-header x-grid-items-header">
                        <table class="x-grid-table-header">
                            <tr><td>№</td><td>Квартира</td></tr>
                        </table>
                    </div>
                    <div class="x-grid-items-body">
                        <?php foreach($apartments as $value):?>
                            <div class="x-grid-block-body x-grid-block-body2">
                                <div class="x-grid-apartment-name" title="3:  - 10000" data-toggle="tooltip">
                                    №<?=$value->id?>:  - <?=$value->price?>&nbsp;тенге
                                    <div class="text-muted"><?= $value->address ?></div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
                <div id="x-grid-cells">
                    <div class="x-grid-header x-grid-cells-header">
                        <div class="x-table-header2">
                            <div class="x-table-header2-title">Цена</div>
                            <div class="x-table-header2-title">Цена со скидкой</div>
                            <div class="x-table-header2-title">Свободно сейчас</div>
                            <div class="x-table-header2-title">Позиция на главной</div>
                            <div class="x-table-header2-title">Поднятие</div>
                        </div>
                    </div>
                    <div class="x-grid-cells-body">
                        <?php foreach($apartments as $value):?>
                            <div class="x-grid-table-rows2" >
                                <div class="x-grid-table-rows2-item">
                                    <?= $form->field($value, '[' . $value->id . ']price')->textInput(['type' => 'number'])->label(false) ?>
                                </div>
                                <div class="x-grid-table-rows2-item">
                                    <?= $form->field($value, '[' . $value->id . ']discount_price')->textInput(['type' => 'number'])->label(false) ?>
                                </div>
                                <div class="x-grid-table-rows2-item">
                                    <?= $form->field($value, '[' . $value->id . ']is_free_now')->checkbox(['label' => ''])->label(false) ?>
                                </div>
                                <div class="x-grid-table-rows2-item">
                                    <?= $form->field($value, '[' . $value->id . ']position')->textInput(['type' => 'number'])->label(false) ?>
                                </div>
                                <div class="x-grid-table-rows2-item">
                                    <?= $form->field($value, '[' . $value->id . ']up_at')->widget(\kartik\datetime\DateTimePicker::classname(), [
                                        'options' => ['placeholder' => 'Выберите дату...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                        ]
                                    ])->label(false); ?>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
        <p></p>
        <p class="text-right">
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('backend', 'Save'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_edit']) ?>
        </p>
    </div>
</div>
<?php ActiveForm::end(); ?>
