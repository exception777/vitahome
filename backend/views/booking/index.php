<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Bookings');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <h3 class="box-title"><?=$this->title?></h3>
        <div class="box-tools">
            <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('backend', 'Create'), ['create'], ['class' => 'btn btn-sm btn-default btn-flat']) ?>
        </div>
    </div>
    <div class="box-body no-padding">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '<div class="table-responsive">{items}</div><div class="padding-md clearfix"><div class="pull-left">{summary}</div>{pager}</div>',
            'pager' => ['options' => ['class' => 'pagination pagination-sm no-margin pull-right']],
            'columns' => [
                'id',
                [
                    'attribute' => 'object_id',
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->object ? $model->object->ob_name : Yii::t('app', '(not set)');
                    }
                ],
                [
                    'attribute' => 'status_id',
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->status ? '<span class="label" style="background-color: ' .
                            $model->status->color . '">' . $model->status->name . '</span>' :
                            Yii::t('app', '(not set)');
                    }
                ],
                [
                    'attribute' => 'customer_id',
                    'format' => 'raw',
                    'value' => function($model) {
                        $guest = '<div>' . $model->getAttributeLabel('customer_id') . ': ' . ($model->customer ?
                            $model->customer->name : Yii::t('app', '(not set)')) . '</div>';
                        return $guest . '<div class="text-muted">' . Yii::$app->formatter->asDatetime($model->date_in) .
                            ' - ' . Yii::$app->formatter->asDatetime($model->date_out). '</div>';
                    }
                ],
                [
                    'attribute' => 'price',
                    'format' => 'raw',
                    'value' => function($model) {
                        $price = '<div>' . $model->days . ' * ' . Yii::$app->formatter->asCurrency($model->price) .
                            ' = ' . Yii::$app->formatter->asCurrency($model->total) . '</div>';
                        $bookingTotal = '<div>' . $model->getAttributeLabel('booking_total') . ': ' .
                            Yii::$app->formatter->asCurrency($model->booking_total) . '</div>';
                        return $price . $bookingTotal;
                    }
                ],
                'information',
                [
                    'attribute' => 'approved',
                    'format' => 'raw',
                    'value' => function($model) {
                        return  $model->approved ? '<i class="fa fa-check text-success"></i> ' :
                            '<i class="fa fa-times text-danger"></i> ';
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'format' => 'raw',
                    'value' => function($model) {
                        $user = '<div>' . $model->getAttributeLabel('user_id') . ': ' . ($model->user ?
                            $model->user->username : Yii::t('app', '(not set)')) . '</div>';
                        return $user . '<div>' . Yii::$app->formatter->asDatetime($model->created_at) . '</div>';
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update}'
                ],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
