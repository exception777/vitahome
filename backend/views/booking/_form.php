<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */
/* @var $form yii\bootstrap\ActiveForm */

$apartmets = ArrayHelper::map(\common\models\Apartment::find()->all(), 'id', 'ob_name');
$statuses = ArrayHelper::map(\common\models\Status::find()->all(), 'id', 'name');
$users = ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username');

\kartik\daterange\MomentAsset::register($this);
?>

<div class="box box-primary">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal'
    ]);
    ?>
    <div class="box-body">
        <?= Html::activeHiddenInput($model, 'customer_id')?>
        <?= $form->errorSummary($model, ['class' => 'alert alert-danger'])?>
        <div class="col-sm-6">

            <?= $form->field($model, 'object_id')->dropDownList($apartmets) ?>

            <?= $form->field($model, 'status_id')->dropDownList($statuses) ?>

            <div class="form-group field-booking-customer_id">
                <label class="control-label col-sm-3" for="booking-customer_id">Клиент</label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <div id="booking-customer_view" class="form-control child-block-inline">
                            <?php
                            if ($model->customer) {
                                $contact = '<div class="text-muted"><a href="tel:'.$model->customer->phone.'">
                                        <i class="fa fa-phone"></i> '.$model->customer->phone.'</a></div>';
                                echo '<div>' . $model->customer->name . '</div>' . $contact;
                            }
                            ?>
                        </div>
                        <span class="input-group-btn">
                            <button type="button" id="customers-list" data-url="<?= \yii\helpers\Url::to(['customer/list'])?>"
                                    class="btn btn-default btn-flat" title="Выбрать клиента"><i class="fa fa-search"></i> </button>
                        </span>
                    </div>
                </div>
            </div>

            <?= $form->field($model, 'days')->textInput() ?>

            <?= $form->field($model, 'date_in')->widget(\kartik\datetime\DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату...'],
                'pluginOptions' => [
                    'autoclose' => true,
                ],
                'pluginEvents' => [
                    'changeDate' => 'function(e) {  formCalculate(); }'
                ]
            ]); ?>

            <?= $form->field($model, 'date_out')->widget(\kartik\datetime\DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату...'],
                'pluginOptions' => [
                    'autoclose' => true,
                ],
                'pluginEvents' => [
                    'changeDate' => 'function(e) {  formCalculate();}'
                ]
            ]); ?>


        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'transfer_in')->textInput() ?>

            <?= $form->field($model, 'transfer_out')->textInput() ?>

            <?= $form->field($model, 'more_pay')->textInput() ?>

            <?= $form->field($model, 'price')->textInput() ?>

            <?= $form->field($model, 'total')->textInput() ?>

            <?= $form->field($model, 'booking_total')->textInput() ?>

            <?= $form->field($model, 'information')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'approved')->checkbox() ?>

            <?= $form->field($model, 'user_id')->dropDownList($users) ?>

            <?php if (!$model->isNewRecord):?>
                <div class="text-muted col-sm-offset-3">Добавлено: <?= Yii::$app->formatter->asDatetime($model->created_at)?></div>
            <?php endif;?>
        </div>
    </div>
    <div class="box-footer">
        <?php if($model->isNewRecord): ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app', 'CreateEdit'),
                ['class' => 'btn btn-success btn-flat', 'name' => 'btn_create_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . Yii::t('app', 'CreateList'),
                ['class' => 'btn btn-success btn-flat', 'name' => 'btn_create_and_list']) ?>
            <?= Html::submitButton('<i class="fa fa-plus-circle"></i> ' . Yii::t('app', 'CreateCreate'),
                ['class' => 'btn btn-success  btn-flat', 'name' => 'btn_create_and_create']) ?>
        <?php else: ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app', 'Save'),
                ['class' => 'btn btn-primary btn-flat', 'name' => 'btn_update_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . Yii::t('app', 'SaveList'),
                ['class' => 'btn btn-primary btn-flat', 'name' => 'btn_update_and_list']) ?>
        <?php endif; ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

Modal::begin([
    'id' => 'modal',
    'header' => 'Выбор клиента',
    'size' => 'modal-lg',
]);

    echo '<div id="modal-content"><div style="text-align:center"><img src="/images/loader.gif"></div></div>';

Modal::end();


$this->registerJs(<<<JS
 $('#customers-list').click(function () {
        $('#modal')
            .modal('show')
            .find('#modal-content')
            .load($(this).data('url'));
    });

$('.modal-content').on('click', '.customer-select', function() {
  $('#booking-customer_id').val($(this).data('id'));
  $('#booking-customer_view').html($(this).parents('tr').find('.customer-name').html());
  $('#modal').modal('hide');
});
    $('#booking-price').on('keyup', function () {
        formCalculatePayment();
    });
    $('#booking-transfer_in').on('keyup', function () {
        formCalculatePayment();
    });
    $('#booking-transfer_out').on('keyup', function () {
        formCalculatePayment();
    });
    $('#booking-more_pay').on('keyup', function () {
        formCalculatePayment();
    });
    $('#booking-days').on('keyup', function () {
        formCalculatePayment();
    });

    function formCalculate() {
        formCalculateDays();
        formCalculatePayment();
    }

    function formCalculateDays() {
        let sDate = moment($('#booking-date_in').val()),
            eDate = moment($('#booking-date_out').val()),
            days;
        days = getRangeDateDays(sDate.valueOf(), eDate.valueOf());
        if (days <= 0) days = 1;
        $('#booking-days').val(days);
    }

    function formCalculatePayment() {
        $('#booking-total').val($('#booking-days').val() * $('#booking-price').val());
        $('#booking-booking_total').val(parseInt($('#booking-total').val()) + parseInt($('#booking-transfer_in').val()) + 
        parseInt($('#booking-transfer_out').val()) + parseInt($('#booking-more_pay').val()));
    }
    
    function getRangeDateDays(date1, date2) {
        date1 = new Date(date1);
        date1.setHours(0, 0, 0, 0);
        date2 = new Date(date2);
        date2.setHours(0, 0, 0, 0);
        var days = Math.floor((date2.getTime() - date1.getTime()) / (3600000 * 24));
        return days;
    }
JS
);
?>