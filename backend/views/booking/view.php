<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> <?= Yii::$app->name?>
            <small class="pull-right">Дата: <?= date('d.m.Y', strtotime($model->created_at))?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
          <!-- /.col -->
          <div class="col-sm-4 invoice-col">
              <b>Бронь: <?= $model->id?></b><br>
              <b>Квартира:</b> <?= $model->object->ob_name?><br>
              <b>Статус:</b> <?= $model->status->name?><br>
              <b>Утверждена:</b> <?= $model->approved ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-remove text-danger"></i>'?>
          </div>
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Гость: <?= $model->customer->name ?></strong><br>
            Телефон: <?= $model->customer->phone ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <b>Заезд:</b> <?= Yii::$app->formatter->asDatetime($model->date_in)?><br>
            <b>Выезд:</b> <?= Yii::$app->formatter->asDatetime($model->date_out)?><br>
            <b>Сутки:</b> <?= $model->days?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Кол-во</th>
              <th>Услуга</th>
              <th>Описание</th>
              <th>Сумма</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>1</td>
              <td>Проживание</td>
              <td><?= $model->object->ob_name?></td>
              <td><?= Yii::$app->formatter->asCurrency($model->price)?></td>
            </tr>
            <?php if ($model->more_pay):?>
            <tr>
              <td>1</td>
              <td>Доп. оплата</td>
              <td></td>
              <td><?= Yii::$app->formatter->asCurrency($model->more_pay)?></td>
            </tr>
            <?php endif;?>
            <?php if ($model->transfer_in):?>
            <tr>
              <td>1</td>
              <td>Трансфер</td>
              <td>Встреча</td>
              <td><?= Yii::$app->formatter->asCurrency($model->transfer_in)?></td>
            </tr>
            <?php endif;?>
            <?php if ($model->transfer_out):?>
            <tr>
              <td>1</td>
              <td>Трансфер</td>
              <td>Проводы</td>
              <td><?= Yii::$app->formatter->asCurrency($model->transfer_out)?></td>
            </tr>
            <?php endif;?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12 col-sm-6">
          <p class="lead">Оплата</p>

          <div class="table-responsive">
            <table class="table">
              <tbody>
              <tr>
                <th>Сутки:</th>
                <td><?= $model->days?></td>
              </tr>
              <tr>
                <th>За проживание:</th>
                <td><?= Yii::$app->formatter->asCurrency($model->total)?></td>
              </tr>
              <tr>
                <th>Всего:</th>
                <td><?= Yii::$app->formatter->asCurrency($model->booking_total)?></td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
            <?= Html::a('<i class="fa fa-edit"></i> ' . Yii::t('backend', 'Update'), ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary btn-flat']) ?>
         <!--   <button type="button" class="btn btn-primary" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Экспорт PDF
            </button>
            <button type="button" class="btn btn-primary" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Счет на оплату
            </button>
            <button type="button" class="btn btn-primary" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Отчетные документы
            </button>
            <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> -->
        </div>
      </div>
    </section>