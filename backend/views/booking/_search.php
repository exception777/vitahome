<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\BookingSearch */
/* @var $form yii\widgets\ActiveForm */

$objects = ArrayHelper::map(\common\models\Apartment::find()->all(), 'id', 'ob_name');
$statuses = ArrayHelper::map(\common\models\Status::find()->all(), 'id', 'name');
$users = \common\models\User::dropList();
?>

<?php $this->beginContent('@backend/views/layouts/blocks/_search.php',
    ['filterActive' => (bool)Yii::$app->request->get('BookingSearch')]); ?>
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'data-pjax' => 1
            ],
        ]); ?>

        <div class="row">
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'id') ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'object_id')->dropDownList($objects, ['prompt' => Yii::t('yii', '(not set)')]) ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'status_id')->dropDownList($statuses, ['prompt' => Yii::t('yii', '(not set)')]) ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'user_id')->dropDownList($users, ['prompt' => Yii::t('yii', '(not set)')]) ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'customer_id') ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'approved')->dropDownList(Yii::$app->params['yesNo'], ['prompt' => Yii::t('yii', '(not set)')]) ?>
            </div>
            <div class="col-md-3 col-sm-4">
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-search"></i> ' . Yii::t('backend', 'Search'), ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('<i class="fa fa-refresh"></i> ' . Yii::t('backend', 'Reset'), ['/booking/index'], ['class' => 'btn btn-default btn-flat']) ?>
        </div>
        <?php ActiveForm::end(); ?>

<?php $this->endContent() ?>