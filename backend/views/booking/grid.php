<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ApartmentSearch */
/* @var $apartments common\models\Apartment */
/* @var $status common\models\Status */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Бронирование';
$this->params['breadcrumbs'][] = $this->title;
$apartmentsJs = [];
foreach ($apartments as $value) {
    $address = $value->address ? '<div class=\'text-muted\'>' . $value->address . '</div>' : '';
    $title = $value->ob_name . ' - ' . Yii::$app->formatter->asCurrency($value->price) . $address;
    $apartmentsJs[] = '{id: ' . $value->id . ', name: "' . $value->ob_name . '", title: "' . $title . '", price: ' . $value->price . '}';
}

\backend\assets\BookingAsset::register($this);

$this->registerJs("
var admin = " . (Yii::$app->user->identity->isRole(['root', 'admin']) ? 'true': 'false') . ";
var apartments = [".implode(',', $apartmentsJs)."];
var urlCustomerSearch = '".Url::toRoute(['ajax/customer-search'])."';
var urlBookingData = '".Url::toRoute(['ajax/booking-data'])."';
var urlBookingSave = '".Url::toRoute(['ajax/booking-save'])."';
var urlBookingDelete = '".Url::toRoute(['ajax/booking-delete'])."';
var urlPaymentSave = '".Url::toRoute(['ajax/payment-save'])."';
", $this::POS_BEGIN);

?>

<div class="box box-solid">
    <div class="box-header">
            <div class="btn-group">
                <button class="btn btn-sm btn-default btn-flat" id="back" title="Назад на 7 дней">
                    <i class="fa fa-angle-double-left"></i>
                </button>
                <button class="btn btn-sm btn-default btn-flat" id="back_day" title="Назад на день">
                    <i class="fa fa-angle-left"></i>
                </button>
                <div class="btn-group">
                    <button class="btn btn-sm btn-default btn-flat dropdown-toggle" title="Перейти" data-toggle="dropdown" aria-expanded="false">
                        Перейти  <i class="fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#" id="now_day" >Сегодня</a></li>
                        <li class="divider"></li>
                        <li>
                            <div class="form-group">
                                <div class="input-group date input-group-sm margin">
                                    <input type="text" name="date_step" id="date-step-datepicker" class="form-control"/>
                                    <span class="input-group-btn">
                            <button type="button" class="btn btn-default btn-flat" id="btn-date-step" title="Перейти к дате">
                                <i class="fa fa-arrow-right"></i></button>
                        </span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <button class="btn btn-sm btn-default btn-flat" id="next_day" title="Вперед на день">
                    <i class="fa fa-angle-right"></i>
                </button>
                <button class="btn btn-sm btn-default btn-flat" id="next" title="Вперед">
                    <i class="fa fa-angle-double-right"></i>
                </button>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm btn-flat" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-calendar"></i> <span class="hidden-xs">Размер сетки</span> <i class="fa fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#" id="week1">1 Неделя</a></li>
                    <li><a href="#" id="week2">2 Недели</a></li>
                    <li><a href="#" id="month">Месяц</a></li>
                </ul>
            </div>
            <div class="btn-group">
                <button class="btn btn-sm btn-default btn-flat" id="refresh"><i class="fa  fa-refresh"></i>
                    Обновить
                </button>
            </div>
            <div class="btn-group">
                <div class="input-group input-group-sm" style="width: 150px">
                    <input type="text" class="form-control" name="input_search_by_id" id="input-search_by_id" placeholder="Поиск по id">
                    <span class="input-group-btn">
            <button type="button" class="btn btn-default btn-flat" id="btn-search_by_id"><i class="fa fa-search"></i></button>
        </span>
                </div>
            </div>
    </div>
    <div class="box-body">
            <div id="x-window" class="x_panel">
                <div id="x-alert"></div>
                <div class="x-grid-body">
                    <div id="x-grid-items">
                        <div class="x-grid-header x-grid-items-header">
                            <table class="x-grid-table-header">
                                <tr><td>№</td><td>Квартира</td></tr>
                            </table>
                        </div>
                        <div class="x-grid-items-body"></div>
                    </div>
                    <div id="x-grid-cells">
                        <div class="x-grid-header x-grid-cells-header"></div>
                        <div class="x-grid-cells-body">
                            <table id="x-grid-table-rows">
                                <?php foreach($apartments as $value):?>
                                    <tr><td data-id="<?=$value->id?>" data-price="<?=$value->price?>"></td></tr>
                                <?php endforeach;?>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="overlay" id="overlay" style="display:none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
    </div>
</div>

<div class="modal fade" id="booking-form" tabindex="-1" role="dialog" aria-labelledby="booking-form-label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content nav-tabs-custom">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#booking_tab" data-toggle="tab" class="modal-title"
                                          id="booking-form-label"></a></li>
                    <li><a href="#payments_tab" data-toggle="tab">Оплаты</a></li>
                </ul>
            </div>
            <div class="modal-body tab-content">
                <div id="modal-booking-info"></div>
                <div class="tab-pane active" id="booking_tab">
                    <input type="hidden" name="form[id]" id="form-id">
                    <input type="hidden" name="form[customer][id]" id="form-customer-id">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="form-apartment" class="control-label">Апартаменты:</label>
                                <select name="form[apartment]" id="form-apartment" class="form-control">
                                    <?php foreach ($apartments as $value): ?>
                                        <option value="<?= $value->id ?>"><?= $value->id . ' - ' . $value->ob_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="form-status" class="control-label">Статус:</label>
                                <select name="form[status]" id="form-status" class="form-control">
                                    <?php foreach ($statuses as $value): ?>
                                        <option value="<?= $value->id ?>"><?= $value->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="form-start-date" class="control-label">Заезд:</label>
                                <div class='input-group date' id='form-start-datepicker'>
                                    <input type="text" name="form[startDate]" id="form-start-date"
                                           class="form-control"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="form-end-date" class="control-label">Выезд:</label>
                                <div class='input-group date' id='form-end-datepicker'>
                                    <input type="text" name="form[endDate]" id="form-end-date" class="form-control"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="form-comment" class="control-label">Информация к брони:</label>
                                <textarea name="form[comment]" id="form-comment" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p style="text-align: center;"><strong>Клиент</strong></p>
                                    <div class="form-group">
                                        <label for="form-customer-name" class="control-label">Поиск клиента:</label>
                                        <div class="input-group">
                                            <input type="text" name="form-customer-search" id="form-customer-search"
                                                   class="form-control" value="" placeholder="Введите телефон или имя гостя">
                                            <div id="modal-add-customer" class="input-group-addon"><i
                                                        class="fa fa-search"></i></div>
                                            <div id="customer-result"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 form-horizontal">
                                    <div class="form-group">
                                        <label for="form-customer-name" class="col-sm-4 control-label">Ф.И.О:</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="form[customer][name]" id="form-customer-name"
                                                       class="form-control" value="">
                                                <div id="modal-clear-customer" class="input-group-addon"><i
                                                            class="fa fa-times"></i></div>
                                            </div>
                                        </div>
                                        <div id="customer-profile" class="col-sm-offset-4 col-sm-8 text-center padding-top-sm"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="form-customer-phone" class="col-sm-4 control-label">Телефон:</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="form[customer][phone]" id="form-customer-phone"
                                                   class="form-control" value="" maxlength="20">
                                        </div>
                                        <div class="col-sm-offset-4 col-sm-8 text-muted text-sm">Формат: 77010123456</div>
                                        <div id="phone-call" class="col-sm-offset-4 col-sm-8 text-center padding-top-sm"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="form-customer-email" class="col-sm-4 control-label">Email:</label>
                                        <div class="col-sm-8">
                                            <input type="email" name="form[customer][email]" id="form-customer-email"
                                                   class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="form-customer-information"
                                               class="col-sm-4 control-label">Информация:</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="form[customer][information]" id="form-customer-information"
                                                   class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-6 col-xs-12 form-horizontal">
                            <p style="text-align: center;"><strong>Калькуляция</strong></p>
                            <div class="form-group">
                                <label for="form-days" class="col-sm-4 control-label">Сутки:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[days]" id="form-days" class="form-control"
                                           value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-transfer_in" class="col-sm-4 control-label">Трансфер встреча:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[transfer_in]" id="form-transfer_in"
                                           class="form-control" value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-transfer_out" class="col-sm-4 control-label">Трансфер проводы:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[transfer_out]" id="form-transfer_out"
                                           class="form-control" value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-more_pay" class="col-sm-4 control-label">Доп. сумма:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[more_pay]" id="form-more_pay" class="form-control"
                                           value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-price" class="col-sm-4 control-label">Цена:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[price]" id="form-price" class="form-control"
                                           value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-total" class="col-sm-4 control-label">За проживание:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[total]" id="form-total" class="form-control"
                                           value="0" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-booking_total" class="col-sm-4 control-label">Всего:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[booking_total]" id="form-booking_total"
                                           class="form-control" value="0" readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-sm-6 text-left">
                                <button type="button" class="btn btn-danger btn-flat" id="btn-reserv-remove"><i
                                            class="fa fa-times"></i> Снять бронь
                                </button>
                            </div>
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-primary btn-flat" id="btn-reserv-save"><i
                                            class="fa fa-save"></i> Сохранить
                                </button>
                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"> Закрыть</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane" id="payments_tab">
                    <div class="row">
                        <div class="col-sm-12" id="booking-payments"></div>
                    </div>
                    <input type="hidden" name="form[payment][id]" id="form-payment-id" value="">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label for="form-payment-date" class="control-label">Дата оплаты:</label>
                                <div class='input-group date' id='form-payment-datepicker'>
                                    <input type="text" name="form[payment][payDate]" id="form-payment-date"
                                           class="form-control"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label for="form-payment-price" class="control-label">Оплачено:</label>
                                <input type="text" name="form[payment][price]" id="form-payment-price"
                                       class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label for="form-payment-comment" class="control-label">Комментарий:</label>
                                <input type="text" name="form[payment][comment]" id="form-payment-comment"
                                       class="form-control" value="">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="button" class="btn btn-primary btn-flat" id="btn-payment-save"><i
                                            class="fa fa-save"></i> Сохранить
                                </button>
                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"> Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="overlay" id="overlayModal" style="display: none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>



<?php /*
<div class="modal fade" id="booking-form" tabindex="-1" role="dialog" aria-labelledby="booking-form-label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box nav-tabs-custom">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#booking_tab" data-toggle="tab" class="modal-title" id="booking-form-label"></a></li>
                    <li><a href="#payments_tab" data-toggle="tab">Оплаты</a></li>
                </ul>
            </div>
            <div class="modal-body tab-content">
                <div class="tab-pane active" id="booking_tab">
                    <input type="hidden" name="form[id]" id="form-id">
                    <input type="hidden" name="form[customer][id]" id="form-customer-id">
                    <div id="modal-booking-info"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form-apartment" class="control-label">Апартаменты:</label>
                                <select name="form[apartment]" id="form-apartment" class="form-control">
                                    <?php foreach($apartments as $value):?>
                                        <option value="<?=$value->id?>"><?=$value->id .' - ' .$value->ob_name?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form-status" class="control-label">Статус:</label>
                                <select name="form[status]" id="form-status" class="form-control">
                                    <?php foreach($status as $value):?>
                                        <option value="<?=$value->id?>"><?=$value->name?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="form-start-date" class="control-label">Заезд:</label>
                                <div class='input-group date' id='form-start-datepicker'>
                                    <input type="text" name="form[startDate]" id="form-start-date"  class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="form-end-date" class="control-label">Выезд:</label>
                                <div class='input-group date' id='form-end-datepicker'>
                                    <input type="text" name="form[endDate]" id="form-end-date" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="form-comment" class="control-label">Коментарий к брони:</label>
                                <textarea name="form[comment]" id="form-comment" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-xs-12 form-horizontal">
                            <p style="text-align: center;"><strong>Гость</strong></p>
                            <div class="form-group">
                                <label for="form-customer-name" class="col-sm-4 control-label">Ф.И.О:</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" name="form[customer][name]" id="form-customer-name" class="form-control" value="">
                                        <div id="modal-add-customer" class="input-group-addon"><i class="fa fa-search"></i></div>
                                        <div id="modal-clear-customer" class="input-group-addon"><i class="fa fa-times"></i></div>
                                        <div id="customer-result"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-customer-phone" class="col-sm-4 control-label">Телефон:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="form[customer][phone]" id="form-customer-phone" class="form-control" value="">
                                </div>
                                <div id="phone-call" class="col-md-12"></div>
                            </div>
                            <div class="form-group">
                                <label for="form-customer-inn" class="col-sm-4 control-label">ИИН(ИНН):</label>
                                <div class="col-sm-8">
                                    <input type="text" name="form[customer][inn]" id="form-customer-inn" class="form-control" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-customer-document" class="col-sm-4 control-label">Документ:</label>
                                <div class="col-sm-8">
                                    <?=Html::dropDownList('form[customer][document]', null, Yii::$app->params['documentTypes'],
                                        ['id' => "form-customer-document", 'class' => "form-control"])?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-customer-document-number" class="col-sm-4 control-label">№ документа:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="form[customer][documentNumber]" id="form-customer-document-number" class="form-control" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-customer-document-date" class="col-sm-4 control-label">Выдан:</label>
                                <div class="col-sm-8">
                                    <div class="input-group date" id='form-customer-document-datepicker'>
                                        <input type="text" name="form[customer][documentDate]" id="form-customer-document-date" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 form-horizontal">
                            <p style="text-align: center;"><strong>Калькуляция</strong></p>
                            <div class="form-group">
                                <label for="form-days" class="col-sm-4 control-label">Сутки:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[days]" id="form-days" class="form-control" value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-price" class="col-sm-4 control-label">Цена:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[price]" id="form-price" class="form-control" value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-discount" class="col-sm-4 control-label">Скидка:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[discount]" id="form-discount" class="form-control" value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-total" class="col-sm-4 control-label">Всего:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="form[total]" id="form-total" class="form-control" value="0" readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <button type="button" class="btn btn-danger" id="btn-reserv-remove"><i class="fa fa-times"></i> Удалить</button>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-primary" id="btn-reserv-save"><i class="fa fa-save"></i> Сохранить</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"> Закрыть</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane" id="payments_tab">
                    <div id="modal-booking-payment-info"></div>
                    <div class="row"><div class="col-md-12" id="booking-payments"></div></div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="form-payment-date" class="control-label">Дата оплаты:</label>
                                <div class='input-group date' id='form-payment-datepicker'>
                                    <input type="text" name="form[payment][payDate]" id="form-payment-date"  class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="form-payment-price" class="control-label">Оплачено:</label>
                                <input type="text" name="form[payment][price]" id="form-payment-price" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="form-payment-comment" class="control-label">Коментарий:</label>
                                <input type="text" name="form[payment][comment]" id="form-payment-comment" class="form-control" value="">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary" id="btn-payment-save"><i class="fa fa-save"></i> Сохранить</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"> Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="overlay" id="overlayModal" style="display: none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div> */