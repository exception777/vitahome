<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Area */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Район', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
