<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\City;

/* @var $this yii\web\View */
/* @var $model common\models\Area */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusList']) ?>

        <?=$form->field($model, 'city_id')->dropDownList(City::allCities(), [ 'style' => 'width:100%']) ?>

    </div>

    <div class="box-footer">
        <?php if($model->isNewRecord): ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('backend', 'CreateEdit'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . \Yii::t('backend', 'CreateList'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_list']) ?>
            <?= Html::submitButton('<i class="fa fa-plus-circle"></i> ' . \Yii::t('backend', 'CreateCreate'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_create']) ?>
        <?php else: ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('backend', 'Save'),
                ['class' => 'btn btn-primary', 'name' => 'btn_update_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . \Yii::t('backend', 'SaveList'),
                ['class' => 'btn btn-primary', 'name' => 'btn_update_and_list']) ?>
        <?php endif; ?>
    </div>

<?php ActiveForm::end(); ?>

</div>