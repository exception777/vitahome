<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Районы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <h3 class="box-title"><?=$this->title?></h3>
        <div class="box-tools">
            <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('backend', 'Create'), ['create'], ['class' => 'btn btn-sm btn-default btn-flat']) ?>
        </div>
    </div>
    <div class="box-body no-padding">

    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => '<div class="table-responsive">{items}</div><div class="padding-md clearfix"><div class="pull-left">{summary}</div>{pager}</div>',
            'pager' => ['options' => ['class' => 'pagination pagination-sm no-margin pull-right']],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function($model){
                        return '<span class="label ' . ($model->status ? 'label-success' : 'label-danger') . '">' .
                        Yii::$app->params['statusList'][$model->status] . '</span>';
                    }
                ],
                [
                    'attribute' => 'city_id',
                    'value' => 'city.name',
                    'filterInputOptions' => ['class' => 'form-control'],
                    'filter' => \common\models\City::allCities()
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                ],
            ],
        ]); ?>
    </div>

    <?php Pjax::end(); ?>
</div>
