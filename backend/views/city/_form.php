<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\City */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
$location = explode(',', $model->location);
$x = 0;
if(isset($location[0])){
    if(is_numeric($location[0])){
        $x = $location[0];
    }
}
$y = 0;
if(isset($location[1])) {
    if(is_numeric($location[1])){
        $y = $location[1];
    }
}
$this->registerJs('
ymaps.ready(function(){
	map = new ymaps.Map ("map",{center:[' . ($x?:'51.128422') . ',' . ($y?:'71.430564') . '],zoom:14},{});
	map.events.add("click", function (e) {
            var coords = e.get("coords");
            $("#city-location").val(coords[0].toPrecision(8)+","+coords[1].toPrecision(8));
            map.balloon.open(coords, {
                contentHeader: "Установлена метка",
                contentBody:
                    "<p>Координаты на карте: " + [
                        coords[0].toPrecision(8),
                        coords[1].toPrecision(8)
                    ].join(", ") + "</p>"
            });

        });

    var x = ' . $x . ', y = ' . $y . ';
    if(x != 0 && y != 0){
        map.balloon.open([x, y],{
            contentBody: "<p>Координаты на карте: " + [x, y].join(",") + "</p>"
        });
    }
});');

?>

<div class="box box-primary">
<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusList']) ?>
                <?= $form->field($model, 'alias')->textInput(['maxlength' => true])->hint('Оставьте поле пустым, что бы альяс сформировался автоматически') ?>
            </div>
        </div>

        <?= $form->field($model, 'location')->hiddenInput();?>
        <div id="map" style="width: 100%; height: 400px; margin-bottom:10px"></div>

    </div>

    <div class="box-footer">
        <?php if($model->isNewRecord): ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('backend', 'CreateEdit'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . \Yii::t('backend', 'CreateList'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_list']) ?>
            <?= Html::submitButton('<i class="fa fa-plus-circle"></i> ' . \Yii::t('backend', 'CreateCreate'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_create']) ?>
        <?php else: ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('backend', 'Save'),
                ['class' => 'btn btn-primary', 'name' => 'btn_update_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . \Yii::t('backend', 'SaveList'),
                ['class' => 'btn btn-primary', 'name' => 'btn_update_and_list']) ?>
        <?php endif; ?>
    </div>

<?php ActiveForm::end(); ?>
</div>
