<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Города';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <h3 class="box-title"><?=$this->title?></h3>
        <div class="box-tools">
            <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('backend', 'Create'), ['create'], ['class' => 'btn btn-sm btn-default btn-flat']) ?>
        </div>
    </div>
    <div class="box-body no-padding">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '<div class="table-responsive">{items}</div><div class="padding-md clearfix"><div class="pull-left">{summary}</div>{pager}</div>',
            'pager' => ['options' => ['class' => 'pagination pagination-sm no-margin pull-right']],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'title',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function($model){
                        return '<span class="label ' . ($model->status ? 'label-success' : 'label-danger') . '">' .
                        Yii::$app->params['statusList'][$model->status] . '</span>';
                    }
                ],
                'alias',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                ],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
