<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use common\models\WalletTransaction;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */

$this->title = empty($profile->name) ? Html::encode($profile->user->username) : Html::encode($profile->name);
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <?= Html::img($profile->getAvatarUrl(128), [
                        'class' => 'profile-user-img img-responsive img-circle',
                        'alt' => $profile->user->username,
                    ]) ?>
                    <h3 class="profile-username text-center"><?= $this->title ?></h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Объявлений</b> <a class="pull-right"><?= count($profile->user->apartments)?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Баланс</b>
                            <a href="#push-balance" data-toggle="modal" data-target="#push-balance" class="pull-right"
                               title="<?= Yii::t('backend', 'Push balance')?>" id="user-balance">
                                <?= Yii::$app->formatter->asCurrency($profile->user->wallet->balance)?></a>
                        </li>
                    </ul>

                    <?= Html::a(Yii::t('user', 'Update'), ['/user/admin/update', 'id' => $profile->user_id],
                        ['class' => 'btn btn-primary btn-block btn-flat'])?>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Профиль</h3>
                </div>
                <div class="box-body">
                    <?php if (!empty($profile->location)): ?>
                        <strong><i class="fa fa-map-marker margin-r-5"></i> <?= $profile->getAttributeLabel('location')?></strong>
                        <p class="text-muted"><?= Html::encode($profile->location) ?></p>
                        <hr>
                    <?php endif; ?>
                    <?php if (!empty($profile->website)): ?>
                        <strong><i class="glyphicon glyphicon-globe margin-r-5"></i> <?= $profile->getAttributeLabel('website')?></strong>
                        <p class="text-muted"><?= Html::a(Html::encode($profile->website), Html::encode($profile->website)) ?></p>
                        <hr>
                    <?php endif; ?>
                    <?php if (!empty($profile->public_email)): ?>
                        <strong><i class="glyphicon glyphicon-envelope margin-r-5"></i> <?= $profile->getAttributeLabel('public_email')?></strong>
                        <p class="text-muted"><?= Html::a(Html::encode($profile->public_email), 'mailto:' . Html::encode($profile->public_email)) ?></p>
                        <hr>
                    <?php endif; ?>
                    <?php if (!empty($profile->bio)): ?>
                        <strong><i class="fa fa-file-text-o margin-r-5"></i> <?=$profile->getAttributeLabel('bio')?></strong>
                        <p><?= Html::encode($profile->bio) ?></p>
                        <hr>
                    <?php endif; ?>
                    <p class="text-muted">
                        <i class="glyphicon glyphicon-time"></i> <?= Yii::t('user', 'Joined on {0, date}', $profile->user->created_at) ?>
                    </p>
                </div>
            </div>
        </div>

        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">Объявления</a></li>
                    <li><a href="#timeline" data-toggle="tab">Транзакции</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <table class="table">
                            <tr>
                                <th>Название</th>
                                <th>Цена</th>
                                <th>Статус</th>
                                <th>Адрес</th>
                                <th>Обновлено</th>
                            </tr>
                            <?php foreach ($profile->user->apartments as $apartment): ?>
                                <tr>
                                    <td><?= Html::a($apartment->id . ' - ' . $apartment->name, ['/apartment/update', 'id' => $apartment->id])?></td>
                                    <td><?= Yii::$app->formatter->asCurrency($apartment->price)?></td>
                                    <td><?= $apartment->getStatusLabel()?></td>
                                    <td><?= $apartment->address?></td>
                                    <td><?= Yii::$app->formatter->asDatetime($apartment->updated_at)?></td>
                                </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                    <div class="tab-pane" id="timeline">
                        <div id="alert-block"></div>
                        <div class="text-right margin-bottom-sm">
                            <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('backend', 'Create'),
                                false, ['value' => '/wallet-transaction/create',
                                    'class' => 'showModalButton btn btn-default btn-flat btn-sm',
                                    'title' => Yii::t('backend', 'Create')])?>
                        </div>
                        <?php Pjax::begin(['id' => 'wallet-transaction-list']);?>
                        <?= \yii\grid\GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout' => '<div class="table-responsive">{items}</div><div class="padding-md clearfix"><div class="pull-left">{summary}</div>{pager}</div>',
                            'pager' => ['options' => ['class' => 'pagination pagination-sm no-margin pull-right']],
                            'columns' => [
                                'id',
                                [
                                    'attribute' => 'status',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return $model->statusLabel();
                                    }
                                ],
                                'comment',
                                [
                                    'attribute' => 'amount',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Yii::$app->formatter->asCurrency($model->amount);
                                    }
                                ],
                                [
                                    'attribute' => 'commission',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Yii::$app->formatter->asCurrency($model->commission);
                                    }
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Yii::$app->formatter->asDatetime($model->created_at);
                                    }
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{update} {delete}',
                                    'buttons' => [
                                        'update' => function($url, $model) {
                                            return Html::a(Html::tag('span', '', ['class' => "glyphicon glyphicon-pencil"]), false,
                                                ['value' => \yii\helpers\Url::to(['/wallet-transaction/update', 'id' => $model->id]),
                                                'title' => Yii::t('yii', 'Update'), 'class' => 'showModalButton']);
                                        },
                                        'delete' => function($url, $model) {
                                            return Html::a(Html::tag('span', '', ['class' => "glyphicon glyphicon-trash"]),
                                                ['/wallet-transaction/delete', 'id' => $model->id], ['title' => Yii::t('yii', 'Delete')]);
                                        }
                                    ]
                                ],
                            ],
                        ]); ?>
                        <?php Pjax::end();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
Modal::begin([
    'id' => 'push-balance',
    'header' => '<h4 class="modal-title">' . Yii::t('backend', 'Push balance') . '</h4>',
]);
?>
<div id="modal-alert"></div>

<div class="form-group">
    <label for="input-status"><?= Yii::t('backend', 'Status')?></label>
    <?= Html::dropDownList('status', [WalletTransaction::REFILL], WalletTransaction::$statusLabel,
        ['class' => 'form-control', 'id' => 'input-status']);?>
</div>
<div class="form-group">
    <label for="input-balance"><?= Yii::t('backend', 'Balance')?></label>
    <?= Html::textInput('balance', null, ['class' => 'form-control', 'id' => 'input-balance']);?>
</div>
<div class="form-group">
    <label for="input-comment"><?= Yii::t('backend', 'Comment')?></label>
    <?= Html::textInput('comment', null, ['class' => 'form-control', 'id' => 'input-comment']);?>
</div>

<?= Html::button('Пополнить', ['class' => 'btn btn-primary', 'id' => 'btn-push-balance']);?>

<?php Modal::end();?>

<?php
$url = \yii\helpers\Url::to(['/ajax/push-balance']);

$this->registerJs(<<<JS
$('#btn-push-balance').on('click', function() {
    var balance = $('#input-balance').val();
    if (!balance) {
        $('#modal-alert').html(alertShow('Укажите сумму', 'danger'));
        return false;
    }
    $.ajax({
        method: 'POST',
        url: '{$url}',
        data: {id: {$profile->user_id}, balance: balance, comment: $('#input-comment').val(), status: $('#input-status').val()},
        success: function(data) {
            console.log(data);
            if (data.type == 'success') {
                $('#user-balance').html(data.balance);
            }
            $('#modal-alert').html(alertShow(data.message, data.type));
        },
        error: function() {
            alert('Error!');
        }
    });    
});

JS
);
?>
