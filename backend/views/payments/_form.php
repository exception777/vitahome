<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Payment */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="payment-form box box-primary">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal'
    ]); ?>
    <div class="box-body">
        <?= $form->errorSummary($model, ['class' => 'alert alert-danger'])?>

        <?= $form->field($model, 'booking_id')->textInput() ?>

        <?= $form->field($model, 'pay_date')->widget(\kartik\datetime\DateTimePicker::classname(), [
            'options' => ['placeholder' => 'Выберите дату...'],
            'pluginOptions' => [
                'autoclose' => true,
                'minView' => 2,
                'format' => 'yyyy-mm-dd'
            ]
        ]); ?>

        <?= $form->field($model, 'price')->textInput() ?>

        <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

        <?php if (!$model->isNewRecord):?>
            <div class="text-muted col-sm-offset-3">Добавлено: <?= Yii::$app->formatter->asDatetime($model->created_at)?></div>
        <?php endif;?>

    </div>
    <div class="box-footer">
        <?php if($model->isNewRecord): ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('backend', 'CreateEdit'),
                ['class' => 'btn btn-success btn-flat', 'name' => 'btn_create_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . Yii::t('backend', 'CreateList'),
                ['class' => 'btn btn-success btn-flat', 'name' => 'btn_create_and_list']) ?>
            <?= Html::submitButton('<i class="fa fa-plus-circle"></i> ' . Yii::t('backend', 'CreateCreate'),
                ['class' => 'btn btn-success  btn-flat', 'name' => 'btn_create_and_create']) ?>
        <?php else: ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('backend', 'Save'),
                ['class' => 'btn btn-primary btn-flat', 'name' => 'btn_update_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . Yii::t('backend', 'SaveList'),
                ['class' => 'btn btn-primary btn-flat', 'name' => 'btn_update_and_list']) ?>
        <?php endif; ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
