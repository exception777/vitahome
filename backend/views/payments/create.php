<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Payment */

$this->title = Yii::t('app', 'Cash');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cash'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
