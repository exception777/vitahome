<?php

use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $payments \common\models\Payment[] */
/* @var $dateStart \DateTime */
/* @var $dateEnd \DateTime */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('backend', 'Cash');
$title = $this->title;
if ($dateStart == $dateEnd) {
     $title .= ' за ' . Yii::$app->formatter->asDate($dateStart, 'd MMMM Y г.');
} else {
    $title .= ' c ' . Yii::$app->formatter->asDate($dateStart, 'd MMMM Y') . ' по ' .
        Yii::$app->formatter->asDate($dateEnd, 'd MMMM Y г.');
}
$this->params['breadcrumbs'][] = $this->title;
$dateWeek = new \DateTime();
$dateWeek->modify('-7 days');
$dateMonth = new \DateTime();
$dateMonth->modify('-1 month');
?>

<div class="payment-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <h4><?= $title;?></h4>
        <?php $form = ActiveForm::begin(['id'=> 'form-payment', 'method' => 'get', 'action' => \yii\helpers\Url::to(['payments/index'])]);?>
        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div class="btn-group">
                        <?= Html::a('Сегодня', ['payments/index', 'date_start' => date('Y-m-d'), 'date_end' => date('Y-m-d')], ['class' => 'btn btn-default btn-flat'])?>
                        <?= Html::a('За неделю', ['payments/index', 'date_start' => $dateWeek->format('Y-m-d'), 'date_end' => date('Y-m-d')], ['class' => 'btn btn-default btn-flat'])?>
                        <?= Html::a('За месяц', ['payments/index', 'date_start' => $dateMonth->format('Y-m-d'), 'date_end' => date('Y-m-d')], ['class' => 'btn btn-default btn-flat'])?>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div class="input-group drp-container">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <?php
                        echo \kartik\daterange\DateRangePicker::widget([
                            'name' => 'date_range',
                            'value' => $dateStart->format('Y-m-d') . ' - ' . $dateEnd->format('Y-m-d'),
                            'convertFormat' => true,
                            'useWithAddon' => true,
                            'startAttribute' => 'date_start',
                            'endAttribute' => 'date_end',
                            'pluginOptions' => [
                                'locale' => ['format'=>'Y-m-d']
                            ]
                        ]);
                        ?>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-flat" type="submit"  title="Показать">
                                <i class="fa fa-eye"></i></button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12 text-right">
                <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('backend', 'Create'), ['create'],
                    ['class' => 'btn btn-success btn-flat']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Бронь</th>
                <th>Дата оплаты</th>
                <th>Сумма</th>
                <th>Комментарий</th>
                <th></th>
            </tr>
            <?php
            $total = 0;
            foreach ($payments as $payment):
                $total += $payment->price;
                ?>
                <tr>
                    <td>
                        <div>№ <?= Yii::$app->formatter->asInteger($payment->booking_id)?>,
                            <?= $payment->booking->customer->name ?>
                        </div>
                        <div><?= Yii::$app->formatter->asDatetime($payment->booking->date_in) . ' - ' .
                            Yii::$app->formatter->asDatetime($payment->booking->date_out)?></div>
                    </td>
                    <td><?= Yii::$app->formatter->asDate($payment->pay_date)?></td>
                    <td><?= Yii::$app->formatter->asCurrency($payment->price) ?></td>
                    <td><?= $payment->comment ?></td>
                    <td>
                        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $payment->id],
                            ['title' => Yii::t('yii', 'Update')]) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $payment->id], [
                            'data' => [
                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                    </td>
                </tr>
            <?php endforeach;?>
            <tr>
                <th colspan="2" class="text-right">Итого</th>
                <th><?= Yii::$app->formatter->asCurrency($total);?></th>
                <th></th>
            </tr>
        </table>
    </div>
    <?php Pjax::end(); ?>
</div>
