<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ApartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Квартиры';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <h3 class="box-title"><?=$this->title?></h3>
        <div class="box-tools">
            <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('backend', 'Create'), ['create'], ['class' => 'btn btn-sm btn-default btn-flat']) ?>
        </div>
    </div>
    <div class="box-body no-padding">

        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '<div class="table-responsive">{items}</div><div class="padding-md clearfix"><div class="pull-left">{summary}</div>{pager}</div>',
            'pager' => ['options' => ['class' => 'pagination pagination-sm no-margin pull-right']],
            'columns' => [
                [
                    'attribute' => 'id',
                    'format' => 'raw',
                    'label' => Yii::t('models', 'Name'),
                    'value' => function ($model) {
                        return Html::a($model->id . ' - ' . $model->name, ['apartment/update', 'id' => $model->id]);
                    }
                ],
                [
                    'attribute' => 'price',
                    'value' => function($model){
                        return Yii::$app->formatter->asCurrency($model->price);
                    }
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function($model){
                        return $model->getStatusLabel();
                    }
                ],
                'address',
                [
                    'attribute' => 'city_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->city ? Html::a($model->city->name, ['city/update', 'id' => $model->city_id]) : Yii::t('app', '(not set)');
                    }
                ],
                [
                    'attribute' => 'user_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->user ? Html::a($model->user->username, ['/user/profile/show', 'id' => $model->user_id]) : Yii::t('app', '(not set)');
                    }
                ],
                [
                    'attribute' => 'updated_at',
                    'format' => 'raw',
                    'value' => function($model) {
                        return Yii::$app->formatter->asDatetime($model->updated_at);
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                ],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
