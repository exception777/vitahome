<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\City;
use common\models\Area;
use dektrium\user\models\User;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Apartment */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
$coords = explode(',', $model->maps);
$x = 0;
if(isset($coords[0])){
    if(is_numeric($coords[0])){
        $x = $coords[0];
    }
}
$y = 0;
if(isset($coords[1])) {
    if(is_numeric($coords[1])){
        $y = $coords[1];
    }
}
$this->registerJs('
ymaps.ready(function(){
	map = new ymaps.Map ("map",{center:[' . ($x?:'51.128422') . ',' . ($y?:'71.430564') . '],zoom:14},{});
	map.events.add("click", function (e) {
            var coords = e.get("coords");
            $("#apartment-maps").val(coords[0].toPrecision(8)+","+coords[1].toPrecision(8));
            map.balloon.open(coords, {
                contentHeader: "Установлена метка",
                contentBody:
                    "<p>Координаты на карте: " + [
                        coords[0].toPrecision(8),
                        coords[1].toPrecision(8)
                    ].join(", ") + "</p>"
            });

        });

    var x = ' . $x . ', y = ' . $y . ';
    if(x != 0 && y != 0){
        map.balloon.open([x, y],{
            contentBody: "<p>Координаты на карте: " + [x, y].join(",") + "</p>"
        });
    }
});
$(function() {
    $( "#sortable" ).sortable({
        connectWith: "#sortable",
        opacity: 0.8,
        stop: function(event, ui) {
            var k = 0;
            var pos_photo = [];
            $("ul#sortable li.ui-state-default").each(function(){
                pos_photo[k] = $(this).data("id");
                k++;
            });
            $.ajax({
                type: "POST",
                url: "'.\yii\helpers\Url::toRoute(['apartment/position']).'",
                data: {"id": '.($model->isNewRecord ? 0: $model->id).', "position": pos_photo},
                cache: false
            }).done(function (data) {
                $(\'#photo-success\').html(\'\');

                if(data.code === 100){
                    $(\'#photo-success\').html(\'<div class="alert alert-success">Позиция фотографии изменена.</div>\');
                } else{
                    $(\'#photo-alert\').html(\'<div class="alert alert-danger">Не удалось изменить позицию фотографии.</div>\');
                }
            }).fail(function() {
                alert( "Ошибка!!! Не удалось изменить позицию фотографий." );
            });
        }
    });
    $( "#sortable" ).disableSelection();
  });
');
$this->registerCss('
.photo-delete{display:block;color: red;}
.photo-delete:hover{opacity: .8;color:red;}
#sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
#sortable li { margin: 3px 3px 3px 0; padding: 1px; float: left; text-align: center; }
');

?>
<div class="box box-primary">
<?php $form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data'],
    'id' => 'apartment-form'
]); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-6">

                <?= $form->field($model, 'price')->textInput() ?>
                <?= $form->field($model, 'discount_price')->textInput() ?>
                <?= $form->field($model, 'is_free_now')->checkbox() ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-6">

                <?= $form->field($model, 'city_id')->dropDownList(City::allCities()) ?>

                <?= $form->field($model, 'area_id')->dropDownList(Area::allAreas()) ?>
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'floor')->textInput() ?>

                <?= $form->field($model, 'floor_total')->textInput() ?>

                <?= $form->field($model, 'room')->textInput() ?>

                <?= $form->field($model, 'square')->textInput() ?>

                <?= $form->field($model, 'sleep')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'status')->dropDownList($model->getStatusList()) ?>
                <?= $form->field($model, 'user_id')->dropDownList(\yii\helpers\ArrayHelper::map(User::find()
                    ->asArray()
                    ->orderBy('username')
                    ->all(), 'id', 'username')) ?>

                <?= $form->field($model, 'up_at')->widget(\kartik\datetime\DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите дату...'],
                    'pluginOptions' => [
                        'autoclose' => true,
                    ]
                ]); ?>

                <?= $form->field($model, 'expired_at')->widget(\kartik\datetime\DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите дату...'],
                    'pluginOptions' => [
                        'autoclose' => true,
                    ]
                ]); ?>


                <?php if(!$model->isNewRecord): ?>
                    <div class="callout callout-info">
                        <p><strong>Создано:</strong> <?=$model->created_at?></p>
                        <p><strong>Последнее обновление:</strong> <?=$model->updated_at?></p>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'ob_enable')->checkbox() ?>
                <?= $form->field($model, 'position')->textInput() ?>
                <?= $form->field($model, 'ob_name')->textInput(['maxlength' => true]) ?>
                <?php if ($model->ical_hash):?>
                    <div class="form-group field-apartment-position required">
                        <label class="control-label" for="ical_hash">Адрес в формате iCal</label>
                        <input type="text" id="ical_hash" class="form-control" name="ical_hash"  onfocus="this.select()" readonly="readonly"
                               value="<?= \yii\helpers\Url::to(['/calendar/ical', 'id' => $model->ical_hash], true)?>" autocomplete="offe">
                        <p class="help-block help-block-error "></p>
                    </div>
                <?php endif;?>
            </div>
        </div>

        <div class="padding-v-md">
            <div class="line line-dashed"></div>
        </div>
        <?php
        $statuses = \common\models\Status::dropList();
        DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 4, // the maximum times, an element can be cloned (default 999)
            'min' => 0, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $modelsIcal[0],
            'formId' => 'apartment-form',
            'formFields' => [
                'title',
                'ical_url',
                'status_id',
                'approved',
            ],
        ]); ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-calendar"></i> Импорт календарей
                <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Добавить</button>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body container-items"><!-- widgetContainer -->
                <?php foreach ($modelsIcal as $index => $modelIcal): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <span class="panel-title">Календарь</span>
                            <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            // necessary for update action.
                            if (!$modelIcal->isNewRecord) {
                                echo Html::activeHiddenInput($modelIcal, "[{$index}]id");
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= $form->field($modelIcal, "[{$index}]title")->textInput(['maxlength' => true]) ?>
                                    <?= $form->field($modelIcal, "[{$index}]ical_url")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= $form->field($modelIcal, "[{$index}]status_id")->dropDownList($statuses) ?>
                                    <?= $form->field($modelIcal, "[{$index}]approved")->checkbox() ?>
                                </div>
                            </div><!-- end:row -->

                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php DynamicFormWidget::end(); ?>

        <?= $form->field($model, 'maps')->hiddenInput(['maxlength' => true]) ?>

        <div id="map" style="width: 100%; height: 400px; margin-bottom:10px"></div>

        <?= $form->field($model, 'photos[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

        <div class="row" style="margin-bottom:10px">
            <div class="col-md-12">
                <p><strong>Фотографии:</strong></p>
                <div id="photo-success"></div>
                <?php
                if($model->apartmentPhotos){
                    echo '<ul id="sortable">';
                    foreach($model->apartmentPhotos as $value) {

                        echo '<li class="ui-state-default" data-position="'.$value->position.'" data-id="'.$value->id.'">' .
                            Html::a(Html::img('/images/apartments/small/' . $model->id . '/' . $value->name, ['width' => '200']),
                                '/uploads/apartments/' . $model->id . '/' . $value->name, ['target' => '_blank']) .
                            Html::a('<i class="fa fa-close"></i> удалить', ['photodelete', 'id'=>$value->id], [
                                'class' => 'photo-delete',
                                'title' => "Удалить",
                                'aria-label' => "Удалить",
                                'data-confirm' => "Вы уверены, что хотите удалить этот элемент?"
                            ]) .
                            '</li>';
                    }
                    echo '</ul>';
                }else{
                    echo '<div  class="callout callout-warning"><p>Нет загруженных фотографий</p></div>';
                }
                ?>
            </div>
        </div>
    </div>

    <div class="box-footer">
        <?php if($model->isNewRecord): ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('backend', 'CreateEdit'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . \Yii::t('backend', 'CreateList'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_list']) ?>
            <?= Html::submitButton('<i class="fa fa-plus-circle"></i> ' . \Yii::t('backend', 'CreateCreate'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_create']) ?>
        <?php else: ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('backend', 'Save'),
                ['class' => 'btn btn-primary', 'name' => 'btn_update_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . \Yii::t('backend', 'SaveList'),
                ['class' => 'btn btn-primary', 'name' => 'btn_update_and_list']) ?>
        <?php endif; ?>
    </div>
<?php ActiveForm::end(); ?>

</div>