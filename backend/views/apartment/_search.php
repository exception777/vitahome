<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\ApartmentSearch */
/* @var $form yii\widgets\ActiveForm */

$users = \common\models\User::dropList();

?>
<?php $this->beginContent('@backend/views/layouts/blocks/_search.php',
    ['filterActive' => (bool)Yii::$app->request->get('ApartmentSearch')]); ?>

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'data-pjax' => 1
            ],
        ]); ?>

        <div class="row">
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'id') ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'name') ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'address') ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'user_id')->dropDownList($users, ['prompt' => Yii::t('yii', '(not set)')]) ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'city_id')->dropDownList(\common\models\City::allCities(), ['prompt' => Yii::t('yii', '(not set)')]) ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'status')->dropDownList(\common\models\Apartment::getStatusList(), ['prompt' => Yii::t('yii', '(not set)')]) ?>
            </div>
            <div class="col-md-3 col-sm-4">
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-search"></i> ' . Yii::t('backend', 'Search'), ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('<i class="fa fa-refresh"></i> ' . Yii::t('backend', 'Reset'), ['/apartment/index'], ['class' => 'btn btn-default btn-flat']) ?>
        </div>
        <?php ActiveForm::end(); ?>
<?php $this->endContent() ?>

