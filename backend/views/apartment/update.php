<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Apartment */

$this->title = \Yii::t('backend', 'UpdateObject', $model->name);
$this->params['breadcrumbs'][] = ['label' => 'Квартиры', 'url' => ['index']];
$this->params['breadcrumbs'][] = \Yii::t('backend', 'Update');
?>

    <?= $this->render('_form', [
        'model' => $model,
    'modelsIcal' => $modelsIcal
    ]) ?>
