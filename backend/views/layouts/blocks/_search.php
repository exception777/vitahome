<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $filterActive bool */

$this->registerJs('
var filterActive = ' . ($filterActive ? 'true' : 'false') . ';
$("a[href=\"#filters\"]").on("click", function() {
    $(this).text(function(){
        filterActive = !filterActive;
        return filterActive ? "' . Yii::t('backend', 'Hide filters'). '" : 
            "' . Yii::t('backend', 'Show filters'). '";
    });
});');
?>

<div class="box-body">
    <a role="button" data-toggle="collapse" href="#filters" aria-expanded="false" aria-controls="filters">
        <?= $filterActive ? Yii::t('backend', 'Hide filters') : Yii::t('backend', 'Show filters')?>
    </a>
    <div class="collapse <?= $filterActive ? 'in' : '' ?>" id="filters">

        <?= $content?>

    </div>
</div>