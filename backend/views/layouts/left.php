<aside class="main-sidebar">
    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню навигации', 'options' => ['class' => 'header']],
                    [
                        "label" => 'Бронирование',
                        "icon" => "book",
                        "url" => "#",
                        "items" => [
                            ['label' => 'Шахматка', 'icon' => 'book', 'url' => ['/booking/grid'],
                                'visible' => Yii::$app->user->identity->isRole(['root', 'admin', 'booking'])],
                            ['label' => 'Специальные предложения', 'icon' => 'book', 'url' => ['/booking/special-offer'],
                                'visible' => Yii::$app->user->identity->isRole(['root', 'admin', 'booking'])],
                            ["label" => Yii::t('backend', 'Cash'), "url" => "/payments/index", "icon" => "money",
                                'visible' => Yii::$app->user->identity->isRole(['root', 'admin', 'payments'])],
                            ["label" => Yii::t('backend', 'Task'), "url" => "/site/task", "icon" => "calendar",
                                'visible' => Yii::$app->user->identity->isRole(['root', 'admin', 'booking'])],
                            ["label" => Yii::t('backend', 'Bookings'), "url" => ["/booking/index"], "icon" => "files-o",
                                'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                            ["label" => Yii::t('backend', 'Customers'), "url" => ["/customer/index"], "icon" => "users",
                                'visible' => Yii::$app->user->identity->isRole(['root', 'admin', 'customer'])],
                            [
                                'label' => Yii::t('backend', 'Reports'),
                                'icon' => 'file-excel-o',
                                'url' =>'#',
                                'items' => [
                                    ['label' => Yii::t('backend', 'Salary'), 'url' => ['/report/salary']],
                                    ['label' => Yii::t('backend', 'Incomes'), 'url' => ['/report/incomes']]
                                ],
                                'visible' => Yii::$app->user->identity->isRole(['root', 'admin', 'report'])
                            ],
                            ["label" => Yii::t('backend', 'Statuses'), "url" => ["/status/index"],
                                'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                        ]
                    ],
                    ['label' => 'Квартиры', 'icon' => 'picture-o', 'url' => ['/apartment/index'],
                        'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                    ['label' => 'Слайдер', 'icon' => 'file-text-o', 'url' => ['/slider/index'],
                        'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                    ['label' => 'Страницы', 'icon' => 'building', 'url' => ['/page/index'],
                        'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                    ['label' => 'Города', 'icon' => 'building-o', 'url' => ['/city/index'],
                        'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                    ['label' => 'Районы', 'icon' => 'adn', 'url' => ['/area/index'],
                        'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                    ['label' => 'Реклама', 'icon' => 'adn', 'url' => ['/banner/index'],
                        'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                    ['label' => Yii::t('backend', 'Services'), 'icon' => 'adn', 'url' => ['/service/index'],
                        'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                    ['label' => 'Тех поддержка', 'icon' => 'adn', 'url' => ['/support/index'],
                        'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                    ['label' => 'Пользователи', 'icon' => 'user', 'url' => ['/user/admin/index'],
                        'visible' => Yii::$app->user->identity->isRole(['root', 'admin'])],
                ],
            ]
        ) ?>

    </section>

</aside>
