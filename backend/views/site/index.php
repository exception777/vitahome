<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'VitaHome';
$this->params['breadcrumbs'] = []
?>
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-teal">
            <div class="inner">
                <h3>Бронирование</h3>
                <p><?php echo Html::a('Шахматка', ['booking/grid'], ['style' => ['color'=> '#fff']]);?></p>
            </div>
            <div class="icon">
                <i class="fa fa-book"></i>
            </div>
            <?php echo Html::a('Перейти к списку <i class="fa fa-arrow-circle-right"></i>', ['booking/index'],
                ['class' => 'small-box-footer']);?>
        </div>
    </div>
    <?php if(Yii::$app->user->can('admin')):?>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-teal">
            <div class="inner">
                <h3>Кэш сайта</h3>
                <p><?=Html::a('Очистить кэш', ['site/cache-flush'], ['style' => ['color'=> '#fff']]);?></p>
            </div>
            <div class="icon">
                <i class="fa fa-archive"></i>
            </div>
            <?php echo Html::a('<i class="fa fa-arrow-circle-right"></i>', ['#'],
                ['class' => 'small-box-footer']);?>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>Квартиры</h3>
                <p><?php echo Html::a('Добавить новую', ['apartment/create'], ['style' => ['color'=> '#fff']]);?></p>
            </div>
            <div class="icon">
                <i class="fa fa-bed"></i>
            </div>
            <?php echo Html::a('Перейти к списку <i class="fa fa-arrow-circle-right"></i>', ['apartment/index'],
                ['class' => 'small-box-footer']);?>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>Слайдер</h3>
                <p><?php echo Html::a('Добавить новый', ['slider/create'], ['style' => ['color'=> '#fff']]);?></p>
            </div>
            <div class="icon">
                <i class="fa fa-picture-o"></i>
            </div>
            <?php echo Html::a('Перейти к списку <i class="fa fa-arrow-circle-right"></i>', ['slider/index'],
                ['class' => 'small-box-footer']);?>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-fuchsia">
            <div class="inner">
                <h3>Страницы</h3>
                <p><?php echo Html::a('Добавить новую', ['page/create'], ['style' => ['color'=> '#fff']]);?></p>
            </div>
            <div class="icon">
                <i class="fa fa-file-text-o"></i>
            </div>
            <?php echo Html::a('Перейти к списку <i class="fa fa-arrow-circle-right"></i>', ['page/index'],
                ['class' => 'small-box-footer']);?>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>Города</h3>
                <p><?php echo Html::a('Добавить новый', ['city/create'], ['style' => ['color'=> '#fff']]);?></p>
            </div>
            <div class="icon">
                <i class="fa fa-building"></i>
            </div>
            <?php echo Html::a('Перейти к списку <i class="fa fa-arrow-circle-right"></i>', ['city/index'],
                ['class' => 'small-box-footer']);?>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>Районы</h3>
                <p><?php echo Html::a('Добавить новый', ['area/create'], ['style' => ['color'=> '#fff']]);?></p>
            </div>
            <div class="icon">
                <i class="fa fa-building-o"></i>
            </div>
            <?php echo Html::a('Перейти к списку <i class="fa fa-arrow-circle-right"></i>', ['area/index'],
                ['class' => 'small-box-footer']);?>
        </div>
    </div>
    <?php endif;?>
</div>
