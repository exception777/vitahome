<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $tasks array */
/* @var $dateStart \DateTime */
/* @var $dateEnd \DateTime */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Task');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="task-index box box-primary">
    <div class="box-header with-border">
        <?php $form = ActiveForm::begin(['id'=> 'form-task', 'method' => 'get', 'action' => \yii\helpers\Url::to(['site/task'])]);?>
        <div class="row">
            <div class="col-lg-5 col-md-7 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div class="input-group drp-container">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <?php
                        echo \kartik\daterange\DateRangePicker::widget([
                            'name' => 'date_range',
                            'value' => $dateStart->format('Y-m-d H:i') . ' - ' . $dateEnd->format('Y-m-d H:i'),
                            'convertFormat' => true,
                            'useWithAddon' => true,
                            'startAttribute' => 'date_start',
                            'endAttribute' => 'date_end',
                            'pluginOptions' => [
                                'timePicker'=>true,
                                'timePickerIncrement'=>30,
                                'timePicker24Hour' => true,
                                'locale' => ['format'=>'Y-m-d H:i']
                            ]
                        ]);
                        ?>
                        <span class="input-group-btn">
                             <button class="btn btn-primary btn-flat" type="submit" title="Показать"><i class="fa fa-eye"></i></button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-5 col-sm-12 col-xs-12 text-right">
                <div class="form-group">
                    <div class="btn-group">
                        <?= Html::a('<i class="fa fa-tasks"></i> Сегодня', ['site/task', 'date_start' => date('Y-m-d 08:00'),
                            'date_end' => date('Y-m-d 22:00')], ['class' => 'btn btn-default btn-flat'])?>
                        <?php
                        echo \yii2assets\printthis\PrintThis::widget([
                            'htmlOptions' => [
                                'id' => 'print-block',
                                'btnClass' => 'btn btn-info btn-flat',
                                'btnId' => 'btnPrintThis',
                                'btnText' => 'Печать',
                                'btnIcon' => 'fa fa-print'
                            ],
                            'options' => [
                                'debug' => false,
                                'importCSS' => true,
                                'importStyle' => false,
        //                        'loadCSS' => "path/to/my.css",
                                'pageTitle' => "",
                                'removeInline' => false,
                                'printDelay' => 333,
                                'header' => null,
                                'formValues' => true,
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div id="print-block" class="box-body table-responsive no-padding">
        <table class="table table-bordered table-striped table-tash">
            <tr>
                <th>Время</th>
                <th>Заезд</th>
                <th>Выезд</th>
                <th>Квартира</th>
                <th>Клиент</th>
                <th>Сумма</th>
                <th>Комментарий</th>
                <th class="no-print"></th>
            </tr>
            <?php foreach ($tasks as $task): ?>
                <tr>
                    <td><?= $task['time']?></td>
                    <td><?= $task['check_in'] ? '<i class="fa fa-check text-success"></i>': ''?></td>
                    <td><?= $task['check_in'] ? '' : '<i class="fa fa-remove text-danger"></i>'?></td>
                    <td><?= $task['apartment']?></td>
                    <td><?= $task['customer'] ?></td>
                    <td><?= $task['total'] ?></td>
                    <td>
                        <div class="task-information"><?= $task['information']?></div>
                    </td>
                    <td class="no-print">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm btn-flat edit"><i class="fa fa-edit"></i></button>
                            <button type="button" class="btn btn-danger btn-sm btn-flat trash"><i class="fa fa-trash"></i></button>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
        </table>
    </div>
</div>

<?php
$this->registerJs(<<<JS
$('.trash').on('click', function() {
   $(this).parents('tr').remove(); 
});
$('.edit').on('click', function() {
    let task = $(this).parents('tr').find('.task-information');
    let text = task.html();
    task.html('<textarea class="form-control" rows=3>' + text + '</textarea>');
    task.find('textarea').focus();
});
$('.task-information').on('focusout', 'textarea', function() {
    let text = $(this).val();
    $(this).parent().html(text);
});
JS
);

?>