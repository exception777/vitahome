<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .content-wrapper{
            margin:0;
        }
        .error-page{
            margin-top:5%
        }
    </style>
</head>
<body class="skin-blue sidebar-mini">
<?php $this->beginBody() ?>
    <div class="wrapper">
        <div class="content-wrapper">
            <section class="content">
                <div class="error-page">
                    <h2 class="headline text-yellow"> <?=$exception->statusCode?></h2>
                    <div class="error-content">
                        <h3><i class="fa fa-warning text-yellow"></i> <?= Html::encode($this->title) ?></h3>
                        <p>
                            <?= nl2br(Html::encode($message)) ?><br>
                            Meanwhile, you may <a href="../../index.html">return to dashboard</a> or try using the search form.
                        </p>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

