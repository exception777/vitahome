<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Wallet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wallet-form">

    <?php $form = ActiveForm::begin(['id' => 'wallet-transaction-form']); ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'commission')->textInput() ?>

    <?= $form->field($model, 'comment')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$this->registerJs(<<<JS
var form =jQuery('#wallet-transaction-form');
form.on('beforeSubmit', function(e) {
    e.preventDefault();
    jQuery.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: new FormData(form[0]),
        mimeType: 'multipart/form-data',
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'json',
        success: function (data) {
            $('#alert-block').html(alertShow(data.message, data.type));
            $('#modal').modal('hide');
            $.pjax.reload('#wallet-transaction-list');
        }
    });
     return false;
});
JS
);
?>