<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Wallet */

$this->title = Yii::t('backend', 'Create Wallet');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Wallets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wallet-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
