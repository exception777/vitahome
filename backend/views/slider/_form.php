<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

        <p><strong>Фотографии:</strong></p>
        <?php
        if($model->photo){
            echo '<p>' . Html::img('/images/slider/' . $model->photo, ['width' => '200']) . '</p>';
        }
        ?>
        <?= Html::fileInput('uploadPhoto', null, ['id' => 'uploadPhoto']) ?>

        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusList'])?>

    </div>

    <div class="box-footer">
        <?php if($model->isNewRecord): ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('backend', 'CreateEdit'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . \Yii::t('backend', 'CreateList'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_list']) ?>
            <?= Html::submitButton('<i class="fa fa-plus-circle"></i> ' . \Yii::t('backend', 'CreateCreate'),
                ['class' => 'btn btn-success', 'name' => 'btn_create_and_create']) ?>
        <?php else: ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . \Yii::t('backend', 'Save'),
                ['class' => 'btn btn-primary', 'name' => 'btn_update_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . \Yii::t('backend', 'SaveList'),
                ['class' => 'btn btn-primary', 'name' => 'btn_update_and_list']) ?>
        <?php endif; ?>
    </div>

<?php ActiveForm::end(); ?>
