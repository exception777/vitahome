<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдер';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <div class="box-tools">
            <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'photo',
                'format' => 'raw',
                'label' => 'Изображение',
                'value' => function($model){
                    return Html::img('/images/slider/' . $model->photo, ['width' => '100px']);
                }
            ],
            'title',
            'url:url',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model){
                    return '<span class="label ' . ($model->status ? 'label-success' : 'label-danger') . '">' .
                    Yii::$app->params['statusList'][$model->status] . '</span>';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>

    </div>
</div>
