<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\CustomerSearch */
/* @var $form yii\widgets\ActiveForm */

$users = \common\models\User::dropList();

?>
<?php $this->beginContent('@backend/views/layouts/blocks/_search.php',
    ['filterActive' => (bool)Yii::$app->request->get('CustomerSearch')]); ?>
        <?php $form = ActiveForm::begin([
            'action' => [Yii::$app->controller->action->id],
            'method' => 'get',
            'options' => [
                'data-pjax' => 1
            ],
        ]); ?>

        <div class="row">
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'id') ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'name') ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'phone') ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <?= $form->field($model, 'user_id')->dropDownList($users, ['prompt' => Yii::t('yii', '(not set)')]) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-search"></i> ' . Yii::t('backend', 'Search'), ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('<i class="fa fa-refresh"></i> ' . Yii::t('backend', 'Reset'), ['/customer/' . Yii::$app->controller->action->id], ['class' => 'btn btn-default btn-flat']) ?>
        </div>
        <?php ActiveForm::end(); ?>
<?php $this->endContent() ?>
