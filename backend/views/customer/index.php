<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customers');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="customer-index box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?=$this->title?></h3>
        <div class="box-tools">
            <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('backend', 'Create'), ['create'], ['class' => 'btn btn-sm btn-default btn-flat']) ?>
        </div>
    </div>
    <div class="box-body no-padding">
        <?= $this->render('_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]) ?>
    </div>
</div>

