<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $bookings common\models\Booking[] */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Customers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-4">
        <div class="box box-widget widget-user-2">
            <div class="widget-user-header bg-yellow"  style="background: url('/images/bg_city.png') center center;">
                <div class="widget-user-image">
                    <img class="img-circle" src="/images/user.png" alt="User Avatar">
                </div>
                <h3 class="widget-user-username"><?= $model->name?></h3>
                <h5 class="widget-user-inn"><?= $model->iin?> &nbsp;</h5>
            </div>
            <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                    <li><a>Забронированно <span class="pull-right badge bg-blue"><?= count($bookings)?></span></a></li>
                </ul>
            </div>
            <div class="box-body box-profile">
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b><?= $model->getAttributeLabel('phone')?></b> <a href="tel:<?= $model->phone?>" class="pull-right"><?= $model->phone?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= $model->getAttributeLabel('email')?></b> <a href="mail:<?= $model->email?>" class="pull-right"><?= $model->email?></a>
                    </li>
                </ul>
                <?= Html::a('<i class="fa fa-edit"></i> ' . Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat btn-block']) ?>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">О клиенте</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> <?= $model->getAttributeLabel('document')?></strong>
                <p class="text-muted">
                    <?= isset(Yii::$app->params['documentTypes'][$model->document]) ? Yii::$app->params['documentTypes'][$model->document] : ''?><br>
                    <strong><?= $model->getAttributeLabel('document_number')?></strong> <?= $model->document_number?><br>
                    <strong><?= $model->getAttributeLabel('document_date')?></strong> <?= Yii::$app->formatter->asDate($model->document_date)?>
                </p>
                <hr>
                <strong><i class="fa fa-file-text-o margin-r-5"></i> <?= $model->getAttributeLabel('information')?></strong>
                <p class="text-muted"><?= $model->information?></p>
                <hr>
                <strong><i class="fa fa-user margin-r-5"></i> <?= $model->getAttributeLabel('user_id')?></strong>
                <p class="text-muted">
                    <?php if ($model->user):?>
                    <strong><?= $model->getAttributeLabel('user_id')?></strong> <?= $model->user->username?><br>
                    <?php endif;?>
                    <strong><?= $model->getAttributeLabel('created_at')?></strong> <?= Yii::$app->formatter->asDatetime($model->created_at)?>
                </p>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#bookings" data-toggle="tab">Проживание</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="bookings">
                    <ul class="timeline timeline-inverse">
                        <?php foreach ($bookings as $booking):?>
                            <li class="time-label">
                                <span style="background-color: <?= $booking->approved ? $booking->status->color: '#dd4b39'?>; color: white">
                                    <?= Yii::$app->formatter->asDate($booking->date_in)?></span>
                            </li>
                            <li>
                                <i class="fa fa-info bg-teal"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> забронированно <?= Yii::$app->formatter->asDate($booking->created_at)?></span>
                                    <h3 class="timeline-header">
                                        <?= Html::a('№ ' . Yii::$app->formatter->asInteger($booking->id),
                                            ['booking/view', 'id' => $booking->id])?>
                                        <?= $booking->approved ? $booking->status->name: 'Бронь снята'?>
                                    </h3>
                                    <div class="timeline-body">
                                        <div><?= $booking->getAttributeLabel('object_id') . ': ' . $booking->object->ob_name?></div>
                                        <div><?= Yii::$app->formatter->asDatetime($booking->date_in) .
                                            ' - ' . Yii::$app->formatter->asDatetime($booking->date_out)?></div>
                                        <div><?= $booking->getAttributeLabel('total') . ': ' . Yii::$app->formatter->asCurrency($booking->total)?></div>
                                        <div>Забронировал: <?= $booking->user ? $booking->user->username : Yii::t('yii', '(not set)')?></div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach;?>
                        <li>
                            <i class="fa fa-clock-o bg-gray"></i>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
