<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


Pjax::begin(['enablePushState' => !Yii::$app->request->isAjax]);
?>

<?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '<div class="table-responsive">{items}</div><div class="padding-md clearfix"><div class="pull-left">{summary}</div>{pager}</div>',
        'pager' => ['options' => ['class' => 'pagination pagination-sm no-margin pull-right']],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model) {
                    $contact = '<div class="text-muted"><a href="tel:'.$model->phone.'"><i class="fa fa-phone"></i> '.$model->phone.'</a></div>';
                    return '<div>' . $model->name . '</div>' . $contact;
                },
                'contentOptions' => ['class' => 'customer-name']
            ],
            [
                'attribute' => 'information',
                'format' => 'raw',
                'value' => function($model) {
                    $info = $model->information ? '<div><strong>' . Yii::t('app', 'Information') .
                        ':</strong> ' . $model->information . '</div>': '';
                    return $info;
                }
            ],
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return $model->user ? $model->user->username : Yii::t('yii', '(not set)');
                }
            ],
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'select' => function ($url, $model, $key) {
                        return \yii\helpers\Html::button('<i class="fa fa-check"></i>', ['data-id' => $model->id,
                            'class' => 'btn btn-info customer-select']);
                    },
                ],
                'template' => Yii::$app->controller->action->id == 'list' ? '{select}' : '{view} {update}'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>

