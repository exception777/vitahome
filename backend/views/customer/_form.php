<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */

$users = \yii\helpers\ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username');
?>

<div class="customer-form box box-primary">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal'
    ]); ?>
    <div class="box-body">
        <?= $form->errorSummary($model, ['class' => 'alert alert-danger'])?>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'information')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'iin')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'document')->dropDownList(Yii::$app->params['documentTypes']) ?>
                <?= $form->field($model, 'document_number')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'document_date')->widget(\kartik\datetime\DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите дату...'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'minView' => 2,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]); ?>

                <?= $form->field($model, 'user_id')->dropDownList($users, ['prompt' => 'Выберите из списка...']) ?>

                <?php if (!$model->isNewRecord):?>
                    <div class="text-muted col-sm-offset-3">Добавлено: <?= Yii::$app->formatter->asDatetime($model->created_at)?></div>
                <?php endif;?>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?php if($model->isNewRecord): ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app', 'CreateEdit'),
                ['class' => 'btn btn-success btn-flat', 'name' => 'btn_create_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . Yii::t('app', 'CreateList'),
                ['class' => 'btn btn-success btn-flat', 'name' => 'btn_create_and_list']) ?>
            <?= Html::submitButton('<i class="fa fa-plus-circle"></i> ' . Yii::t('app', 'CreateCreate'),
                ['class' => 'btn btn-success  btn-flat', 'name' => 'btn_create_and_create']) ?>
        <?php else: ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app', 'Save'),
                ['class' => 'btn btn-primary btn-flat', 'name' => 'btn_update_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . Yii::t('app', 'SaveList'),
                ['class' => 'btn btn-primary btn-flat', 'name' => 'btn_update_and_list']) ?>
        <?php endif; ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
