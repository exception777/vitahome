<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\Support;
use \common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SupportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Support');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <div class="box-tools">
            <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function($model){
                        $label = $model->getStatusLabel();
                        if(count($model->supportMessages) > 0){
                            $label .= ' <span title="'.Yii::t('backend', 'New Message').'" class="badge bg-yellow">'.count($model->supportMessages).'</span>';
                        }
                        return $label;
                    },
                    'filter' => Support::getStatusList()
                ],
                [
                    'attribute' => 'user_id',
                    'value' => 'user.username',
                    'filter' => User::dropList()
                ],
                'created_at',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {delete}'
                ],
            ],
        ]); ?>

    </div>
</div>
