<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Support */

$this->title = Yii::t('backend', 'Support');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Supports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">

    <div class="box-header">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>