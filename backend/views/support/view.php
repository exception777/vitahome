<?php

use yii\helpers\Html;
use \yii\bootstrap\ActiveForm;
use \common\models\SupportMessage;

/* @var $this yii\web\View */
/* @var $support common\models\Support */
/* @var $message common\models\SupportMessage */

$this->title = $support->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Support'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header">
        <div class="box-heading"><?=$this->title?></div>
    </div>
    <div class="box-body">
        <?php if($support->supportMessages):?>
            <?php foreach ($support->supportMessages as $supportMessage):?>
                <div class="support-message <?=$supportMessage->user_id == Yii::$app->user->id?'message-right':
                    ($supportMessage->status == SupportMessage::STATUS_NEW?'message-new':'')?>">
                    <div class="message"><?=$supportMessage->message?></div>
                    <div class="message-date"><?=Yii::$app->formatter->asDatetime($supportMessage->created_at, 'dd MMM YYYY, HH:mm')?></div>
                </div>
            <?php endforeach;?>
        <?php else:?>
            <p class="text-center text-muted"><?=Yii::t('frontend', 'Not record')?></p>
        <?php endif;?>

        <?php $form = ActiveForm::begin([
            'id' => 'apartment-create',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-3',
                    'offset' => 'col-sm-offset-3',
                    'wrapper' => 'col-sm-9',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]); ?>

        <?=$form->errorSummary($message)?>
        <?=$form->field($message, 'message')->textarea(['rows' => 6]) ?>
    </div>

    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-paper-plane"></i> ' . \Yii::t('frontend', 'Send'),
            ['class' => 'col-sm-offset-3 btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
