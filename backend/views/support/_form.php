<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Support */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <?=$form->errorSummary($model);?>

        <?= $form->field($model, 'user_id')->dropDownList(\common\models\User::getUserList()) ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <div class="form-group field-support-title required">
            <?=Html::label(Yii::t('backend', 'Message'), 'form-message', ['class' => 'control-label'])?>
            <?= Html::textarea('message', null, ['class' => 'form-control', 'id' => 'form-message', 'required' => true])?>
        </div>

    </div>

    <div class="box-footer">
        <?= Html::submitButton(Yii::t('backend', 'Create'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

