<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Status */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form box box-primary">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal'
    ]); ?>
    <div class="box-body">
        <?= $form->errorSummary($model, ['class' => 'alert alert-danger'])?>


        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'color')->widget(\kartik\color\ColorInput::classname(), [
            'options' => ['placeholder' => Yii::t('backend', 'Select color ...')],
        ]) ?>

        <?= $form->field($model, 'paid')->checkbox() ?>

        <?= $form->field($model, 'approved')->checkbox() ?>

        <?php if (!$model->isNewRecord):?>
            <div class="text-muted col-sm-offset-3">Добавлено: <?= Yii::$app->formatter->asDatetime($model->created_at)?></div>
        <?php endif;?>

    </div>
    <div class="box-footer">
        <?php if($model->isNewRecord): ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app', 'CreateEdit'),
                ['class' => 'btn btn-success btn-flat', 'name' => 'btn_create_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . Yii::t('app', 'CreateList'),
                ['class' => 'btn btn-success btn-flat', 'name' => 'btn_create_and_list']) ?>
            <?= Html::submitButton('<i class="fa fa-plus-circle"></i> ' . Yii::t('app', 'CreateCreate'),
                ['class' => 'btn btn-success  btn-flat', 'name' => 'btn_create_and_create']) ?>
        <?php else: ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app', 'Save'),
                ['class' => 'btn btn-primary btn-flat', 'name' => 'btn_update_and_edit']) ?>
            <?= Html::submitButton('<i class="fa fa-save"></i> <i class="fa fa-list"></i> ' . Yii::t('app', 'SaveList'),
                ['class' => 'btn btn-primary btn-flat', 'name' => 'btn_update_and_list']) ?>
        <?php endif; ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>