<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\StatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Statuses');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?=$this->title?></h3>
        <div class="box-tools">
            <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('backend', 'Create'), ['create'], ['class' => 'btn btn-sm btn-default btn-flat']) ?>
        </div>
    </div>
    <div class="box-body no-padding">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '<div class="table-responsive">{items}</div><div class="padding-md clearfix"><div class="pull-left">{summary}</div>{pager}</div>',
            'pager' => ['options' => ['class' => 'pagination pagination-sm no-margin pull-right']],
            'columns' => [['class' => 'yii\grid\SerialColumn'],
                'name',
                [
                    'attribute' => 'color',
                    'format' => 'raw',
                    'value' => function($model) {
                        return '<span style="background-color: ' . $model->color . '" class="color-preview"></span> ' . $model->color;
                    }
                ],
                [
                    'attribute' =>'paid',
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->paid ? '<i class="fa fa-check text-success"></i> ' : '<i class="fa fa-times text-danger"></i> ';
                    }
                ],
                [
                    'attribute' =>'approved',
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->approved ? '<i class="fa fa-check text-success"></i> ' : '<i class="fa fa-times text-danger"></i> ';
                    }
                ],
                'created_at:datetime',
                ['class' => 'yii\grid\ActionColumn','template' => '{update}'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
