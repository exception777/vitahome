<?php

namespace backend\controllers;

use backend\models\CustomerSearch;
use backend\models\BookingSearch;
use common\models\Apartment;
use common\models\Customer;
use common\models\Booking;
use common\models\Status;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;


class BookingController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $rules = [];
        if (Yii::$app->user->identity) {
            if (Yii::$app->user->identity->isRole(['root', 'admin'])) {
                $rules[] = [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['@'],
                ];
            } elseif (Yii::$app->user->identity->isRole(['booking'])) {
                $rules[] = [
                    'actions' => ['grid'],
                    'allow' => true,
                    'roles' => ['@'],
                ];
            }
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Booking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGrid()
    {
        $apartments = Apartment::find()->where(['ob_enable' => 1])->orderBy(['ob_name' => SORT_ASC])->all();
        $statuses = Status::find()->where(['approved' => Status::ENABLE])->all();

        return $this->render('grid', [
            'apartments' => $apartments,
            'statuses' => $statuses
        ]);
    }

    public function actionSpecialOffer()
    {
        $apartments = Apartment::find()->where(['ob_enable' => 1])->orWhere(['user_id' => 3])->orderBy(['ob_name' => SORT_ASC])->all();
        if (Yii::$app->request->isPost && Yii::$app->request->post()) {
            $data = Yii::$app->request->post('Apartment');

            foreach ($apartments as $apartment) {
                $apartmentData = $data[$apartment->id] ?? [];
                $apartment->price = $apartmentData['price'] ?? $apartment->price;
                $apartment->discount_price = $apartmentData['discount_price'] ?? $apartment->discount_price;
                $apartment->is_free_now = $apartmentData['is_free_now'] ?? $apartment->is_free_now;
                $apartment->position = $apartmentData['position'] ?? $apartment->position;
                $apartment->up_at = $apartmentData['up_at'] ?? $apartment->up_at;
                $apartment->save();
            }

            return $this->redirect(['booking/special-offer']);
        }

        return $this->render('special-offer', [
            'apartments' => $apartments
        ]);
    }

    /**
     * Displays a single Booking model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Booking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Booking();

        $post = Yii::$app->request->post();

        if($model->load($post)) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Success create'));
                if (isset($post['btn_create_and_create'])) {
                    return $this->redirect(['create']);
                }
                if (isset($post['btn_create_and_list'])) {
                    return $this->redirect(['index']);
                }
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Error create'));
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Booking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if($model->load($post)){
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Success save'));
                if (isset($post['btn_update_and_list'])) {
                    return $this->redirect(['index']);
                }
                return $this->redirect(['update', 'id' => $model->id]);
            } else{
                Yii::$app->session->setFlash('error', Yii::t('app', 'Error save'));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Booking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
