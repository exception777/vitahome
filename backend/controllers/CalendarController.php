<?php
namespace backend\controllers;

use common\models\Apartment;
use common\models\Booking;
use common\models\Ical;
use backend\services\CalendarService;
use Sabre\VObject\Component\VCalendar;
use Sabre\VObject\Reader;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CalendarController extends Controller
{
    public function actionIcal()
    {
        $apartment = Apartment::find()->where(['ical_hash' => Yii::$app->request->get('id')])->one();

        if (!$apartment) {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }

        $bookings = Booking::find()->where(['object_id' => $apartment->id, 'approved' => Booking::ENABLE])
            ->andWhere(['>=', 'date_out', date('Y-m-d H:i:s')])->all();

        $vCalendar = new VCalendar();
        $vCalendar->PRODID = '-//MyBookingBase.Ru//MyBookingBase Calendar 0.1//EN';

        if ($bookings) {
            foreach ($bookings as $booking) {
                if ($booking->ical_hash) continue;
                $dtStart = new \DateTime($booking->date_in);
                $dtEnd = new \DateTime($booking->date_out);
                $vCalendar->add('VEVENT', [
                   'UID' => Yii::$app->getSecurity()->generateRandomString(),
                   'SUMMARY' => $booking->id,
                   'DTSTART' => $dtStart,
                   'DTEND' => $dtStart,
                ]);
            }
        }

        header('Content-Type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename="ical.ics"');

        echo $vCalendar->serialize();
    }

    public function actionIcalImport()
    {
        $calendarService = new CalendarService();
        $calendarService->import();

        Yii::$app->session->setFlash('success', Yii::t('backend', 'Calendar sync success'));

        return $this->redirect(Yii::$app->request->referrer);
    }
}