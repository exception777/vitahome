<?php

namespace backend\controllers;

use common\models\SupportMessage;
use Yii;
use common\models\Support;
use backend\models\SupportSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SupportController implements the CRUD actions for Support model.
 */
class SupportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Support models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SupportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Support model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $support = $this->findModel($id);

        foreach ($support->supportMessages as $supportMessage) {
            if($supportMessage->user_id == $support->user_id && $supportMessage->status == SupportMessage::STATUS_NEW){
                Yii::$app->db->createCommand('UPDATE support_message SET status = :status WHERE support_id = :id AND user_id = :userId',
                    [':status' => SupportMessage::STATUS_DEFAULT, 'id' => $support->id, ':userId' => $support->user_id])->execute();
                break;
            }
        }

        $message = new SupportMessage();
        $post = Yii::$app->request->post();
        if($message->load($post)) {
            $message->support_id = $support->id;
            $message->user_id = Yii::$app->user->id;
            $message->status = SupportMessage::STATUS_NEW;
            if ($message->save()) {
                Yii::$app->session->setFlash('success', Yii::t('backend', 'Success save'));
                return $this->redirect(['view', 'id' => $support->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('frontend', 'Error save'));
            }
        }

        return $this->render('view', [
            'support' => $support,
            'message' => $message
        ]);
    }

    /**
     * Creates a new Support model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Support();

        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $model->status = Support::STATUS_OPEN;
            if ($model->save()) {
                if($post['message']){
                    $message = new SupportMessage();
                    $message->message = $post['message'];
                    $message->user_id = Yii::$app->user->id;
                    $message->support_id = $model->id;
                    $message->status = SupportMessage::STATUS_NEW;
                    $message->save();
                }
                Yii::$app->session->setFlash('success', Yii::t('backend', 'Success save'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('frontend', 'Error save'));
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Support model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Support model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Support the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Support::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
