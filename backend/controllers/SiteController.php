<?php
namespace backend\controllers;

use common\models\Booking;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $rules = [];
        if (Yii::$app->user->identity) {
            if (Yii::$app->user->identity->isRole(['root', 'admin', 'booking'])) {
                $rules[] = [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['@'],
                ];
            }
        }
        $rules[] = [
            'actions' => ['error'],
            'allow' => true,
        ];
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'backend\models\ErrorModel',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCacheFlush()
    {
        Yii::$app->cache->flush();
        Yii::$app->frontendCache->flush();
        Yii::$app->session->setFlash('success', 'Кэш очищен.');
        $this->redirect('index');
    }

    public function actionTask()
    {
        $dateStart = new \DateTime(Yii::$app->request->get('date_start', date('Y-m-d 08:00:00')));
        $dateEnd = new \DateTime(Yii::$app->request->get('date_end', date('Y-m-d 22:00:00')));

        $bookings = Booking::find()->with(['object', 'payments', 'customer'])->where('(date_in >= :dateStart AND date_in <= :dateEnd) OR
            (date_out >= :dateStart AND date_out <= :dateEnd)',
            ['dateStart' => $dateStart->format('Y-m-d H:i:s'), 'dateEnd' => $dateEnd->format('Y-m-d H:i:s')])
            ->andWhere(['approved' => Booking::ENABLE])
            ->all();

        $fmt = new \IntlDateFormatter(
            'ru_RU',
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            Yii::$app->getTimeZone(),
            \IntlDateFormatter::GREGORIAN,
            'HH:mm, dd MMMM'
        );
        $tasks = [];

        foreach ($bookings as $booking) {
            $payments = 0;
            if ($booking->payments) {
                foreach ($booking->payments as $payment) {
                    $payments += $payment->price;
                }
            }

            $task = [
                'id' => 0,
                'time' => '',
                'check_in' => true,
                'apartment' => '<div>' . $booking->object->ob_name . '</div><div>' . $booking->object->address . '</div>',
                'customer' => '<div>' . $booking->customer->name . '</div><div>' . $booking->customer->phone . '</div>',
                'total' => '<div>Оплачено: ' . Yii::$app->formatter->asCurrency($payments) . '</div><div>' .
                    $booking->getAttributeLabel('booking_total') . ': ' .
                    Yii::$app->formatter->asCurrency($booking->booking_total) . '</div>',
                'information' => $booking->information,
            ];

            $dateIn = new \DateTime($booking->date_in);
            $dateOut = new \DateTime($booking->date_out);
            if ($dateIn > $dateStart && $dateIn < $dateEnd) {
                $task['id'] = $dateIn->getTimestamp();
                $task['time'] =  $fmt->format($dateIn);
                $tasks[] = $task;
            }
            if ($dateOut > $dateStart && $dateOut < $dateEnd) {
                $task['id'] = $dateOut->getTimestamp();
                $task['time'] =  $fmt->format($dateOut);
                $task['check_in'] = false;
                $tasks[] = $task;
            }
        }

        if ($tasks) {
            usort($tasks, function ($a, $b) {
                if ($a['id'] === $b['id']) return 0;
                return $a['id'] > $b['id'] ? 1 : -1;
            });
        }

        return $this->render('task', [
            'tasks' => $tasks,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd
        ]);
    }

}
