<?php

namespace backend\controllers;

use common\models\Booking;
use Yii;
use common\models\Customer;
use backend\models\CustomerSearch;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $rules = [];
        if (Yii::$app->user->identity) {
            if (Yii::$app->user->identity->isRole(['root', 'admin', 'customer'])) {
                $rules[] = [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['@'],
                ];
            }
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider

        ]);
    }

    public function actionList()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);

    }

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $bookings = Booking::find()->with()->where(['customer_id' => $model->id])->orderBy(['date_in' => SORT_DESC])->all();

        return $this->render('view', [
            'model' => $model,
            'bookings' => $bookings
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Customer();

        $post = Yii::$app->request->post();

        if($model->load($post)) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Success create'));
                if (isset($post['btn_create_and_create'])) {
                    return $this->redirect(['create']);
                }
                if (isset($post['btn_create_and_list'])) {
                    return $this->redirect(['index']);
                }
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Error create'));
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if($model->load($post)){
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Success save'));
                if (isset($post['btn_update_and_list'])) {
                    return $this->redirect(['index']);
                }
                return $this->redirect(['update', 'id' => $model->id]);
            } else{
                Yii::$app->session->setFlash('error', Yii::t('app', 'Error save'));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
