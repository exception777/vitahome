<?php
namespace backend\controllers\user;

use Yii;
use backend\models\WalletTransactionSearch;
use dektrium\user\Finder;
use yii\web\NotFoundHttpException;

class ProfileController extends \dektrium\user\controllers\ProfileController
{
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        parent::__construct($id, $module, $finder, $config);
    }

    /**
     * Shows user's profile.
     *
     * @param int $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionShow($id)
    {
        $profile = $this->finder->findProfileById($id);

        if ($profile === null) {
            throw new NotFoundHttpException();
        }

        $searchModel = new WalletTransactionSearch();
        $searchModel->wallet_id = $profile->user->wallet->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('show', [
            'profile' => $profile,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
