<?php

namespace backend\controllers;

use backend\base\Model;
use common\models\ApartmentPhoto;
use common\models\Ical;
use Gregwar\Image\Image;
use Yii;
use common\models\Apartment;
use backend\models\ApartmentSearch;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ApartmentController implements the CRUD actions for Apartment model.
 */
class ApartmentController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Apartment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApartmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Apartment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Apartment();
        $post = Yii::$app->request->post();
        if($model->load($post)) {
            $model->updated_at = $model->created_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success create'));
                if (isset($post['btn_create_and_create'])) {
                    return $this->redirect(['create']);
                }
                if (isset($post['btn_create_and_list'])) {
                    return $this->redirect(['index']);
                }
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', \Yii::t('backend', 'Error create'));
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Apartment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsIcal = $model->icals;

        $post = Yii::$app->request->post();
        if($model->load($post)){
            $oldIDs = ArrayHelper::map($modelsIcal, 'id', 'id');
            $modelsIcal = Model::createMultiple(Ical::classname(), $modelsIcal);
            Model::loadMultiple($modelsIcal, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsIcal, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsIcal) && $valid;

            if ($valid) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            Ical::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsIcal as $modelIcal) {
                            $modelIcal->apartment_id = $model->id;
                            $modelIcal->last_update = date('Y-m-d H:i:s');
                            if (! ($flag = $modelIcal->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Success save'));
                        if (isset($post['btn_update_and_list'])) {
                            return $this->redirect(['index']);
                        }
                        return $this->redirect(['update', 'id' => $model->id]);
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Error save'));
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelsIcal' => (empty($modelsIcal)) ? [new Ical()] : $modelsIcal
        ]);
    }

    /**
     * Deletes an existing Apartment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($this->findModel($id)->delete()){
            Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success delete'));
        }

        return $this->redirect(['index']);
    }

    public function actionPhotodelete($id){
        $model = ApartmentPhoto::findOne($id);
        $apartmentId = $model->apartment_id;
        @unlink(Yii::$app->params['uploadOrig'] . $apartmentId . '/' . $model->name);
        @unlink(Yii::$app->params['uploadLarge'] . $apartmentId . '/' . $model->name);
        @unlink(Yii::$app->params['uploadSmall'] . $apartmentId . '/' . $model->name);
        $model->delete();
        Yii::$app->session->setFlash('success', \Yii::t('backend', 'Success save'));
        return $this->redirect(['update', 'id' => $apartmentId]);
    }

    public function actionPosition(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = [];
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            /**
             * @var $model \common\models\Apartment
             */
            $model = Apartment::find()->with('apartmentPhotos')->where(['id' => $data['id']])->one();
            foreach ($model->apartmentPhotos as $photo) {
                foreach($data['position'] as $key=>$value){
                    if($value == $photo->id){
                        $photo->position = $key;
                        $photo->save();
                        continue;
                    }
                }
            }

            $result = [
                'code' => 100,
            ];
        }
        return $result;
    }

    /**
     * Finds the Apartment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apartment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Apartment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
