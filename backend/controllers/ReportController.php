<?php

namespace backend\controllers;

use common\models\Booking;
use common\models\Payment;
use common\models\Status;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\HttpException;

class ReportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $rules = [];
        if (Yii::$app->user->identity) {
            if (Yii::$app->user->identity->isRole(['root', 'admin', 'report'])) {
                $rules[] = [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['@'],
                ];
            }
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionSalary()
    {
        $salaryDate = new \DateTime(Yii::$app->request->post('salary_date', date('Y-m-d 10:00:00')));

        $users = User::find()->with('profile')->where('blocked_at IS NULL')->all();

        $bookings = Booking::find()->with(['object', 'customer'])->joinWith(['status' => function($q) {
            return $q->andWhere(['status.paid' => Status::ENABLE, 'status.approved' => Status::ENABLE]);
        }])->where(['=', 'DATE_FORMAT(date_out, "%Y-%m")', $salaryDate->format('Y-m')])
            ->all();

        $salary = [];

        foreach ($users as $user) {
            if ($user->username == 'root') {
                continue;
            }
            $salary[$user->id] = [
                'name' => $user->profile->name ?: $user->username,
                'percent' => $user->percent,
                'count' => 0,
                'total' => 0,
                'booking_total' => 0,
                'bookings' => []
            ];
            foreach ($bookings as $key => $booking) {
                if ($user->id !== $booking->user_id) continue;
                $salary[$user->id]['count']++;
                $salary[$user->id]['total'] += $booking->total;
                $salary[$user->id]['booking_total'] += $booking->booking_total;
                $salary[$user->id]['bookings'][] = $booking;
                unset($bookings[$key]);
            }
        }

        return $this->render('salary', [
            'salary' => $salary,
            'salaryDate' => $salaryDate
        ]);
    }

    public function actionIncomes()
    {
        $dateStart = new \DateTime(Yii::$app->request->get('date_start', date('Y-m-d', time() - 864000)));
        $dateEnd = new \DateTime(Yii::$app->request->get('date_end', date('Y-m-d')));

        $payments = Payment::find()->where('pay_date >= :dateStart AND pay_date <= :dateEnd',
            ['dateStart' => $dateStart->format('Y-m-d'), 'dateEnd' => $dateEnd->format('Y-m-d')])
        ->orderBy(['pay_date' => SORT_ASC])->all();

        $paymentsData = [];
        $chartData = [];
        foreach ($payments as $payment) {
            $time = strtotime($payment->pay_date) * 1000;
            if (isset($paymentsData[$time])) {
                $paymentsData[$time] += $payment->price;
            } else {
                $paymentsData[$time] = $payment->price;
            }
        }

        $total = 0;
        foreach ($paymentsData as $key => $value) {
            $d = new \DateTime();
            $d->setTimestamp($key / 1000);
            $chartData[] = [$key, $value];
            $total += $value;
        }

        return $this->render('incomes', [
            'chartData' => $chartData,
            'total' => $total,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd
        ]);
    }
}