<?php
namespace backend\controllers;

use common\models\Apartment;
use common\models\Customer;
use common\models\Payment;
use common\models\Booking;
use common\models\Wallet;
use common\models\WalletTransaction;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class AjaxController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        $rules = [];
        if (Yii::$app->user->identity) {
            if (Yii::$app->user->identity->isRole(['root', 'admin', 'booking'])) {
                $rules[] = [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['@'],
                ];
            }
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionBookingData()
    {
        $result = ['code' => 404];
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            if (!isset($data['loadDate'])) {
                $data['loadDate'] = date('Y-m-d');
            }
            if (!isset($data['showDays'])) {
                $data['showDays'] = 14;
            }
            $flatsId = [];
            $flats = Apartment::find()->select('id')->where(['ob_enable' => 1])->all();
            foreach ($flats as $value) {
                $flatsId[] = $value->id;
            }
            $startDate = new \DateTime($data['loadDate']);
//            $startDate->setTime(0, 0, 0);
            $endDate = new \DateTime(date('Y-m-d H:i:s', strtotime($data['loadDate']) + $data['showDays'] * 24 * 3600));
//            $endDate->setTime(23, 59, 59);
            $bookings = Booking::find()->with(['customer', 'status', 'payments', 'user'])
//                ->where(['<=', 'date_in', $endDate->format("Y-m-d 00:00:00")])
//                ->andWhere(['>', 'date_out', $startDate->format("Y-m-d 00:00:00")])

                ->where(['<=', 'DATE_FORMAT(date_in, "%Y-%m-%d")', $endDate->format("Y-m-d")])
                ->andWhere(['>', 'DATE_FORMAT(date_out, "%Y-%m-%d")', $startDate->format("Y-m-d")])
                ->andWhere(['in', 'object_id', $flatsId])
                ->andWhere(['approved' => Booking::ENABLE])
                ->asArray()->all();

            $result['bookings'] = $bookings;
            $result['loadDate'] = $data['loadDate'];
            $result['showDays'] = $data['showDays'];
            $result['code'] = 200;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionCustomerSearch()
    {
        $result = ['code' => 404];
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            $query = Customer::find();
            if (is_numeric($data['customer'])) {
                $query->where(['like', 'phone', $data['customer']]);
            } else {
                $query->where(['like', 'name', $data['customer']]);
            }
            $customers = $query->asArray()->all();

            $result['customers'] = $customers;
            $result['code'] = 200;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionBookingSave()
    {
        $result = ['code' => 'danger', 'message' => 'Страница не найдена'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isAjax) {
            return $result;
        }

        $data = Yii::$app->request->post();
        $startDate = new \DateTime($data['startDate']);
        $endDate = new \DateTime($data['endDate']);
        if ($startDate->getTimestamp() >= $endDate->getTimestamp()) {
            $result['code'] = 'warning';
            $result['message'] = 'Не правильно указаны даты проживания.';
            return $result;
        }
        $checkBookingQuery = Booking::find()->where(['object_id' => $data['apartmentId'], 'approved' => Booking::ENABLE])
            ->andWhere('((date_in >= :startDate AND date_in <= :endDate) OR (date_out >= :startDate AND date_in <= :endDate))',
                [':startDate' => $startDate->format('Y-m-d H:i:s'), ':endDate' => $endDate->format('Y-m-d H:i:s')]);

        if ($data['id']) {
            $checkBookingQuery->andWhere(['not in', 'id', [$data['id']]]);
        }

        $checkBooking = $checkBookingQuery->one();
        if ($checkBooking) {
            $result['code'] = 'warning';
            $result['message'] = 'На этот периуд уже есть бронь.';
            return $result;
        }

        if ($data['id']) {
            $booking = Booking::find()->where('id = :id', [':id' => $data['id']])->one();
            if (!$booking) {
                $result['code'] = 'danger';
                $result['message'] = 'Бронь не найдена.';
                return $result;
            }
        } else {
            $booking = new Booking();
        }

        if ($data['customer']['id']) {
            $customer = Customer::find()->where('id = :id', [':id' => $data['customer']['id']])->one();
            if (!$customer) {
                $result['code'] = 'danger';
                $result['message'] = 'Гость не найден.';
                return $result;
            }
        } else {
            $customer = new Customer();
            $customer->user_id = Yii::$app->user->id;
        }
        if (!$booking->user_id) {
            $booking->user_id = Yii::$app->user->id;
        }
        $booking->object_id = $data['apartmentId'];
        $booking->status_id = $data['statusId'];
        $booking->date_in = $startDate->format('Y-m-d H:i:s');
        $booking->date_out = $endDate->format('Y-m-d H:i:s');
        $booking->days = $data['days'];
        $booking->transfer_in = $data['transferIn'];
        $booking->transfer_out = $data['transferOut'];
        $booking->more_pay = $data['morePay'];
        $booking->price = $data['price'];
        $booking->total = $data['total'];
        $booking->booking_total = $data['bookingTotal'] ?: 0;
        $booking->information = $data['comment'];
        $booking->approved = Booking::ENABLE;

        $customer->name = $data['customer']['name'];
        $customer->phone = $data['customer']['phone'];
        $customer->email = $data['customer']['email'];
        $customer->information = $data['customer']['information'];

        if ($customer->validate()) {
            if ($customer->save()) {
                $booking->customer_id = $customer->id;
                if ($booking->validate()) {
                    if ($booking->save()) {
                        $result['booking'] = Booking::find()->with(['customer', 'status', 'payments', 'user'])->where(['id' => $booking->id])->asArray()->one();
                        $result['message'] = 'Изменения сохранены';
                    } else {
                        $result['code'] = 'danger';
                        $result['message'] = 'Не удалось сохранить данные брони.';
                        return $result;
                    }
                } else {
                    $result['code'] = 'danger';
                    $result['message'] = 'Указаны не допустимые значения в данных брони.';
                    return $result;
                }
            } else {
                $result['code'] = 'danger';
                $result['message'] = 'Не удалось сохранить данные гостя.';
                return $result;
            }
        } else {
            $result['code'] = 'danger';
            $result['message'] = 'Указаны не допустимые значения в данных гостя.';
            return $result;
        }

        $result['code'] = 200;
        return $result;
    }

    public function actionBookingDelete()
    {
        $result = ['code' => 'danger', 'message' => 'Страница не найдена'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isAjax) {
            return $result;
        }
        $data = Yii::$app->request->post();

        $booking = Booking::find()->where(['id' => $data['id']])->one();

        if ($booking) {
            $booking->approved = Booking::DISABLE;
            if ($booking->save()) {
                $result['code'] = 200;
                $result['message'] = 'Бронь удалена';
                return $result;
            } else {
                $result['code'] = 'danger';
                $result['message'] = 'Не удалось удалить бронь.';
                return $result;
            }
        }

        return $result;
    }

    public function actionPaymentSave()
    {
        $result = ['code' => 'danger', 'message' => 'Страница не найдена'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isAjax) {
            return $result;
        }

        $data = Yii::$app->request->post();
        $date = new \DateTime($data['paymentDate']);

        if (empty($data['bookingId']) || empty($date) || empty($data['paymentPrice'])) {
            $result['code'] = 'warning';
            $result['message'] = 'Не правельно указаны данные.';
            return $result;
        }
        if ($data['id']) {
            $payment = Payment::find()->where(['id' => $data['id'], 'booking_id' => $data['bookingId']])->one();
            if (!$payment) {
                $result['code'] = 'warning';
                $result['message'] = 'Не правельно указаны данные.';
                return $result;
            }
        } else {
            $payment = new Payment();
            $payment->booking_id = $data['bookingId'];
        }
        $payment->pay_date = $date->format('Y-m-d H:i:s');
        $payment->price = $data['paymentPrice'];
        $payment->comment = $data['paymentComment'];

        if ($payment->validate()) {
            if ($payment->save()) {
                $result['booking'] = Booking::find()->with(['customer', 'status', 'payments'])->where(['id' => $data['bookingId']])->asArray()->one();
                $result['message'] = 'Изменения сохранены';
            } else {
                $result['code'] = 'danger';
                $result['message'] = 'Не удалось сохранить данные об оплате.';
                return $result;
            }
        } else {
            $result['code'] = 'warning';
            $result['message'] = 'Указаны не допустимые значения в оплате.';
            return $result;
        }

        $result['code'] = 200;
        return $result;
    }

    public function actionPushBalance()
    {
        $result = ['type' => 'danger', 'message' => 'Страница не найдена'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isAjax) {
            return $result;
        }
        $data = Yii::$app->request->post();

        $wallet = Wallet::find()->where(['user_id' => $data['id']])->one();

        if (!$wallet) {
            $result['type'] = 'danger';
            $result['message'] = 'Личный счет не найден';
            return $result;
        }

        if (!is_numeric($data['balance'])) {
            $result['type'] = 'danger';
            $result['message'] = 'Не верно указанны данные';
            return $result;
        }

        $wallet->balance = $wallet->balance + $data['balance'];
        if ($wallet->save()) {
            $transaction = new WalletTransaction();
            $transaction->wallet_id = $wallet->id;
            $transaction->amount = abs($data['balance']);
            $transaction->commission = 0;
            $transaction->comment = $data['comment'];
            $transaction->status = $data['status'];
            $transaction->save();
        }

        $result['balance'] = Yii::$app->formatter->asCurrency($wallet->balance);
        $result['type'] = 'success';
        $result['message'] = 'Баланс пополнен';

        return $result;
    }
}
