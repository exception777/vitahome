<?php

namespace backend\controllers;

use common\models\Booking;
use Yii;
use common\models\Payment;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class PaymentsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $rules = [];
        if (Yii::$app->user->identity) {
            if (Yii::$app->user->identity->isRole(['root', 'admin', 'payments'])) {
                $rules[] = [
                    'actions' => [],
                    'allow' => true,
                    'roles' => ['@'],
                ];
            }
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dateStart = new \DateTime(Yii::$app->request->get('date_start', date('Y-m-d')));
        $dateEnd = new \DateTime(Yii::$app->request->get('date_end', date('Y-m-d')));

        $payments = Payment::find()->joinWith(['booking' => function($q) {
            $q->andWhere(['approved' => Booking::ENABLE]);
        }])->with(['booking.customer'])->where('pay_date >= :dateStart AND pay_date <= :dateEnd',
            ['dateStart' => $dateStart->format('Y-m-d'), 'dateEnd' => $dateEnd->format('Y-m-d')])
            ->orderBy(['pay_date' => SORT_ASC])
            ->all();

        return $this->render('index', [
            'payments' => $payments,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd
        ]);
    }

    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payment();

        $post = Yii::$app->request->post();

        if($model->load($post)) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Success create'));
                if (isset($post['btn_create_and_create'])) {
                    return $this->redirect(['create']);
                }
                if (isset($post['btn_create_and_list'])) {
                    return $this->redirect(['index']);
                }
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Error create'));
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if($model->load($post)){
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Success save'));
                if (isset($post['btn_update_and_list'])) {
                    return $this->redirect(['index']);
                }
                return $this->redirect(['update', 'id' => $model->id]);
            } else{
                Yii::$app->session->setFlash('error', Yii::t('app', 'Error save'));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($this->findModel($id)->delete()){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Success delete'));
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
