$(function(){
    $(document).on('click', '.showModalButton', function(){
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent').load($(this).attr('value'));
            $('#modalHeader').find('h4').html($(this).attr('title'));
        } else {
            $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
            $('#modalHeader').find('h4').html($(this).attr('title'));
        }
    });
});

function alertShow(message, type) {
    return '<div class="alert alert-' + type + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Закрыть">' +
        '<span aria-hidden="true">&times;</span></button>' + message + '</div>'
}
