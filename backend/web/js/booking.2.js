jQuery(document).ready(function ($) {
    var daysName = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        showDays = 14,
        loadDate = new Date(),
        minDate = moment(),
        loadingData = true,
        gridItems = $('#x-grid-items'),
        gridCells = $('#x-grid-cells'),
        gridTableRows = $('#x-grid-table-rows'),
        bookings = [],
        customers = [],
        modalBookingInfo = $('#modal-booking-info'),
        formModeCreate = true,
        formId = $('#form-id'),
        formApartment = $('#form-apartment'),
        formStatus = $('#form-status'),
        formStartDate = $('#form-start-date'),
        formStartDatepicker = $('#form-start-datepicker'),
        formEndDate = $('#form-end-date'),
        formEndDatepicker = $('#form-end-datepicker'),
        formDays = $('#form-days'),
        formTransferIn = $('#form-transfer_in'),
        formTransferOut = $('#form-transfer_out'),
        formMorePay = $('#form-more_pay'),
        formPrice = $('#form-price'),
        formTotal = $('#form-total'),
        formBookingTotal = $('#form-booking_total'),
        formComment = $('#form-comment'),
        formCustomerId = $('#form-customer-id'),
        formCustomerName = $('#form-customer-name'),
        customerResult = $('#customer-result'),
        modalClearCustomer = $('#modal-clear-customer'),
        formCustomerPhone = $('#form-customer-phone'),
        customerPhoneResult = $('#customer-phone-result'),
        formCustomerEmail = $('#form-customer-email'),
        formCustomerInformation = $('#form-customer-information'),
        phoneCall = $('#phone-call'),
        dateStepDatepicker = $('#date-step-datepicker'),
        formCustomerSearch = $('#form-customer-search'),
        customerProfile = $('#customer-profile');

    if (admin) {
        minDate.set({'year': '2014'});
    } else {
        minDate.subtract(10, 'days');
    }
    if (Cookies.get('showDays') !== undefined) {
        showDays = Cookies.get('showDays');
    }
    if (Cookies.get('loadDate') !== undefined) {
        loadDate.setTime(Cookies.get('loadDate'));
    }
    moment.locale('ru');

    gridPaint();
    var infoBlock = document.createElement('div');
    infoBlock.setAttribute('id', 'info-block');
    $('body').append(infoBlock);

    formStartDatepicker.datetimepicker({
        locale: 'ru',
        sideBySide: true,
        format: 'YYYY-MM-DD HH:mm'
    });
    formEndDatepicker.datetimepicker({
        locale: 'ru',
        useCurrent: false,
        sideBySide: true,
        format: 'YYYY-MM-DD HH:mm'
    });
    formStartDatepicker.on('dp.change', function (e) {
        formEndDatepicker.data('DateTimePicker').minDate(e.date);
        formCalculate();
    });
    formEndDatepicker.on('dp.change', function (e) {
        formCalculate();
    });

    $('#form-customer-document-datepicker').datetimepicker({
        locale: 'ru',
        format: 'YYYY-MM-DD'
    });
    dateStepDatepicker.datetimepicker({
        locale: 'ru',
        format: 'YYYY-MM-DD'
    });

    gridCells.on('click', 'div.cell-empty', function () {
        var startDate = moment(loadDate),
            apartmentId = $(this).parents('td[data-id]').data('id'),
            apartmentPrice = $(this).parents('td[data-price]').data('price');
        formClear();
        formModeCreate = true;
        $('#booking-form-label').html('Новая бронь');
        formApartment.val(apartmentId);
        startDate.add($(this).parent('td').index(), 'd');
        startDate.set({'hour': 12, 'minute': 0, 'second': 0});
        formStartDatepicker.data('DateTimePicker').date(startDate.clone());
        startDate.add(1, 'd');
        formEndDatepicker.data('DateTimePicker').date(startDate.clone());
        formPrice.val(apartmentPrice);
        formCalculate();
        $('#booking-form').modal("show");
    }).on('click', 'div.x-grid-booking[data-id]', function () {
        var booking = getBooking($(this).data('id'));
        if (booking) {
            formBookingEditOpen(booking);
        } else {
            $('#x-alert').html(alertShow('Бронь не найдена. Обновите страницу', 'danger'));
        }
    }).on('mouseenter', 'div.x-grid-booking[data-id]', function(e){
        var booking = getBooking($(this).data('id'));
        if (!booking) return;
        var startDate = moment(booking['date_in']), endDate = moment(booking['date_out']);
        var str = '';
        if (startDate.hour() < 10 || startDate.hour() > 21) {
            str += '<div><strong>Раний заезд ' + startDate.format('HH:mm') + '</strong></div>';
        }
        if (endDate.hour() > 21 || endDate.hour() < 10) {
            str += '<div><strong>Поздний выезд ' + endDate.format('HH:mm') + '</strong></div>';
        }
        $(infoBlock).html(str + '<div><strong>№</strong>' + booking['id'] + ' '  + booking['status']['name'] + '</div>' +
            '<div>' + getBookingDateInfo(moment(booking['date_in']), moment(booking['date_out'])) + '</div>');
        if (booking.hasOwnProperty('user') && booking['user'] && booking['user'].hasOwnProperty('username')) {
            $(infoBlock).append('<div><strong>Забронировал:</strong> ' + booking['user']['username'] + '</div>');
        }
        $(infoBlock).show();
    }).on('mouseleave', 'div.x-grid-booking[data-id]', function(e){
        $(infoBlock).hide();
    }).on('mousemove', 'div.x-grid-booking[data-id]', function(e){
        $(infoBlock).css('left', e.pageX - 50);
        $(infoBlock).css('top', e.pageY + 10);
    });

    $('#booking-form').on('click', '#btn-reserv-save', function () {
        if (formStartDate.val() == '') {
            modalBookingInfo.html(alertShow('Укажите дату заезда', 'danger'));
            return false;
        }
        if (formEndDate.val() == '') {
            modalBookingInfo.html(alertShow('Укажите дату выезда', 'danger'));
            return false;
        }
        if (formCustomerName.val() == '') {
            modalBookingInfo.html(alertShow('Укажите Ф.И.О. клиента', 'danger'));
            return false;
        }
        if (formCustomerPhone.val() == '') {
            modalBookingInfo.html(alertShow('Укажите телефон клиента', 'danger'));
            return false;
        }
        modalBookingInfo.html('');
        overlayModalShow();
        $.ajax({
            url: urlBookingSave,
            type: 'POST',
            data: {
                'id': formId.val(),
                'apartmentId': formApartment.val(),
                'statusId': formStatus.val(),
                'startDate': formStartDate.val(),
                'endDate': formEndDate.val(),
                'days': formDays.val(),
                'transferIn': formTransferIn.val(),
                'transferOut': formTransferOut.val(),
                'morePay': formMorePay.val(),
                'price': formPrice.val(),
                'total': formTotal.val(),
                'bookingTotal': formBookingTotal.val(),
                'comment': formComment.val(),
                'customer': {
                    'id': formCustomerId.val(),
                    'name': formCustomerName.val(),
                    'phone': formCustomerPhone.val(),
                    'email': formCustomerEmail.val(),
                    'information': formCustomerInformation.val()
                }
            }
        })
            .done(function (data) {
                if (data['code'] == 200) {
                    var booking = data['booking'], rec = true;
                    modalBookingInfo.html(alertShow(data['message'], 'success'));
                    for (var i = 0; i < bookings.length; i++) {
                        if (bookings[i]['id'] == booking['id']) {
                            bookings[i] = booking;
                            rec = false;
                            break;
                        }
                    }
                    if (rec) bookings.push(booking);
                    gridRemoveBooking(booking['id']);
                    gridPaintBooking(booking);
                    if (formModeCreate) {
                        $('#booking-form-label').html('Редактировать бронь: №' + booking['id']);
                        formDataWrite(booking);
                    }
                } else {
                    modalBookingInfo.html(alertShow(data['message'], data['code']));
                }

            })
            .fail(function () {
                alert('Ошибка в запросе');
            })
            .always(function () {
                overlayModalHide();
            });
    })
        .on('click', '#modal-add-customer', function () {
            if (formCustomerSearch.val() == '') {
                modalBookingInfo.html(alertShow('Введите имя гостя', 'danger'));
                return false;
            }

            customerResult.html('search...');
            customerResult.show();
            $.ajax({
                url: urlCustomerSearch,
                type: 'POST',
                data: {'customer': formCustomerSearch.val()}
            })
                .done(function (data) {
                    customerResult.html('');
                    if (data['code'] == 200) {
                        if (data['customers']) {
                            customers = data['customers'];
                            for (var key in customers) {
                                if (customers.hasOwnProperty(key)) {
                                    customerResult.append('<div class="customer-item" data-id="' + customers[key]['id'] + '">' +
                                        customers[key]['name'] + ' (' + customers[key]['phone'] + ')</div>');
                                }
                            }
                        }
                        else {
                            customerResult.html('<div class="text-info">Совпадений не найдено</div>');
                        }
                    }
                    if (data['code'] == 404) {
                        customerResult.html('<div class="text-danger">Ошибка. Данные не найдены.</div>');
                    }
                })
                .fail(function () {
                    alert('Ошибка в загрузке данных');
                })
                .always(function () {
                    customerResult.append('<div class="text-center"><button type="button" id="customer-search-close" ' +
                        'class="btn btn-default btn-sm btn-block">Закрыть</button></div>');
                });
        }).on('click', '#customer-search-close', function () {
        customerResult.hide();
    }).on('click', '.customer-item', function () {
        var c = null, id = $(this).data('id');
        for (var key in customers) {
            if (customers.hasOwnProperty(key)) {
                if (customers[key]['id'] == id) {
                    c = customers[key];
                    break;
                }
            }
        }
        if (c != null) {
            modalClearCustomer.show();
            formCustomerId.val(c['id']);
            formCustomerName.val(c['name']);
            formCustomerPhone.val(c['phone']);
            formCustomerEmail.val(c['email']);
            formCustomerInformation.val(c['information']);
            phoneCall.html('<a class="btn btn-default btn-sm btn-block btn-flat" href="tel:' + c['phone'] + '">Позвонить: ' + c['phone'] + '</a>');
            customerProfile.html('<a class="btn btn-default btn-sm btn-block btn-flat" target="_blank" href="/customer/view?id=' + c['id'] + '">Открыть профиль</a>');
            customerResult.hide();
            customerPhoneResult.hide();
        }
    }).on('click', '#modal-clear-customer', function () {
        modalClearCustomer.hide();
        formClearCustomer();
    }).on('click', '#customer-phone-search-close', function () {
        customerPhoneResult.hide();
    }).on('click', '#modal-clear-customer-phone', function () {
        modalClearCustomer.hide();
        formClearCustomer();
    });
    formPrice.on('keyup', function () {
        formCalculatePayment();
    });
    formTransferIn.on('keyup', function () {
        formCalculatePayment();
    });
    formTransferOut.on('keyup', function () {
        formCalculatePayment();
    });
    formMorePay.on('keyup', function () {
        formCalculatePayment();
    });
    formDays.on('keyup', function () {
        formCalculatePayment();
    });

    $('#week1').on('click', function () {
        if (showDays !== 7) {
            showDays = 7;
            Cookies.set('showDays', 7);
            gridPaint();
        }
    });
    $('#week2').on('click', function () {
        if (showDays !== 14) {
            showDays = 14;
            Cookies.set('showDays', 14);
            gridPaint();
        }
    });
    $('#month').on('click', function () {
        if (showDays !== 30) {
            showDays = 30;
            Cookies.set('showDays', 30);
            gridPaint();
        }
    });
    $('#back_day').on('click', function () {
        var newLoadDate = new Date(loadDate);
        newLoadDate.setDate(loadDate.getDate() - 1);
        if (minDate > newLoadDate) return false;
        loadDate.setDate(loadDate.getDate() - 1);
        Cookies.set('loadDate', loadDate.getTime());
        gridPaint();
    });
    $('#back').on('click', function () {
        var newLoadDate = new Date(loadDate);
        newLoadDate.setDate(loadDate.getDate() - 7);
        if (minDate.valueOf() > newLoadDate.valueOf()) return false;
        loadDate.setDate(loadDate.getDate() - 7);
        Cookies.set('loadDate', loadDate.getTime());
        gridPaint();
    });
    $('#now_day').on('click', function () {
        loadDate = new Date();
        Cookies.remove('loadDate');
        gridPaint();
    });
    $('#next_day').on('click', function () {
        loadDate.setDate(loadDate.getDate() + 1);
        Cookies.set('loadDate', loadDate.getTime());
        gridPaint();
    });
    $('#next').on('click', function () {
        loadDate.setDate(loadDate.getDate() + 7);
        Cookies.set('loadDate', loadDate.getTime());
        gridPaint();
    });
    $('#refresh').on('click', function () {
        gridPaint();
    });
    $('#btn-date-step').on('click', function() {
        if (!dateStepDatepicker.val()) return;
        var newLoadDate = moment(dateStepDatepicker.val());
        if (minDate.valueOf() > newLoadDate.valueOf()) {
            newLoadDate = minDate.clone();
        }
        loadDate.setTime(newLoadDate.valueOf());
        Cookies.set('loadDate', loadDate.getTime());
        gridPaint();
    });
    var inputSearchById = $('#input-search_by_id');
    $('#btn-search_by_id').on('click', function(){
        var booking = getBooking(inputSearchById.val());
        if (!booking) {
            inputSearchById.val('');
            return;
        }
        console.log(!booking, booking, inputSearchById.val());
        formBookingEditOpen(booking);
    });
    $(inputSearchById).on('keydown', function(e) {
        if (e.keyCode === 13) {
            $('#btn-search_by_id').click();
        }
    });

    $('#btn-reserv-remove').on('click', function () {
        if (!confirm('Вы уверены, что хотите удалить эту бронь?')) {
            return false;
        }
        modalBookingInfo.html('');
        overlayModalShow();
        var bookingId = formId.val();
        $.ajax({
            url: urlBookingDelete,
            type: 'POST',
            data: {'id': bookingId}
        })
            .done(function (data) {
                if (data['code'] == 200) {
                    $('#booking-form').modal("hide");
                    formClear();
                    gridRemoveBooking(bookingId);
                } else {
                    modalBookingInfo.html(alertShow(data['message'], data['code']));
                }
            })
            .fail(function () {
                alert('Ошибка в загрузке данных');
            })
            .always(function () {
                overlayModalHide();
            });
    });

    var formPaymentId = $('#form-payment-id'),
        formPaymentDatepicker = $('#form-payment-datepicker'),
        formPaymentDate = $('#form-payment-date'),
        formPaymentPrice = $('#form-payment-price'),
        formPaymentComment = $('#form-payment-comment'),
        bookingPayments = $('#booking-payments');

    formPaymentDatepicker.datetimepicker({
        locale: 'ru',
        sideBySide: true,
        format: 'YYYY-MM-DD'
    });

    $('#btn-payment-save').on('click', function () {
        if (formPaymentDate.val() == '') {
            modalBookingInfo.html(alertShow('Укажите дату оплаты', 'danger'));
            return false;
        }
        if (formPaymentPrice.val() == '') {
            modalBookingInfo.html(alertShow('Укажите сумму оплаты', 'danger'));
            return false;
        }
        modalBookingInfo.html('');
        overlayModalShow();
        $.ajax({
            url: urlPaymentSave,
            type: 'POST',
            data: {
                'id': formPaymentId.val(), 'bookingId': formId.val(), 'paymentDate': formPaymentDate.val(),
                'paymentPrice': formPaymentPrice.val(), 'paymentComment': formPaymentComment.val()
            }
        })
            .done(function (data) {
                if (data['code'] == 200) {
                    var booking = data['booking'], rec = true;
                    modalBookingInfo.html(alertShow(data['message'], 'success'));
                    for (var i = 0; i < bookings.length; i++) {
                        if (bookings[i]['id'] == booking['id']) {
                            bookings[i] = booking;
                            rec = false;
                            break;
                        }
                    }
                    if (rec) bookings.push(booking);
                    bookingPaymentsShow(booking['payments']);
                    formClearPayment();
                } else {
                    modalBookingInfo.html(alertShow(data['message'], 'danger'));
                }
            })
            .fail(function () {
                alert('Ошибка в загрузке данных');
            })
            .always(function () {
                overlayModalHide();
            });
    });
    bookingPayments.on('click', '.payment-edit', function() {
        var id = $(this).data('id'), booking = getBooking($(this).data('bookingId'));
        var payment = null;
        for (var key in booking['payments']) {
            if (!booking['payments'].hasOwnProperty(key)) continue;
            if (booking['payments'][key]['id'] == id) {
                payment = booking['payments'][key];
                break;
            }
        }
        if (payment) {
            $('tr.payment-row').removeClass('payment-edit');
            $(this).parents('tr.payment-row').addClass('payment-edit');
            formPaymentId.val(payment['id']);
            formPaymentDatepicker.data('DateTimePicker').date(payment['pay_date']);
            formPaymentPrice.val(payment['price']);
            formPaymentComment.val(payment['comment']);
        }

    }).on('click', '.payment-remove', function() {
        $(this).parents('tr.payment-row').remove();
    });
    formCustomerPhone.keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    function gridPaint() {
        if (loadingData) {
            loadingData = false;
            loadBookingData();
            gridCells.find('.x-grid-cells-header').html(initCellHeader());
            loadingData = true;
        }
    }

    function formDataWrite(booking) {
        modalClearCustomer.show();
        formId.val(booking['id']);
        formApartment.val(booking['object_id']);
        formStatus.val(booking['status_id']);
        formStartDatepicker.data('DateTimePicker').date(booking['date_in']);
        formEndDatepicker.data('DateTimePicker').date(booking['date_out']);
        formDays.val(booking['days']);
        formTransferIn.val(booking['transfer_in']);
        formTransferOut.val(booking['transfer_out']);
        formMorePay.val(booking['more_pay']);
        formPrice.val(booking['price']);
        formTotal.val(booking['total']);
        formBookingTotal.val(booking['booking_total']);
        formComment.val(booking['information']);
        if (booking['customer']) {
            formCustomerId.val(booking['customer']['id']);
            formCustomerName.val(booking['customer']['name']);
            formCustomerPhone.val(booking['customer']['phone']);
            formCustomerEmail.val(booking['customer']['email']);
            formCustomerInformation.val(booking['customer']['information']);
            phoneCall.html('<a class="btn btn-default btn-sm btn-block btn-flat" href="tel:' + booking['customer']['phone'] + '">Позвонить: ' + booking['customer']['phone'] + '</a>');
            customerProfile.html('<a class="btn btn-default btn-sm btn-block btn-flat" target="_blank" href="/customer/view?id=' + booking['customer']['id'] + '">Открыть профиль</a>');
        }
    }

    function formClear() {
        modalClearCustomer.hide();
        modalBookingInfo.html('');
        formId.val('');
        formApartment.val(formApartment.find('option:first-child').val());
        formStatus.val(formStatus.find('option:first-child').val());
        formStartDate.val('');
        formEndDate.val('');
        formDays.val(0);
        formTransferIn.val(0);
        formTransferOut.val(0);
        formMorePay.val(0);
        formPrice.val(0);
        formTotal.val(0);
        formBookingTotal.val(0);
        formComment.val('');
        bookingPayments.html('');
        phoneCall.html('');
        customerProfile.html('');
        formClearCustomer();
        formClearPayment();
    }

    function formClearCustomer() {
        formCustomerId.val('');
        formCustomerName.val('');
        formCustomerPhone.val('');
        formCustomerEmail.val('');
        formCustomerInformation.val('');
        phoneCall.html('');
        customerProfile.html('');
    }

    function formClearPayment() {
        formPaymentDate.val('');
        formPaymentPrice.val('');
        formPaymentComment.val('');
    }

    function formCalculate() {
        formCalculateDays();
        formCalculatePayment();
    }

    function formCalculateDays() {
        var sDate = moment(formStartDate.val()),
            eDate = moment(formEndDate.val()),
            days;
        days = getRangeDateDays(sDate.valueOf(), eDate.valueOf());
        if (days <= 0) days = 1;
        formDays.val(days);
    }

    function formCalculatePayment() {
        formTotal.val((formDays.val() * formPrice.val()).toFixed(2));
        var total = parseFloat(formTotal.val()) + parseFloat(formTransferIn.val()) + parseFloat(formTransferOut.val()) + parseFloat(formMorePay.val());
        formBookingTotal.val(total.toFixed(2));
    }

    function formBookingEditOpen(booking) {
        formClear();
        formModeCreate = false;
        $('#booking-form-label').html('Редактировать бронь: №' + booking['id']);
        formDataWrite(booking);
        bookingPaymentsShow(booking['payments']);
        $('#booking-form').modal("show");
    }

    function loadBookingData() {
        overlayShow();
        var lDate = moment(loadDate);
        $.ajax({
            url: urlBookingData,
            type: 'POST',
            data: {
                'loadDate': lDate.format(), 'showDays': showDays
            }
        })
            .done(function (data) {
                if (data['code'] == 200) {
                    bookings = data['bookings'];
                    gridItems.find('.x-grid-items-body').html(initApartmentsList());
                }
            })
            .fail(function () {
                alert('Ошибка в загрузке данных');
            })
            .always(function () {
                overlayHide();
            });
    }

    function initApartmentsList() {
        var tmp = '<div class="x-grid-block-body">{name}</div>',
            itemsBody = '';
        for (var key in apartments) {
            if (!apartments.hasOwnProperty(key)) continue;
            var title = apartments[key]['id'] + ": " + apartments[key]['name'] + " - " + apartments[key]['price'];
            itemsBody += tmp.replace('{id}', apartments[key]['id'])
                .replace('{name}', '<div class="x-grid-apartment-name" title="' + title + '" data-toggle="tooltip">№' +
                    apartments[key]['id'] + ": " + apartments[key]['title'] + '</div>');

            gridTableRows.find('td[data-id="' + apartments[key]['id'] + '"]').html(initCellBody());

        }
        initBookingGrid();
        return itemsBody;
    }

    function initBookingGrid() {
        for (var i = 0; i < bookings.length; i++) {
            gridPaintBooking(bookings[i]);
        }
    }

    function initCellHeader() {
        var tmp = '<table class="x-grid-table-header"><tr>{months}</tr><tr>{dates}</tr></table>',
            dates = '',
            months = '',
            startDate = new Date(loadDate),
            startMonth = loadDate.getMonth(),
            lastMontI = 1;
        for (var i = 1; i <= showDays; i++) {
            var attrClass = '';
            if (startDate.getDay() == 0 || startDate.getDay() == 6) {
                attrClass = 'weekend-days';
            }
            dates += '<td class="' + attrClass + '" style="width:' + (100 / showDays) + '%"><span>' + startDate.getDate()
                + '</span><span>' + daysName[startDate.getDay()] + '</span></td>';

            if (startMonth != startDate.getMonth() || i == showDays) {
                var colDays = i - lastMontI;
                months += '<td colspan="' + colDays + '">';
                if (colDays > 2) {
                    months += monthNames[startMonth] + ', ' + startDate.getFullYear() + 'г.';
                }
                months += '</td>';
                lastMontI = i - 1;
                startMonth = startDate.getMonth();
            }

            startDate.setDate(startDate.getDate() + 1);
        }

        tmp = tmp.replace('{dates}', dates).replace('{months}', months);
        return tmp;
    }

    function initCellBody() {
        var tmp = '<table class="x-grid-table-body"><tr>{cell}</tr></table>',
            cell = '', nowDate = new Date(loadDate);
        for (var i = 0; i < showDays; i++) {
            cell += '<td style="width:' + (100 / showDays) + '%"><div class="cell-empty"></div></td>';
            nowDate.setDate(nowDate.getDate() + 1);
        }
        tmp = tmp.replace('{cell}', cell);
        return tmp;
    }

    function isDateRangeCheck(checkDate, startDate, endDate) {
        checkDate = new Date(checkDate);
        checkDate.setHours(0, 0, 0, 0);
        startDate = new Date(startDate);
        startDate.setHours(0, 0, 0, 0);
        endDate = new Date(endDate);
        endDate.setHours(0, 0, 0, 0);
        if (checkDate >= startDate && checkDate <= endDate) {
            return true;
        }
        return false;
    }


    function gridPaintBooking(booking) {
        var tdApart = gridTableRows.find('td[data-id="' + booking['object_id'] + '"]');
        if (tdApart) {
            var left, sizeCell = 100 / showDays, width, startDate = moment(booking['date_in']),
                endDate = moment(booking['date_out']), ld = moment(loadDate);
            ld.set({'hour': 0, 'minute': 0, 'second': 0});
            // startDate.set({'hour': 0, 'minute': 0, 'second': 0});
            // endDate.set({'hour': 0, 'minute': 0, 'second': 0});
            var leftDay = getRangeDateDays(ld, startDate);
            var widthDay = getRangeDateDays(startDate, endDate);
            if (leftDay >= 0) {
                left = leftDay * sizeCell;
                width = widthDay * sizeCell;
            } else {
                left = 0;
                width = (widthDay + leftDay) * sizeCell;
            }
            if (width == 0) width = sizeCell;
            var early = '', late = '';
            if (startDate.hour() < 10 || startDate.hour() > 21) {
                early = '<div class="early"><i class="fa fa-exclamation"></i></div>';
            }
            if (endDate.hour() > 21 || endDate.hour() < 10) {
                late = '<div class="late"><i class="fa fa-exclamation"></i></div>';
            }
            var strBook = '<div class="x-grid-booking" style="left:' + left + '%; width:' + width + '%;background-color:' +
                booking['status']['color'] + '" data-id="' + booking['id'] + '">' +
                early + late +
                '<div class="x-grid-booking-date">' + getBookingDateInfo(startDate, endDate) + '</div>' +
                '<div class="x-grid-booking-customer">' + (booking['customer'] ? booking['customer']['name']: 'Не указан') + '</div></div>';
            tdApart.append(strBook);
        }
    }

    function getRangeDateDays(date1, date2) {
        date1 = new Date(date1);
        date1.setHours(0, 0, 0, 0);
        date2 = new Date(date2);
        date2.setHours(0, 0, 0, 0);
        return Math.floor((date2.getTime() - date1.getTime()) / (3600000 * 24));
    }

    function getBookingDateInfo(startDate, endDate) {
        startDate = moment(startDate);
        endDate = moment(endDate);
        return 'С ' + startDate.format('DD MMM, HH') + ' ч. ДО ' + endDate.format('DD MMM, HH') + ' ч.';
    }

    function gridRemoveBooking(id) {
        gridCells.find('div.x-grid-booking[data-id="' + id + '"]').remove();
    }
    function getBooking(id) {
        var b = null;
        for (var i = 0; i < bookings.length; i++) {
            if (!bookings.hasOwnProperty(i)) continue;
            if (bookings[i]['id'] == id) {
                b = bookings[i];
                break;
            }
        }
        return b;
    }

    function overlayShow() {
        $('#overlay').css('display', 'block');
    }

    function overlayHide() {
        $('#overlay').css('display', 'none');
    }

    function overlayModalShow() {
        $('#overlayModal').css('display', 'block');
    }

    function overlayModalHide() {
        $('#overlayModal').css('display', 'none');
    }

    function bookingPaymentsShow(payments) {
        var paymentHtml = '<table class="table table-bordered"><tr><th>Дата</th><th>Оплата</th><th>Комментарий</th>';
        if (admin) {
            paymentHtml += '<th></th>';
        }
        paymentHtml += '</tr>';
        var total = 0;
        for (var i = 0; i < payments.length; i++) {
            if (!payments.hasOwnProperty(i)) continue;
            total += parseFloat(payments[i]['price']);
            paymentHtml += '<tr class="payment-row"><td>' + moment(payments[i]['pay_date']).format('DD MMMM YYYY г.') +
                '</td><td>' + payments[i]['price'] + '</td><td>' + payments[i]['comment'] + '</td>';
            if (admin) {
                paymentHtml +='<td><button type="button" class="btn btn-sm btn-default payment-edit" data-id="' +
                    payments[i]['id'] + '" data-booking-id="' + payments[i]['booking_id'] + '"><i class="fa fa-edit"></i></button> ' +
                    '<button type="button" class="btn btn-sm btn-danger payment-remove" data-id="' + payments[i]['id'] +
                    '"><i class="fa fa-times"></i></button></td>';
            }
            paymentHtml += '</tr>';
        }
        paymentHtml += '<tr><th>Оплачено</th><th>' + total + '</th><th></th>';
        if (admin) {
            paymentHtml += '<th></th>';
        }
        paymentHtml += '</table>';
        bookingPayments.html(paymentHtml);
    }

    function getDecimal(num) {
        var str = "" + num;
        var zeroPos = str.indexOf(".");
        if (zeroPos == -1) return 0;
        str = str.slice(zeroPos);
        return +str;
    }
});
