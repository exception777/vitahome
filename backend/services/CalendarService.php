<?php
namespace backend\services;

use common\models\Booking;
use common\models\Ical;
use Sabre\VObject\Reader;
use Yii;

class CalendarService
{
    public function import()
    {
        $dtNow = new \DateTime();
        $listIcal = Ical::find()->joinWith('apartment')
            ->where('ical.approved = 1 AND last_update < :nowDate', [':nowDate' => $dtNow->format('Y-m-d H:i:s')])->all();
        $bookings = Booking::find()->where('date_out >= :dateOut', [':dateOut' => $dtNow->format('Y-m-d 00:00:00')])->all();

        foreach ($listIcal as $item) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $item->ical_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $data = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if ($info['http_code'] != 200) {
                continue;
            }

            $vCalendar = Reader::read($data);

            if (!isset($vCalendar->VEVENT)) continue;
            $bookingsInsert = [];
            foreach ($vCalendar->VEVENT as $vevent) {
                $uid = $vevent->UID->getValue();
                $summary = $vevent->SUMMARY->getValue();
                if ($summary == 'Not available') continue;
                if ($vevent->DTSTART->hasTime()) {
                    $dtStart = $vevent->DTSTART->getDateTime()->setTimeZone(new \DateTimeZone(Yii::$app->timeZone));
                    $dtEnd = $vevent->DTEND->getDateTime()->setTimeZone(new \DateTimeZone(Yii::$app->timeZone));
                } else {
                    $dtStart = $vevent->DTSTART->getDateTime();
                    $dtEnd = $vevent->DTEND->getDateTime();
                }
                $description = isset($vevent->DESCRIPTION) ? $vevent->DESCRIPTION : '';
                if ($dtEnd->getTimestamp() < $dtNow->getTimestamp()) continue;
                $checkBooking = false;
                foreach ($bookings as $booking) {
                    if ($booking->object_id != $item->apartment_id) continue;
                    $bStart = new \DateTime($booking->date_in);
                    $bEnd = new \DateTime($booking->date_out);

                    if (($bStart->getTimestamp() >= $dtStart->getTimestamp() && $bStart->getTimestamp() <= $dtEnd->getTimestamp()) ||
                        ($bEnd->getTimestamp() >= $dtStart->getTimestamp() && $bEnd->getTimestamp() <= $dtEnd->getTimestamp()) ||
                        ($bStart->getTimestamp() <= $dtStart->getTimestamp() && $bEnd->getTimestamp() >= $dtEnd->getTimestamp())) {
                        $checkBooking = true;
                    }
                }
                if ($checkBooking) continue;

                $night = $dtStart->diff($dtEnd);
                $bookingsInsert[] = [
                    $item->apartment_id,
                    $item->status_id,
                    $dtStart->format('Y-m-d H:i:s'),
                    $dtEnd->format('Y-m-d H:i:s'),
                    $night->days,
                    $summary . "\n" . $description,
                    $item->apartment->user_id,
                    $item->apartment->price,
                    $item->apartment->price * $night->days,
                    $item->apartment->price * $night->days,
                    Booking::ENABLE,
                    $uid
                ];

            }
            if ($bookingsInsert) {
                Yii::$app->db->createCommand()->batchInsert(Booking::tableName(),
                    ['object_id', 'status_id', 'date_in', 'date_out', 'days', 'information', 'user_id', 'price', 'total',
                        'booking_total', 'approved', 'ical_hash'], $bookingsInsert
                )->execute();
            }
            $lastUpdate = clone $dtNow;
            $lastUpdate->modify('+30 minutes');
            $item->last_update = $lastUpdate->format('Y-m-d H:i:s');
            $item->save();
        }
    }
}