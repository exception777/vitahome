<?php

namespace backend\models;

use common\models\SupportMessage;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Support;

/**
 * SupportSearch represents the model behind the search form about `common\models\Support`.
 */
class SupportSearch extends Support
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'user_id'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Support::find()->with(['user', 'supportMessages' => function($query){
                $query->andWhere('support_message.status = :status AND user_id != :user',
                    [':status' => SupportMessage::STATUS_NEW, ':user' => Yii::$app->user->id]);
            }]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'user_id' => $this->user_id
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->orderBy(['status' => SORT_DESC, 'id' => SORT_DESC]);

        return $dataProvider;
    }
}
