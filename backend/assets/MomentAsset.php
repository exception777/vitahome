<?php

namespace backend\assets;

use yii\web\AssetBundle;

class MomentAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/moment.min.js',
        'js/moment-timezone-with-data.min.js'
    ];

}