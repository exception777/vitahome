<?php

namespace backend\assets;

use yii\web\AssetBundle;

class BookingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'
    ];

    public $js = [
        'js/moment-with-locales.min.js',
        'plugins/datetimepicker/js/bootstrap-datetimepicker.min.js',
        '/js/booking.2.js'
    ];

    public $depends = [
        'backend\assets\AppAsset',
        '\webtoucher\cookie\AssetBundle'
    ];
}